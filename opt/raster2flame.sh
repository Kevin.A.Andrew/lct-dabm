if [ $# != 2 ]
then
    exit 1
fi

awk -f aux/agents.awk "$1.agents" > /tmp/agents_
tail -n +7 "$1.cover" | tr ' ' '\n' | sed -e "s///" | awk 'NF' > /tmp/grid0_
tail -n +7 "$1.parcels" | tr ' ' '\n' | sed -e "s///" | awk 'NF' > /tmp/grid1_
paste -d ',' /tmp/grid0_ /tmp/grid1_ > /tmp/grid2_
awk -f aux/cell.awk /tmp/grid2_ > /tmp/grid22_

echo '{"environment":{}, "agents": {' > /tmp/grid3_
cat /tmp/agents_ >> /tmp/grid3_
echo ',"landcell": { "default": [' >> /tmp/grid3_
cat /tmp/grid22_ >> /tmp/grid3_
echo ']}}}' >> /tmp/grid3_

jq --sort-keys < /tmp/grid3_ > $2

