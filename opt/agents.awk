BEGIN {
    FS=","
    farmers=0
    foresters=0
    biz=0
    res=0
}

/^[0-9]*,[0-9]*,1,/ {
    if (farmers == 0)
        print "\"farmer\": { \"default\": ["
    else
        print ","
    print "{ \"_id\":" $1 ","
    print "\"parcel_id\":" $2 "}"
    farmers++
}

/^[0-9]*,[0-9]*,[34],/ {
    if (foresters == 0)
    {
        print "]},"
        print "\"forester\": { \"default\": ["
    }
    else
        print ","
    print "{ \"_id\":" $1 ","
    print "\"parcel_id\":" $2 "}"
    foresters++
}

/^[0-9]*,[0-9]*,21,/ {
    if (biz == 0)
    {
        print "]},"
        print "\"urban_business\": { \"default\": ["
    }
    else
        print ","
    print "{ \"_id\":" $1 ","
    print "\"parcel_id\":" $2 "}"
    biz++
}

/^[0-9]*,[0-9]*,22,/ {
    if (res == 0)
    {
        print "]},"
        print "\"urban_residence\": { \"default\": ["
    }
    else
        print ","
    print "{ \"_id\":" $1 ","
    print "\"parcel_id\":" $2 "}"
    res++
}

END {
    print "]}"
#    print farmers "\t" foresters "\t" biz "\t" res
}
