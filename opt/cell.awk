BEGIN {
    FS=","
    idx = 0
    x = 0
    xmax = 315
    y = 0
    first = 1
}

// {
    if (first == 0)
    {
        print ","
    }
    else
    {
        first = 0
    }
    if (x >= xmax)
    {
        x = 0
        y++
    }
#    print "{ \"_id\":" idx++ ", \"land_cover\":" $1 ", \"parcel_id\":" $2 ", \"x\":" x++ ", \"y\":" y " },"
    print "{ \"_id\":" idx++ ", \"land_cover\":" $1 ", \"parcel_id\":" $2 " }"
}
