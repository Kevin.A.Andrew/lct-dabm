#serial 1
AC_DEFUN([KT_PROG_NVCC],
[AC_ARG_VAR([NVCC], [CUDA c compiler command])
AC_ARG_VAR([NVFLAGS], [CUDA c compiler flags])
AC_PATH_PROG([NVCC], [nvcc], [no])
AS_IF([test "x$NVCC" != "xno"],
    [AC_SUBST([HAS_NVCC], [$NVCC])
    AC_DEFINE([HAS_NVCC], [], [Defined if nvcc is found])])
])

AC_DEFUN([KT_PROG_DEVICEQUERY], [
    AC_PATH_PROG([DEVICEQUERY], [deviceQuery], [no])
    AS_IF([test "x$DEVICEQUERY" != "xno"],
        [AC_SUBST([HAS_DEVICEQUERY], [$DEVICEQUERY])
        AC_DEFINE([HAS_DEVICEQUERY], [], [Defined if deviceQuery is found])])
])

AC_DEFUN([KT_DEVICEQUERY_COMPUTE_CAPABILITY], [
AS_IF([test "x$USING_CUDA" == "xyes"], [
    AC_REQUIRE([KT_PROG_DEVICEQUERY])
    AS_IF([test -n "$HAS_DEVICEQUERY"],
    [AC_MSG_CHECKING([cuda compute capability])
    kt_compute_field=$($DEVICEQUERY | grep -e "CUDA Capability" | cut -d: -f2)
    AC_MSG_RESULT([$kt_compute_field])
    kt_compute_flag=$(head -n1 <<< $kt_compute_field | tr -d '.' | xargs)
    AC_SUBST([CUDA_CAPABILITY], [$kt_compute_flag])],
    [AC_MSG_WARN([No deviceQuery found, skipping CUDA configuration])])
])
])

AC_DEFUN([KT_CUDA_CONFIG_ARGS], [
AC_ARG_ENABLE([cuda],
    [AS_HELP_STRING([--disable-cuda],
                    [Disable CUDA components @<:@default: off@:>@])],
    [AS_IF([test "x$enable_cuda" = "xyes"],
           [AC_SUBST([USING_CUDA], ["yes"])],
           [AC_SUBST([USING_CUDA], ["no"])])],
    [AC_SUBST([USING_CUDA], ["yes"])])
])
