#serial 1
dnl KT_BANNER - Prints a message during configuration in a banner
dnl with a line of dashes above and below the message. Entire
dnl banner will be ignored if --quiet is specified.
AC_DEFUN([KT_BANNER], [
AC_MSG_NOTICE([=============================================================])
AC_MSG_NOTICE([     $1])
AC_MSG_NOTICE([=============================================================])
])

dnl KT_TPUT_WARN - Prints a warning message using tput to colorize, if available
AC_DEFUN([KT_TPUT_WARN], [
AS_IF([test "x`which tput`" != "x"],
      [AC_MSG_WARN([$(tput setaf 1)$1$(tput sgr0)])],
      [AC_MSG_WARN([$1])])
])

