#serial 1
AC_DEFUN([KT_FLAME_VARS], [
AC_ARG_VAR([FLAME_INPUT], [default input file for FLAMEGPU])
AC_ARG_VAR([FLAME_FLAGS], [runtime flags for FLAMEGPU])
AC_ARG_VAR([FLAME_WARN], [warnings log for FLAMEGPU])
])

AC_DEFUN([KT_FLAME_ARGS], [
AC_ARG_ENABLE([seatbelts],
    [AS_HELP_STRING([--disable-seatbelts],
                    [Disable SEATBELTS runtime checking,
                     may improve performance and reduce runtime
                     @<:@default: on@:>@])],
    [AS_IF([test "x$enableval" = "xno"],
           [AC_SUBST([SEATBELTS_FLAG], ["OFF"])],
           [AC_SUBST([SEATBELTS_FLAG], ["ON"])])],
    [AC_SUBST([SEATBELTS_FLAG], ["ON"])])
])

AC_DEFUN([KT_FLAME_CMAKEFLAGS], [
AS_IF([test -n "$CUDA_CAPABILITY"],
    [AC_SUBST([FLAME_CCAPFLAG], ["-DCUDA_ARCHITECTURES=$CUDA_CAPABILITY"])])
AC_SUBST([FLAME_CMAKEFLAGS],
    ["$FLAME_CCAPFLAG -DFLAMEGPU_SEATBELTS=$SEATBELTS_FLAG"])
])

AC_DEFUN([KT_FLAMEGPU], [
AC_REQUIRE([KT_CUDA_CONFIG_ARGS])
AC_REQUIRE([KT_FLAME_VARS])
AC_REQUIRE([KT_FLAME_ARGS])
AS_IF([test "x$USING_CUDA" == "xyes"], [
    AC_REQUIRE([KT_PROG_NVCC])
    AC_REQUIRE([KT_DEVICEQUERY_COMPUTE_CAPABILITY])
    KT_FLAME_CMAKEFLAGS
])
])
