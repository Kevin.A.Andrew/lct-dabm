# Tests for CMAKE
#serial 3
AC_DEFUN([KT_CMAKE_VARS], [
AC_ARG_VAR([CMAKEFLAGS], [cmake flags])
])

AC_DEFUN([KT_PROG_CMAKE],[
KT_CMAKE_VARS
AC_PATH_PROG([CMAKE], [cmake], [no])
AS_IF([test "x$CMAKE" != "xno"],
    [AC_DEFINE([HAS_CMAKE], [], [Defined if CMAKE has is found])])
])

AC_DEFUN([KT_CMAKE_BUILD_DIR], [
AS_IF([test -z "$1"],
[
    AC_MSG_NOTICE([setting cmake build dir... $srcdir/build])
    AC_SUBST([CMAKE_BUILD_DIR], [$srcdir/build])
],
[test -d "$1"],
[
    AC_MSG_NOTICE([setting cmake build dir... $1])
    AC_SUBST([CMAKE_BUILD_DIR], [$1])
],
[
    AC_MSG_NOTICE([setting cmake build dir...])
    AS_MKDIR_P([$1])
    AC_SUBST([CMAKE_BUILD_DIR], [$1])
    AS_IF([test -d "$1"],
    [
        AC_MSG_RESULT(["$1"])
    ],
    [
        AS_MKDIR_P([build])
        AC_SUBST([CMAKE_BUILD_DIR], [build])
        AC_MSG_RESULT([build])
        AC_MSG_WARN([failed to set desired build dir, using 'build' instead])
    ])
])])

AC_DEFUN([KT_CMAKE_CONFIG_DIRS],
[REQUIRE([KT_CMAKE_BUILD_DIR])])

AC_DEFUN([KT_CONFIG_CMAKEFLAGS],
[AS_IF([test -z "$CMAKE_BUILD_DIR"], [KT_CMAKE_BUILD_DIR])
AC_SUBST([CMakeCache_CMAKEFLAGS], ["-B $CMAKE_BUILD_DIR"])
AC_SUBST([CMakeClean_CMAKEFLAGS], ["--build $CMAKE_BUILD_DIR --target clean"])
AC_SUBST([CMakeBuild_CMAKEFLAGS], ["--build $CMAKE_BUILD_DIR"])
])

AC_DEFUN([KT_CONFIG_CMAKEMODES],
[
AC_SUBST([CMAKE_BUILD_MODES], [enabled])
AM_SUBST_NOTMAKE([CMAKE_BUILD_MODES])
AS_IF([test -d "$CMAKE_BUILD_DIR"], [
    AS_MKDIR_P(["$CMAKE_BUILD_DIR/bin/Release"])
    AS_MKDIR_P(["$CMAKE_BUILD_DIR/bin/Debug"])
])
AC_SUBST([Debug_CMAKEFLAGS], ["-DCMAKE_BUILD_TYPE=Debug"])
AC_SUBST([Release_CMAKEFLAGS], ["-DCMAKE_BUILD_TYPE=Release"])
])

AC_DEFUN([KT_CONFIG_CMAKELISTS],
[AS_IF([test -z "$1"],
    [AC_SUBST([CMAKELISTS], [CMakeLists.txt])],
    [test -f "$1"],
    [AC_SUBST([CMAKELISTS], ["$1"])],
    [AC_MSG_WARN([failed])])
])

AC_DEFUN([KT_CONFIG_CMAKERULES],
[
AS_IF([[test "x$CMAKE_BUILD_MODES" = "xenabled"]],
[[kt_cmake_build_rules='
$(CMAKE_BUILD_DIR)/CMakeCache.txt: $(CMAKELISTS)
	@$(CMAKE) $(CMakeCache_CMAKEFLAGS) $(MODEFLAGS) $(EXTRA_CMAKEFLAGS) \
	    $(CMAKEFLAGS) || echo "CMAKE FAILED"

.PHONY: cmake-cache-debug cmake-cache-release
cmake-cache-debug:
	@$(MAKE) $(CMAKE_BUILD_DIR)/CMakeCache.txt MODEFLAGS=$(Debug_CMAKEFLAGS)

cmake-cache-release:
	@$(MAKE) $(CMAKE_BUILD_DIR)/CMakeCache.txt MODEFLAGS=$(Release_CMAKEFLAGS)

## --------------------- ##
##  Build Rules          ##
## --------------------- ##
.PHONY: cmake-build cmake-build-debug cmake-build-release
cmake-build:
	@$(CMAKE) $(CMakeBuild_CMAKEFLAGS) --target $(CMAKE_TGT_MAIN) -- -j 4

cmake-build-debug: cmake-cache-debug cmake-build

cmake-build-release: cmake-cache-release cmake-build
']],
[[kt_cmake_build_rules='
$(CMAKE_BUILD_DIR)/CMakeCache.txt: $(CMAKELISTS)
	@$(CMAKE) $(CMakeCache_CMAKEFLAGS) $(EXTRA_CMAKEFLAGS) $(CMAKEFLAGS) || echo "CMAKE FAILED"

## --------------------- ##
##  Build Rules          ##
## --------------------- ##
.PHONY: cmake-build

cmake-build:
	@$(CMAKE) $(CMakeBuild_CMAKEFLAGS) $(CMAKEFLAGS) --target $(CMAKE_TGT_MAIN)
']])

AS_IF([[test "x$CMAKE" != "xno"]],
[[kt_cmake_rules='## --------------------- ##
##  Default CMake Rules  ##
## --------------------- ##
.PHONY: cmake-clean cmake-config
BUILT_SOURCES+=$(CMAKE_BUILD_DIR)/CMakeCache.txt

cmake-clean:
	@test ! -f $(CMAKE_BUILD_DIR)/CMakeCache.txt || $(CMAKE) $(CMakeClean_CMAKEFLAGS)

# FIXME: what
cmake-config: $(CMAKE_BUILD_DIR)/CMakeCache.txt

']],
[[kt_cmake_rules='']])
AC_SUBST([KT_CMAKE_RULES], [["$kt_cmake_rules$kt_cmake_build_rules"]])
AM_SUBST_NOTMAKE([KT_CMAKE_RULES])
])

AC_DEFUN([KT_INIT_CMAKE],
[AC_REQUIRE([KT_PROG_CMAKE])
AC_SUBST([CMAKE_TGT_MAIN], ["$1"])
AS_IF([test -n "$2"], [KT_PROG_CMAKE_VERSION(["$2"])])
AS_IF([test -n "$3"], [KT_CMAKE_BUILD_DIR(["$3"])])
AS_IF([test -n "$4"], [KT_CONFIG_CMAKEMODES(["$4"])])
AS_IF([test -n "$5"], [AC_SUBST([EXTRA_CMAKEFLAGS], ["$5"])])
KT_CONFIG_CMAKEFLAGS
KT_CONFIG_CMAKELISTS
KT_CONFIG_CMAKERULES
])

AC_DEFUN([KT_PROG_SORT_V],
[AC_MSG_CHECKING([for sort -V])
AS_IF([sort -V -C <<< EOF],
    [AC_MSG_RESULT([yes])
    AC_SUBST([SORT_V], ["sort -V"])
    AC_DEFINE([HAS_SORT_V], [], [Defined if sort -V is found])],
    [AC_MSG_RESULT([no])
    AC_SUBST([SORT_V], ["sort -n"])])
])

AC_DEFUN([KT_PROG_CMAKE_VERSION],
[AC_REQUIRE([KT_PROG_CMAKE])
AC_REQUIRE([KT_PROG_SORT_V])
AC_MSG_CHECKING([cmake version])
kt_cmake_version_min="$1"
AS_IF([test "x$CMAKE" != "xno"],
    [kt_cmake_version=`cmake --version | head -n1 | cut -d" " -f3`
     AC_SUBST([CMAKE_VERSION], [$kt_cmake_version])
     AC_MSG_RESULT([$kt_cmake_version])],
    [AC_MSG_RESULT([no])])
AC_MSG_CHECKING([cmake version >= $kt_cmake_version_min])
AS_IF([test "$CMAKE_VERSION" = "$kt_cmake_version_min"],
    [AC_MSG_RESULT([yes])],
    [test -n "$CMAKE_VERSION"],
    [kt_version_compare=`printf "%s\n%s" $kt_cmake_version_min $CMAKE_VERSION`
     kt_cmake_version_latest=`$SORT_V <<< $kt_version_compare | tail -n1`
     AS_IF([test "$kt_cmake_version_latest" != "$kt_cmake_version_min"],
        [AC_MSG_RESULT([yes])],
        [AC_MSG_RESULT([no])])
    ],
    [AC_MSG_RESULT([no])])
])
