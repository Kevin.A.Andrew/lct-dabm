// flameext.cuh - flame extensions library
/// @file   flameext.cuh
/// @brief  flamegpu extensions library
/// @author Kevin Andrew
/// @date   2023-02-06
#pragma once
#ifndef FLAMEEXT_FLAMEEXT_CUH_
#define FLAMEEXT_FLAMEEXT_CUH_

#include "flamegpu/flamegpu.h"

/*  This checks to make sure that the programs that are needed to build
 *  a model which makes use of these extensions are available.
 *  If they are not, then the build will fail cleanly.
 */
//#ifndef FLAMEEXT_VERSION
//#error "The file '" __FILE__ "' is missing needed support structures."
//#endif

/// Extended FLAMEGPU Functionality
/**
 * Most of the functionality in this namespace either extends the
 * functionality of FLAMEGPU in a way that is not strictly in compliance
 * with the FLAMEGPU specification @em de\ jure or in a way that requires
 * additional dependencies --- up to and including preprocessing outside of
 * capabilities of base @c nvcc.
 *
 */
namespace flameext {

#ifdef DEBUG
#define flameext_new_debug_flag(AGT, NAME) \
    AGT.newVariable<unsigned>(std::string("debug_") + std::string(#NAME), 0u);
#define flameext_raise_debug_flag(NAME) \
    FLAMEGPU->setVariable<unsigned>(std::string("debug_") + std::string(#NAME), 1u);
#else
#define flameext_new_debug_flag(AGT, NAME)
#define flameext_raise_debug_flag(NAME)
#endif

// Ideally, would have syntax closer to:
//     flameext::get_array<int, FMN>("network", id_of);
/// Gets an array from agent memory
/**
 * @note        Need to use this, since FLAME updated DeviceAgentAPI
 * @param VNAME The name of the new array
 * @param PNAME The name of the array in agent memory
 * @param TYPE  The type of the array
 * @param SIZE  The size of the array
 */
#define flameext_get_array(VNAME, PNAME, TYPE, SIZE) \
    std::array< TYPE , SIZE > VNAME; \
    for (size_t idx = 0; idx < SIZE; idx++) \
        VNAME[idx] = FLAMEGPU->getVariable< TYPE , SIZE >( #PNAME , idx);

#define flameext_get_env_array2(VNAME, PNAME, TYPE, SIZE) \
    std::array< TYPE , SIZE > VNAME; \
    for (size_t idx = 0; idx < SIZE; idx++) \
        VNAME[idx] = FLAMEGPU->environment.getProperty< TYPE , SIZE >( #PNAME , idx);

/// Gets an array from message memory
/**
 * @note        Need to use this, since FLAME updated DeviceAgentAPI
 * @param VNAME The name of the new array
 * @param PNAME The name of the array in agent memory
 * @param TYPE  The type of the array
 * @param SIZE  The size of the array
 */
#define flameext_rec_array(VNAME, PNAME, TYPE, SIZE) \
    std::array< TYPE , SIZE > VNAME; \
    for (size_t idx = 0; idx < SIZE; idx++) \
        VNAME[idx] = message.getVariable< TYPE , SIZE >( #PNAME , idx);

/// Stores an array in agent memory
/**
 * @note        Need to use this, since FLAME updated DeviceAgentAPI
 * @param VNAME The name of the new array
 * @param PNAME The name of the array in agent memory
 * @param TYPE  The type of the array
 * @param SIZE  The size of the array
 */
#define flameext_set_array(VNAME, PNAME, TYPE, SIZE) \
    for (size_t idx = 0; idx < SIZE; idx++) \
        FLAMEGPU->setVariable< TYPE , SIZE >( #PNAME , idx, VNAME[idx]);

#define flameext_get_item(ANAME, IDX, TYPE, SIZE) \
    FLAMEGPU->getVariable< TYPE , SIZE >( #ANAME , IDX )

#define flameext_aconfig(NAME, TYPE, SIZE) \
    TYPE , SIZE >( #NAME

#define TABX_AGE_OF flameext_aconfig(inv_ages, int, TABW_INV_BATCHES)

#define flameext_get_itemx(ACONFIG, IDX) \
    FLAMEGPU->getVariable< ACONFIG , IDX )

#define flameext_set_item(ANAME, IDX, VALUE, TYPE, SIZE) \
    FLAMEGPU->setVariable< TYPE , SIZE >( #ANAME , IDX , VALUE )

/// Puts an array in message memory
/**
 * @note        Need to use this, since FLAME updated DeviceAgentAPI
 * @param VNAME The name of the new array
 * @param PNAME The name of the array in agent memory
 * @param TYPE  The type of the array
 * @param SIZE  The size of the array
 */
#define flameext_send_array(VNAME, PNAME, TYPE, SIZE) \
    for (size_t idx = 0; idx < SIZE; idx++) \
        FLAMEGPU->message_out.setVariable< TYPE , SIZE >( #PNAME , idx, VNAME[idx]);

/// Gets an environmental variable.
/**
 * @param TYPE Data type of the variable in the environment.
 * @param NAME Name of the variable in the environment.
 */
#define ENV_GET(TYPE, NAME) FLAMEGPU->environment.getProperty<TYPE>(#NAME)

/// Sets an environmental variable.
/**
 * @param TYPE Data type of the variable in the environment.
 * @param NAME Name of the variable in the environment.
 * @param VALUE Value to set the variable to.
 */
#define ENV_SET(TYPE, NAME, VALUE) \
    FLAMEGPU->environment.setProperty<TYPE>(#NAME, VALUE)

/// Gets environmental variable
#define flameext_get_env(TYPE, NAME) \
    FLAMEGPU->environment.getProperty<TYPE>(#NAME)

/// Gets environmental array variable (full)
#define flameext_get_env_array(PNAME, TYPE, SIZE) \
    FLAMEGPU->environment.getProperty< TYPE , SIZE >( #PNAME )

/// Sets environmental array variable
#define flameext_set_env_array(NAME, TYPE, SIZE, VALUE) \
    FLAMEGPU->environment.setProperty< TYPE , SIZE >( #NAME , VALUE )

#define flameext__connect(FORMER, LATTER) \
    LATTER.dependsOn(FORMER);

typedef short signal;

};

#endif  // FLAMEEXT_FLAMEEXT_CUH_
