#pragma once

#define STTCAT(X)   "state" #X
#define NSTCAT(X)   "next_state" #X
#define ACTCAT(X)   "action" #X
#define RACTCAT(X)  "raction" #X
#define COVCAT(X)   "cover" #X

__host__
std::string
state_factor(const size_t& id)
{
    return std::string("state") + std::to_string(id);
}

__host__
std::string
action_factor(const size_t& id)
{
    return std::string("action") + std::to_string(id);
}

__host__
std::string
cover_pos(const size_t& id)
{
    return std::string("cover") + std::to_string(id);
}

__host__
std::string
neighbor_at(const size_t& id)
{
    return std::string("neighbor") + std::to_string(id);
}

typedef unsigned flame_bool;
const flame_bool flame_true = static_cast<unsigned>(true);
const flame_bool flame_false = static_cast<unsigned>(false);

