// flamerand.cuh - random number generation library for FLAMEGPU2
/**
 * @file   flamerand.cuh
 * @brief  Basic random number generation library for FLAMEGPU2.
 * @author Kevin Andrew
 * @date   2022-12-09
 */
#pragma once
#ifndef FLAMERAND_FLAMERAND_CUH_
#define FLAMERAND_FLAMERAND_CUH_

#include "flamegpu/flamegpu.h"
#include <curand.h>
#include <curand_kernel.h>
#include <cmath>
#include <exception>

/// Generates uniform random floating-point number on [0, 1]
#define FLAMERAND() FLAMEGPU->random.uniform<float>()

/// Macro to generate a random floating-point number within FLAME GPU.
#define FLAMERANDF(x, y) (FLAMEGPU->random.uniform<float>() * (y - x) + x)

/// Macro to generate a random double-precision number within FLAME GPU.
#define FLAMERANDD(x, y) (FLAMEGPU->random.uniform<double>() * (y - x) + x)

/// Macro to generate a random integer number within FLAME GPU.
#define FLAMERANDI(x, y) FLAMEGPU->random.uniform<int>(x, y)

/// Macro to generate a random long long integer number within FLAME GPU.
#define FLAMERANDL(x, y) FLAMEGPU->random.uniform<long long>(x, y)

/// @warning Deprecated: @c POISSON, use flamerand::poisson in all cases
#define _DEPRECATED_POISSON(L, U) \
    int p = 0, v = 0; float n = std::exp(-L), c = n; \
    while (u <= (c += n * (std::pow(L, p) / std::tgamma(p + 1)))) { \
        p++; \
    }

#define POISSON_NEW(VAR, L, U) \
    int VAR; { _DEPRECATED_POISSON(L, U); VAR = p; }

#define POISSON_ASG(VAR, L, U) \
    { _DEPRECATED_POISSON(L, U); VAR = p; }

// #pragma poison _DEPRECATED_POISSON

/// Safely generate a uniform random @c float between 0 and 1 for FLAMEGPU2.
#define U (FLAMEGPU->random.uniform<float>())

/// Safely generate a uniform random @c double between 0 and 1 for FLAMEGPU2.
#define UD (FLAMEGPU->random.uniform<double>())

/// Macro that transforms a distribution from uniform(0,1) to triangular.
/**
 * @param A The lower bound of the random number.
 * @param B The upper bound of the random number.
 * @param C The mode of the distribution.
 * @param U Random number between 0 and 1.
 */
#define TRIRAND(A, B, C, U) float u = U; \
                            (u < (C-A)/(B-A)) \
                            ? A + std::sqrt(u * (B-A) * (C-A)) \
                            : B + std::sqrt((1-u) * (B-A) * (B-C));


/// Macro that transforms a distribution from uniform(0,1) to eq-triangular.
/**
 * @param C The mode of the distribution.
 * @param D The standard deviation of the distribution.
 * @param U Random number between 0 and 1.
 */
#define ETRIRAND(C, D, U) TRIRAND((C-D), (C+D), C, U)


/// Namespace for random number generation within FLAME GPU.
namespace flamerand
{


__global__
void
initialize_seeds(const unsigned int& seed, curandState* states)
{
    const size_t id = threadIdx.x + blockIdx.x * blockDim.x;
    curand_init(seed, id, 0, &states[id]);
}


#if __cpp_exceptions
/// Class for reporting range errors in random number generation.
class range_error : public std::range_error
{
public:
    /// Creates a new @c range_error.
    explicit
    range_error(const std::string& what_arg)
    : std::range_error(what_arg)
    {}

    /// Creates a new @c range_error.
    explicit
    range_error(const char* what_arg)
    : std::range_error(what_arg)
    {}
};
#endif


/// Wrapper for U and UD macros to minimize code contamination.
template <typename Number>
    __host__ __device__ __forceinline__
    Number
    uniform(Number number)
    {
      /// @todo: Find FLAMEGPU-safe way to validate bounds.
      return number;
    }


/**
 * Transforms a random number from a uniform distribution on [0, 1] to a
 * triangular distribution on [a, b].
 *
 * @param a Lower bound of the triangular distribution.
 * @param b Upper bound of the triangular distribution.
 * @param c Mode of the triangular distribution.
 * @param u Random number between 0 and 1.
 * @return The transformed random number.
 */
__host__ __device__ __forceinline__
float
triangle(float a, float b, float c, float u)
{
    if (u < (c - a)/(b - a))
        return a + std::sqrt(u * (b - a) * (c - a));
    else
        return b - std::sqrt((1 - u) * (b - a) * (b - c));
}

/**
 * Transforms a random number from a uniform distribution on [0, 1] to a
 * triangular distribution on [c - d, c + d].
 *
 * @param c Mode of the triangular distribution.
 * @param d Half-width of the triangular distribution.
 * @param u Random number between 0 and 1.
 * @return The transformed random number.
 */
__host__ __device__ __forceinline__
float
triangle(float c, float d, float u)
{
    return triangle(c - d, c + d, c, u);
}


/// Generate a random number from a uniform distribution between min and max.
/**
 * @param min The lower bound of the random number.
 * @param max The upper bound of the random number.
 * @param u A random number between 0 and 1.
 * @returns A random number between min and max.
 */
template <typename T>
    __host__ __device__ __forceinline__
    T
    uniform(T min, T max, float u)
    {
        return u * (max - min) + min;
    }


/// Generate a random number from a Poisson distribution.
/**
 * @param L The mean of the distribution.
 * @param u A random number between 0 and 1.
 * @returns A random number from a Poisson distribution.
 */
__host__ __device__ __forceinline__
unsigned int
poisson(float L, float u)
{
    unsigned int p = 0;
    float n = std::exp(-L);
    float c = n;
    while (u <= (c += n * (std::pow(L, p) / std::tgammaf(p + 1))))
    {
        p++;
    }
    return p;
}

/// Categorical distribution.
/**
 * @tparam T The type of the bin weights.
 * @param categories The number of categories.
 * @returns The index of the category.
 */
template <typename T>
    __host__ 
    size_t
    categorical(unsigned categories, ...)
    {
        T* weights = new T[categories];
        unsigned sum = 0;
        va_list args;
        va_start(args, categories);
        for (unsigned i = 0; i < categories; i++)
        {
            T w = va_arg(args, unsigned);
            weights[i] = w;
            sum += w;
        }
        va_end(args);

        // TODO: FIX
        int r = rand() % sum;
        size_t i = 0;
        do
        {
            r -= weights[i];
            i++;
        } while (r > 0);
        delete[] weights;
        return i;
    }

/**
 * @param p Value to be tested.
 * @return True if p is truthy, false otherwise.
 */
__host__ __device__ __forceinline__
bool
test(int p)
{
    return (p >= 1);
}

/**
 * @param p Value to be tested.
 * @return True if p is truthy, false otherwise.
 */
__host__ __device__ __forceinline__
bool
test(float p)
{
    return (p > 0.0f);
}

/**
 * @param p The probability of success.
 * @param u Random number between 0 and 1.
 * @return Success if u < p, failure otherwise.
 */
__host__ __device__ __forceinline__
bool
test_lt(float p, float u)
{
   return (u < p);
}

__host__ __device__ __forceinline__
bool
test_gt(float p, float u)
{
  return (u > p);
}
  
__host__ __forceinline__
size_t*
shuffled(size_t n)
{
    size_t* indices = new size_t[n];
    for (size_t i = 0; i < n; i++)
    {
        indices[i] = i;
    }
    for (size_t i = 0; i < n; i++)
    {
        size_t j = i + (rand() % (n - i));
        size_t t = indices[i];
        indices[i] = indices[j];
        indices[j] = t;
    }
    return indices;
}

};

#endif
