// flamednn.cuh - 
//
/**
 * @file flamednn.cuh
 * @author Kevin Andrew
 */
#pragma once
#ifndef FLAMEDNN_FLAMEDNN_CUH_
#define FLAMEDNN_FLAMEDNN_CUH_

#define FLAMEDNN_FUNCTION(NAME) \
    FLAMEGPU_HOST_FUNCTION(NAME)

#ifdef _CONFIG_H_
#include "config.h"
#endif

#include "flamelog.h"

#include <sstream>

#define FatalError(s) {                                                \
    std::stringstream _where, _message;                                \
    _where << __FILE__ << ':' << __LINE__;                             \
    _message << std::string(s) + "\n" << __FILE__ << ':' << __LINE__;\
    std::cerr << _message.str() << "\nAborting...\n";                  \
    cudaDeviceReset();                                                 \
    exit(EXIT_FAILURE);                                                \
}

#ifdef DEBUG
#define checkCudaErrors(status) {                                      \
    std::stringstream _error;                                          \
    if (status != 0) {                                                 \
      _error << "Cuda failure\nError: " << cudaGetErrorString(status); \
      FatalError(_error.str());                                        \
    }                                                                  \
}
#else
#define checkCudaErrors(status) status
#endif

#include <exception>

#include <cuda_runtime.h>
#include "cudnn.h"
#include "cudnn_frontend.h"
// where is cudnn_frontend_utils? is it installed with cudnn?
#include <cudnn_frontend_utils.h>


namespace flamednn
{

typedef size_t CUDADevice;
typedef cudaDeviceProp DeviceProperties;
typedef cudnnHandle_t Handle;
typedef cudnnTensorDescriptor_t Tensor;
typedef cudnnConvolutionDescriptor_t ConvolutionDescriptor;
typedef cudnnActivationDescriptor_t ActivationDescriptor;
typedef cudnnTensorDescriptor_t TensorDescriptor;
typedef cudnnFilterDescriptor_t FilterDescriptor;

typedef cudnnStatus_t CUDAStatus;
typedef cudnnDataType_t CUDAType;
typedef cudnnTensorFormat_t CUDAFormat;

/// Exception class for library CUDA errors
class flamednn_exception : public std::runtime_error
{
protected:
    CUDAStatus error_status;
public:
    flamednn_exception(const char* message, CUDAStatus status) throw()
    :std::runtime_error(message), error_status(status)
    {}

    virtual const char*
    what() const throw()
    {
        return std::runtime_error::what();
    }

    CUDAStatus
    status()
    {
        return error_status;
    }
};

bool using_cuda_device;
size_t cuda_device = 1;
size_t flame_device = 0;

__host__ __inline__ void
toggle_device()
{
    if (using_cuda_device)
    {
//        flamelog::debug("switched to FLAME (%zu)", flame_device);
        cudaSetDevice(flame_device);
        using_cuda_device = false;
    }
    else
    {
//        flamelog::debug("switched to cuDNN (%zu)", cuda_device);
        cudaSetDevice(cuda_device);
        using_cuda_device = true;
    }
}

__host__ __inline__ void
set_flame_device(bool end=true)
{
    if (using_cuda_device and end)
    {
        toggle_device();
    }
    else if (not using_cuda_device and not end)
    {
        toggle_device();
    }
}

/// Print information about available CUDA devices.
__host__ __inline__
void
print_device_info(const size_t& device_id = 0)
{
    int totalDevices;
    checkCudaErrors(cudaGetDeviceCount( &totalDevices ));
    printf("\nThere are %d CUDA capable devices on your machine :\n", totalDevices);
    for (int i=0; i< totalDevices; i++) {
        struct cudaDeviceProp prop;
        checkCudaErrors(cudaGetDeviceProperties( &prop, i ));
        printf( "device %d : sms %2d  Capabilities %d.%d, SmClock %.1f Mhz, MemSize (Mb) %d, MemClock %.1f Mhz, Ecc=%d, boardGroupID=%d\n",
                    i, prop.multiProcessorCount, prop.major, prop.minor,
                    (float)prop.clockRate*1e-3,
                    (int)(prop.totalGlobalMem/(1024*1024)),
                    (float)prop.memoryClockRate*1e-3,
                    prop.ECCEnabled,
                    prop.multiGpuBoardGroupID);
    }
}

/// Initialize the CUDA runtime, set the device, and create a handle.
__host__ __inline__
void
init(Handle& handle, const size_t& device_id = 0, const size_t& flame_device_id = 0)
{
    int num_devices = 0;
    cudaGetDeviceCount(&num_devices);
    if (device_id >= static_cast<unsigned>(num_devices))
    {
        fprintf(stderr, "Invalid device id: %ld\n", device_id);
        throw flamednn_exception("Invalid device id.",
                                 CUDAStatus::CUDNN_STATUS_BAD_PARAM);
    }

    using_cuda_device = true;
    cuda_device = device_id;
    flame_device = flame_device_id;
    cudaSetDevice(device_id);
    cudnnCreate(&handle);
    flamelog::debug("Created FLAMEGPU2 CUDA handle for device (%ld).", flame_device_id);
    flamelog::debug("Created cuDNN handle for device (%ld).", device_id);
}

/// Destroy the cuDNN handle.
__host__ __inline__
void
destroy(Handle& handle)
{
    cudnnDestroy(handle);
    fprintf(stdout, "Destroyed cuDNN handle.\n");
}

// n - batch size / number of data samples
// c - channels (f.x. RGB image has 3 channels)
// h - height of sample (f.x. pixel height of image)
// w - width of sample (f.x. pixel width of image)

/// Create a tensor descriptor for a 4D tensor.
__host__ __inline__
void
cuda_tensor(Tensor* tensor,
            const size_t& n, const size_t& c, const size_t& h, const size_t& w,
            const CUDAType& type = CUDNN_DATA_FLOAT,
            const CUDAFormat& format = CUDNN_TENSOR_NCHW)
{
    size_t elements = n * c * h * w;
    cudnnCreateTensorDescriptor(tensor);
    cudnnSetTensor4dDescriptor(*tensor, format, type, n, c, h, w);
}

/// Create an array of managed memory on the GPU and return a pointer to it.
template <typename T>
    __host__ __inline__
    T*
    cuda_array(const size_t& size)
    {
        T* array = nullptr;
        cudaMallocManaged(&array, size * sizeof(T));
        return array;
    }

/// Create an array of managed memory on the GPU with the given pointer.
template <typename Type>
    __host__ __inline__
    void
    cuda_array(Type* array, const size_t& size)
    {
        cudaMallocManaged(&array, size * sizeof(Type));
    }

template <typename Type>
    __host__ __inline__
    void
    cuda_free(Type* ptr)
    {
        cudaFree(ptr);
    }

template <typename Type>
    void
    memcpy_device(Type* tgt, Type* src, size_t count)
    {
        checkCudaErrors(cudaMemcpy(tgt, src, sizeof(Type)*count,
            cudaMemcpyDeviceToDevice));
    }

template <typename Type>
    void
    sync_device(Type* dev, Type* host, size_t count)
    {
        checkCudaErrors(cudaMemcpy(dev, host, sizeof(Type) * count,
            cudaMemcpyHostToDevice));
    }

template <typename Type>
    void
    sync_host(Type* dev, Type* host, size_t count)
    {
        checkCudaErrors(cudaMemcpy(host, dev, sizeof(Type) * count,
            cudaMemcpyDeviceToHost));
    }

template <typename Type, size_t N, size_t C, size_t H=1>
    __host__
    void
    create_tensor(TensorDescriptor& tensor, Type** dev, Type** host)
    {
        cudnnCreateTensorDescriptor(&tensor);
        cudnnSetTensor4dDescriptor(tensor, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT,
            N, C, H, 1);
        cudaMalloc(dev, N * C * H * sizeof(Type));
        *host = (Type*)malloc(sizeof(Type) * N * C * H);
    }

__host__
void
create_simple_layer(FilterDescriptor& filter, const size_t& from, const size_t& to)
{
    cudnnCreateFilterDescriptor(&filter);
    cudnnSetFilter4dDescriptor(filter, CUDNN_DATA_FLOAT, CUDNN_TENSOR_NCHW, to, from, 1, 1);
}

__host__
void
create_simple_layer_weights(float** dev, float** host, const size_t& number, const size_t& mult = 1)
{
    *host = (float*)malloc(sizeof(float) * number * mult);
    cudaMalloc(dev, number * sizeof(float) * number * mult);
}

__host__
void
print_first_3n(const std::string& name, float* host)
{
    fprintf(stderr, "%s=[%f, %f, %f, ...]\n",
        name.c_str(), host[0], host[1], host[2]);
}

__host__
float*
read_network_weights(const std::string& filename, const size_t& size)
{
    std::ifstream file(filename.c_str(), std::ios::binary);
    if (file.is_open())
    {
        float* weights = new float[size];
        file.read(reinterpret_cast<char*>(weights), size * sizeof(float));
        file.close();
        return weights;
    }
#ifdef DEBUG
    else
    {
        fprintf(stderr, "Unable to read from file `%s'\n", filename.c_str());
    }
#endif
    return nullptr;
}

__host__ void
write_weights_to_file(float* weights, const size_t& size, const std::string& filename)
{
    std::ofstream file(filename.c_str(), std::ios::binary);
    if (file.is_open())
    {
        file.write(reinterpret_cast<char*>(weights), size * sizeof(float));
        file.close();
#ifdef DEBUG
        flamelog::debug("writing weights to file `%s'", filename.c_str());
    }
    else
    {
        flamelog::error("unable to write to file `%s'", filename.c_str());
#endif
    }
}

__host__
void
append_weights_to_file(float* weights, const size_t& size,
    const std::string& filename)
{
    std::ofstream file(filename.c_str(), std::ios::binary|std::ios::app);
    if (file.is_open())
    {
        file.write(reinterpret_cast<char*>(weights), size * sizeof(float));
        file.close();
    }
#ifdef DEBUG
    else
    {
        fprintf(stderr, "Unable to write to file `%s'\n", filename.c_str());
    }
#endif
}

__host__
void
appendWeightsToFile(float* weights, const size_t& size, const std::string& filename)
{
    std::ofstream file(filename.c_str(), std::ios::binary|std::ios::app);
    if (file.is_open())
    {
        file.write(reinterpret_cast<char*>(weights), size * sizeof(float));
        file.close();
    }
#ifdef DEBUG
    else
    {
        fprintf(stderr, "Unable to write to file `%s'\n", filename.c_str());
    }
#endif
}

__host__
void
malloc(float** dev_data, size_t size)
{
    checkCudaErrors(cudaMalloc(dev_data, size));
}

__global__
void
compute_transfer_result(float* actor, float* target, float tau, float* output)
{
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    output[idx] = target[idx] + tau * (actor[idx] - target[idx]);
}

};  // namespace flamednn

#endif  //FLAMEDNN_FLAMEDNN_H_
