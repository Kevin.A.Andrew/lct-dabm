#pragma once
#ifndef FLAMEABM_LOG_CUH_
#define FLAMEABM_LOG_CUH_

namespace flameabm
{

namespace log
{
void info(const char* format, ...);
void warn(const char* format, ...);
void error(const char* format, ...);
void debug(const char* format, ...);
void fatal(const char* format, ...);
};

};

#endif /* FLAMEABM_LOG_CUH_ */
