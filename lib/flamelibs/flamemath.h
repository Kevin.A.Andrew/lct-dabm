// flamemath.cuh -
/**
 * @file  flamemath.cuh
 */
#pragma once
#ifndef FLAMEMATH_FLAMEMATH_CUH_
#define FLAMEMATH_FLAMEMATH_CUH_

#include <cmath>

/// An expression that produces the cartisian distance between two points
#define DIST(x, y, x1, y1) \
    (std::sqrt(std::pow(x - x1, 2) + std::pow(y - y1, 2)))

/// Computes a logistic function at a given point with params
#define LOGISTIC(X, K, X0) (1.0 / (1.0 + std::exp(K * (X - X0))))

/// flamemath namespace
namespace flamemath
{

/// Compute the cartisian distance between two points
template <typename Number>
    __host__ __device__ __forceinline__
    Number
    cartesian_distance(Number x0, Number y0, Number x1, Number y1)
    {
        return std::sqrt(std::pow(x0 - x1, 2) + std::pow(y0 - y1, 2));
    }

/// Compute the geodesic distance between two lat/lon coordinates
template <typename Number>
    __host__ __device__ __forceinline__
    Number
    geodesic_distance(Number lat0, Number lon0, Number lat1, Number lon1)
    {
        return std::acos(std::sin(lat0) * std::sin(lat1) +
                         std::cos(lat0) * std::cos(lat1) *
                         std::cos(lon1 - lon0));
    }

/// Logistic function.
template <typename Number>
#if __cplusplus >= 201710L
requires std::floating_point<Number>
#endif
    __host__ __device__ __forceinline__
    Number
    logistic(Number x, Number k, Number x0)
    {
        return 1.0 / (1.0 + std::exp(k * (x - x0)));
    }

/// Gets the minimum of two values.
template <typename Number>
    __host__ __device__ __forceinline__
    Number
    min(Number a, Number b)
    {
        return a < b ? a : b;
    }

template <typename Number>
    __host__ __device__ __forceinline__
    Number
    min(Number a, ...)
    {
        va_list args;
        va_start(args, a);
        Number min = a;
        Number next = va_arg(args, Number);
        while (next != 0)
        {
            min = min < next ? min : next;
            next = va_arg(args, Number);
        }
        va_end(args);
        return min;
    }

/// Get the maximum of two values.
template <typename Number>
    __host__ __device__ __forceinline__
    Number
    max(Number a, Number b)
    {
        return a > b ? a : b;
    }

__host__ __device__ __forceinline__
float
onehot(float a, float b)
{
    return (a >= b) ? 0 : 1;
}

__host__ __device__ __forceinline__
float
onehot(float a, float b, float c)
{
    if (a >= b and a >= c)
        return 0;
    if (b >= a and b >= c)
        return 1;
    return 2;
}

}; // namespace flamemath

#endif
