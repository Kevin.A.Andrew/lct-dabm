#pragma once
#ifndef FLAMELIBS_FLAMEPY_H_
#define FLAMELIBS_FLAMEPY_H_
#define PY_SSIZE_T_CLEAN
#include <Python.h>

namespace flamepy
{

void
init()
{
    Py_Initialize();
    PyRun_SimpleString("print('Python Linked')");
}

void
finalize()
{
    Py_FinalizeEx();
}

};

#endif /* FLAMEPY_H */
