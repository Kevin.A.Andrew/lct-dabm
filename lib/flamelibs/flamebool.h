#pragma once
#ifndef FLAMELIBS_FLAMEBOOL_H_
#define FLAMELIBS_FLAMEBOOL_H_

namespace flameabm
{
typedef unsigned int bool_t;
static const bool_t true_t = 1;
static const bool_t false_t = 0;
}; // namespace flameabm

#endif // FLAMELIBS_FLAMEBOOL_H_
