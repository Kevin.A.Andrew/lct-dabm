#pragma once
#ifndef FLAMEABM_FLAMEABM_CUH_
#define FLAMEABM_FLAMEABM_CUH_

#ifdef _CONFIG_H_
#include "config.h"
#endif

#include "flameabm/flameabm_log.cuh"

namespace flameabm
{

extern const char * model_name;

};

#endif /* FLAMEABM_FLAMEABM_CUH_ */
