#pragma once
#ifndef FLAMELIBS_FLAMELOG_H_
#define FLAMELIBS_FLAMELOG_H_

#ifndef FLAMELOG_COLOR_OUTPUT
#define FLAMELOG_COLOR_OUTPUT true
#endif

#ifndef FLAMELOG_PADDING
#define FLAMELOG_PADDING 0u
#endif

#ifndef FLAMELOG_PRINT_INFO
#ifdef DEBUG
#define FLAMELOG_PRINT_INFO true
#endif
#endif

#ifndef FLAMELOG_PRINT_DEBUG
#ifdef DEBUG
#define FLAMELOG_PRINT_DEBUG true
#endif
#endif

#ifndef FLAMELOG_FILE
#define FLAMELOG_FILE stdout
#endif

#ifndef FLAMELOG_FILE_ERR
#define FLAMELOG_FILE_ERR stderr
#endif

#define FLAMELOG_FILE_INFO FLAMELOG_FILE
#define FLAMELOG_FILE_DEBUG FLAMELOG_FILE
#define FLAMELOG_FILE_WARN FLAMELOG_FILE_ERR
#define FLAMELOG_FILE_ERROR FLAMELOG_FILE_ERR

#define FLAMELOG_PREFIX_VERBOSE "[VERBS] "
#define FLAMELOG_PREFIX_BRIEF   "[BRIEF] "
#define FLAMELOG_PREFIX_INFO    "[INFO ] "
#define FLAMELOG_PREFIX_DEBUG   "[DEBUG] "
#define FLAMELOG_PREFIX_WARN    "[WARN ] "
#define FLAMELOG_PREFIX_ERROR   "[ERROR] "


namespace flamelog
{

enum channel
{
Info,
Default,
Debug,
Warning,
Error
};

const char* black = "\x1B[0m";
const char* blue = "\x1B[34m";
const char* yellow = "\x1B[33m";
const char* red = "\x1B[31m";

void
print_padding(const unsigned& padding=FLAMELOG_PADDING)
{
    for (unsigned line = 0; line < padding; ++line)
        fprintf(FLAMELOG_FILE, "\n");
}

void set_color(const flamelog::channel& ch)
{
#ifdef FLAMELOG_COLOR_OUTPUT
    if (not FLAMELOG_COLOR_OUTPUT)
        return;
#endif
    switch (ch)
    {
        case Info:
            [[fallthrough]];
        case Default:
            fprintf(FLAMELOG_FILE, "%s", black);
            break;
        case Debug:
            fprintf(FLAMELOG_FILE_DEBUG, "%s", blue);
            break;
        case Warning:
            fprintf(FLAMELOG_FILE_WARN, "%s", yellow);
            break;
        case Error:
            fprintf(FLAMELOG_FILE_ERROR, "%s", red);
            break;
    };
};

void
info(const char* format, ...)
{
#ifdef FLAMELOG_PRINT_INFO
    va_list args;
    va_start(args, format);
    flamelog::print_padding();
    flamelog::set_color(Info);
    fprintf(FLAMELOG_FILE_INFO, FLAMELOG_PREFIX_INFO);
    vfprintf(FLAMELOG_FILE_INFO, format, args);
    fprintf(FLAMELOG_FILE_INFO, "\n");
    flamelog::set_color(Default);
    flamelog::print_padding();
    fflush(FLAMELOG_FILE_INFO);
    va_end(args);
#endif
}

void
debug(const char* format, ...)
{
#ifdef FLAMELOG_PRINT_DEBUG
    va_list args;
    va_start(args, format);
    flamelog::print_padding();
    flamelog::set_color(Debug);
    fprintf(FLAMELOG_FILE_DEBUG, FLAMELOG_PREFIX_DEBUG);
    vfprintf(FLAMELOG_FILE_DEBUG, format, args);
    fprintf(FLAMELOG_FILE_DEBUG, "\n");
    flamelog::set_color(Default);
    flamelog::print_padding();
    fflush(FLAMELOG_FILE_DEBUG);
    va_end(args);
#endif
}

void
warn(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    flamelog::print_padding();
    flamelog::set_color(Warning);
    fprintf(FLAMELOG_FILE_WARN, FLAMELOG_PREFIX_WARN);
    vfprintf(FLAMELOG_FILE_WARN, format, args);
    fprintf(FLAMELOG_FILE_WARN, "\n");
    flamelog::set_color(Default);
    flamelog::print_padding();
    fflush(FLAMELOG_FILE_WARN);
    va_end(args);
}

void
warn_once(bool* flag, const char* format, ...)
{
    if (*flag) return;
    *flag = true;
    va_list args;
    va_start(args, format);
    warn(format, args);
    va_end(args);
}

void
error(const char* format, ...)
{
    va_list args;
    va_start(args, format);
    flamelog::print_padding();
    flamelog::set_color(Error);
    fprintf(FLAMELOG_FILE_ERROR, FLAMELOG_PREFIX_ERROR);
    vfprintf(FLAMELOG_FILE_ERROR, format, args);
    fprintf(FLAMELOG_FILE_ERROR, "\n");
    flamelog::set_color(Default);
    flamelog::print_padding();
    fflush(FLAMELOG_FILE_ERROR);
    va_end(args);
}

void
error_once(bool* flag, const char* format, ...)
{
    if (*flag) return;
    *flag = true;
    va_list args;
    va_start(args, format);
    error(format, args);
    va_end(args);
}

};

#endif
