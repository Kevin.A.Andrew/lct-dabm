**Outstanding Issues**
* Need to finish stepwise model output of land-cover rasters
* Need to finish weight specs so it's not using hard-coded values
 * same issue with state-files, but lower priority
* Need to finished documenting model and source
* Need to clean up paper draft and get it all together
* Model Configuration Details (MORE THAN ORIG)

# Current Status

* The Simple Running Model is here in the `main` branch, although it's missing a few big pieces

* I'm getting the rest of the files cleaned up as fast as I can; the version in here right now should:
  * Build and Run
  * Generate Basic Output (network weights and land-cover rasters in the correct format)

* Still Needs
  * Reading weights files without having them hard-coded
  * Actually outputting on each model step / year --- should these be going into a fifo pipe/socket or ... (?)
  * Double-check all agents and params to make sure they're in line with the ODD+D and model as I was running it in 2020/2021 before the big loss

# Getting Started

## Prerequisites

The following programs are required for building the model.

* `cmake 3.18+` CMake Build Tool
* `ncvv11+` CUDA C Compiler
* `c++17+` compatible C Compiler

Optional Dependencies:
* `python 3+`, `gawk` - currently used for converting between land-cover rasters and FLAMEGPU state-files
* `jq`, `check-json` used for doing some optional JSON validation and checking

**Note** On `epscor-pascal`, everything necessary is installed in the `tensorflow` container.

## Building

### My Setup

1. First Build
    ```bash
    git clone git@gitlab.uvm.edu:Kevin.A.Andrew/lct-dabm.git && cd lct-dabm
    git submodule update --init && ./bootstrap --force
    tensorflow
    ./configure && make flame-model
    ```
2. Following Builds
    ```bash
    make flame-model
    ```
3. Runs
    ```bash
    # Direct
    ./bin/lct-dabm --in [FILE] --steps [STEP-COUNT]
    # With Helper Script
    ./bin/run.sh --input [FILE] --output [FILE] --training --episode-count [NUM_EPS] --episode-length [NUM_STEPS]
    # OR (TODO needs config)
    ./bin/run.sh --input [FILE] --output [FILE] --testing
    ```

### The First Build

1. Clone and enter the repository.
    ```bash
    git clone git@gitlab.uvm.edu:Kevin.A.Andrew/lct-dabm.git && cd lct-dabm
    ```
2. Update remote sources and boostrap the build system.
    ```bash
    git submodule update --init
    ./bootstrap --force
    ```
3. Configure for build / runtime environment
    ```bash
    # For me, on pascal, I need to enter the tensorflow container here
    # This call isn't needed in all environments.
    tensorflow
    ./configure && make flame-model
    ```

### Building the Model with `make`

After the build system has been configured with autotools, it can be built/re-built at
any time with `make` with the following commands.

**Debug Target** Slightly slower runtime and larger executable — will spit out errors.
```bash
make flame-model
```

**Release Target** Runs faster, with slightly longer build-time — no warnings, errors, or messages.
```bash
make cmake-release
```

## Running the Model

The model can be invoked in two ways:
* `bin/run.sh` helper script which handles some processing
* `bin/lct-dabm` directly invoke executable with FLAMEGPU config

### Using `run.sh`

The model has two main operating modes: *training* and *testing*.

The mode will be selected with the following precedence:
* mode specified by command line argument
* mode specified in input state-file
* training mode

### Training Mode

When the model is started in training mode, network weights will be initialized at random
and will run *from scratch*. After the run is completed, network weights will be saved
in the `weights` directory.

```bash
./bin/run.sh --training --input [INPUT] --output [OUTPUT] --step [NUMBER]
```

**Warning** Network weights will be saved to fixed file names in the
`checkpoint/` and `weights/` directories, so it's very easy for things to be overwritten on accident.

### Testing Mode

Once network weights have been trained, the model can load those weights for
a testing run as follows:

```bash
./bin/run.sh --testing --input [INPUT] --output [OUTPUT] --step [NUMBER]
```

Testing mode runs the model for a single episode environment with pre-trained
model weights. **TODO** Write more about operating modes, the differences,
and model configuration at rutime.

**Note** Right now, there isn't a way to specify which network weights
to load. Fixed names will be checked in the `weights` directory, and
if they aren't found, model behavior is *undefined*.

# Important Files
## Code Files
* `include/config.h`, `include/defaults.h` high level model configuration
* `include/agent_config.h`, `include/flamegpu_config.h` configuration of agent parameters
* `include/learning_config.h`, `include/netconfig[tgt].h` configuration of machine learning parameters
* `aux/config.json` configuring *special* variables before runtime
* `include/learning/` learning functionality code
* `include/model/` model action functionality

## Data Files
* `data/sample/a5-step-0.json` - sample model input file, generated by

* `data/sample/a5-step-0.weights` - sample model weights file
* `data/sample/a5-step-6000.json` - sample model output
* `data/sample/a5.agents` - sample input agent raster
* `data/sample/a5.parcels` - sample input parcel raster

## Executables / Scripts
* `bin/lct-dabm` - main model executable
* `bin/run.sh` - helper script
* `aux/flame2raster.sh` - converts a FLAMEGPU state-file to CSV land-cover raster
* `aux/raster2flame.sh` - converts raster spec files into FLAMEGPU model input

## Directories
* `checkpoints/` - directory where model training checkpoints are saved
* `weights/` - directory where final model training weights are saved

# Model Configuration
