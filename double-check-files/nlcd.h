#pragma once
#define NLCD_WATER          11
#define NLCD_URBAN_OPEN     21
#define NLCD_URBAN_LOW      22
#define NLCD_URBAN_MEDIUM   23
#define NLCD_URBAN_HIGH     24
#define NLCD_BARREN         31
#define NLCD_FOREST_DEC     41
#define NLCD_FOREST_CON     42
#define NLCD_FOREST_MIX     43
#define NLCD_SCRUB          52
#define NLCD_GRASSLAND      71
#define NLCD_PASTURE        81
#define NLCD_CROPS          82
#define NLCD_WOODY_WETLANDS 90
#define NLCD_WETLANDS       95

typedef unsigned nlcd_t;
namespace nlcd {
const nlcd_t water = 11;
const nlcd_t urban_open = 21;
const nlcd_t urban_low = 22;
const nlcd_t urban_medium = 23;
const nlcd_t urban_high = 24;
const nlcd_t barren = 31;
const nlcd_t forest_dec = 41;
const nlcd_t forest_con = 42;
const nlcd_t forest_mix = 43;
const nlcd_t scrub = 52;
const nlcd_t grassland = 71;
const nlcd_t pasture = 81;
const nlcd_t crops = 82;
const nlcd_t woody_wetlands = 90;
const nlcd_t wetlands = 95;
};

