# FILES THAT NEED CONFIGURED

## Input-Specific Parameters
These parameters need to be externalized;
right now they are hard-coded and have to
be changed by hand for each study-area.

|Parameter|Source|Description|
|---|---|---|
|`FARM_NUM`|`defaults.h`| Number of Farmer Agents|
|`FOR_NUM`|`defaults.h`|Number of Forester Agents|
|`RES_NUM`|`defaults.h`|Number of Resident Agents|
|`BIZ_NUM`|`defaults.h`|Number of Commercial Agents|
|`MIN_PID`|`defaults.h`|Min Parcel ID Number|
|`MAX_PID`|`defaults.h`|Max Parcel ID Number|

## Run-Specific Parameters
These parameters are hard-coded,
but not linked to the input data.

|Parameter|Source|Description|
|---|---|---|
|`FARM_MAX_NEIGHBORS`|`defaults.h`|Size of Farm Networks|
|`FOREST_MAX_NEIGHBORS`|`defaults.h`|Size of Forester Networks|
|`MAX_EMPLOYEES`|`defaults.h`|Maximum Business Size|
|`NETCONFIG_FILE`|`netconfig.h`|ANN Configuration Def|
|`NETCONFIGTGT_FILE`|`netconfigtgt.h`|Duplicate to `netconfig`, but for Target Networks


