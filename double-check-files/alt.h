
namespace networks
{
struct mu
{
    const size_t in[4] = {31, 4, 1, 1};
    const size_t l1[4] = {31, 5, 1, 1};
    const size_t l2[4] = {31, 5, 1, 1};
    const size_t out[4] = {31, 6, 1, 1};

    float* in_data;
    float* in_data_dev;
    float* l1_data;
    float* l1_data_dev;
    float* l2_data;
    float* l2_data_dev;
    float* out_data;
    float* out_data_dev;

    cudnnTensorDescriptor_t in_desc;
    cudnnTensorDescriptor_t l1_desc;
    cudnnTensorDescriptor_t l2_desc;
    cudnnTensorDescriptor_t out_desc;

    cudnnFilterDescriptor_t l1_filter_desc;
    cudnnFilterDescriptor_t l2_filter_desc;
};
};

namespace dev
{
float* biz_mu_in;
};

namespace host
{
float* biz_mu_in;
};

__host__
void
sync_to_dev()
{
    cudaMemcpy(dev::biz_mu_in, host::biz_mu_in, sizeof(float) * 1, cudaMemcpyHostToDevice);
}

__host__
void
sync_from_dev()
{
    cudaMemcpy(host::biz_mu_in, dev::biz_mu_in, sizeof(float) * 1, cudaMemcpyDeviceToHost);
}

template <typename Type>
    void
    sync_device(Type* dev, Type* host, size_t count)
    {
        cudaMemcpy(dev, host, sizeof(Type) * count, cudaMemcpyHostToDevice);
    }

template <typename Type>
    void
    sync_host(Type* dev, Type* host, size_t count)
    {
        cudaMemcpy(host, dev, sizeof(Type) * count, cudaMemcpyDeviceToHost);
    }

__global__
void
kernel()
{
    dev::biz_mu_in[0] = 1.0f;
}

__host__
void
toggle_device()
{}

__host__
void
example()
{
    /* Do something to host::biz_mu_in. */
    toggle_device();
    sync_device(dev::biz_mu_in, host::biz_mu_in, 1);
    cuda_kernel<<<1, 1>>>(dev::biz_mu_in);
    sync_host(dev::biz_mu_in, host::biz_mu_in, 1);
    toggle_device();
    /* Use host::biz_mu_in. */
}
