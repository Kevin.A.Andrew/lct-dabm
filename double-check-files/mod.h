#pragma once
#include "config.h"

#define FARMER_BOVINE_MIN 3600.0f
#define FARMER_BOVINE_MAX 5400.0f
#define FARMER_BOVINE_MEAN 4800.0f

// #define FARMER_BOVINE_MIN 2
// #define FARMER_BOVINE_MAX 4
// #define FARMER_BOVINE_MEAN 3
// #define FARMER_BOVINE_SCALE 1800.0f

#define FARMER_LAND_SCALE 1650600.0f

#define HAY_MULT 1e-32
#define HAY_POW_MULT 0.0358
#define CORN_MULT(Y) (11.433 * Y - 86.826)
#define MEAT_MULT 2e-20
#define MEAT_POW_MULT 0.0234
#define MILK_MULT 2e-9
#define MILK_POW_MULT 0.0114

__device__ __forceinline__
float
produce_hay_usd(float hay_fact, float bmp_fact, float cov_pas, unsigned year)
{
    return (hay_fact * std::pow(cov_pas, bmp_fact))
        * HAY_MULT * std::exp(year * HAY_POW_MULT);
}

__device__ __forceinline__
float
produce_corn_usd(float corn_fact, float bmp_fact, float cov_crp, unsigned year)
{
    return (corn_fact * std::pow(cov_crp, bmp_factor))
        * CORN_MULT(year);
}

__device__ __forceinline__
float
produce_meat_usd(float meat_fact, float bmp_fact, float cov_pas, unsigned year)
{
    return (meat_fact * std::pow(cov_pas, bmp_fact))
        * MEAT_MULT * std::exp(year * MEAT_POW_MULT);
}

__device__ __forceinline__
float
produce_milk_usd(float milk_fact, float bmp_fact, float cov_pas, unsigned year)
{
    return (milk_fact * std::pow(cov_pas, bmp_fact))
        * MILK_MULT * std::exp(year * MILK_POW_MULT);
}

__device__ __forceinline__
float
produce_phos(float fact, float bmp_fact, float cov)
{
    return fact * std::pow(cov, bmp_factor);
}

