#pragma once
#include "config.h"


FLAMEGPU_AGENT_FUNCTION_CONDITION(\
is_forestry_step)
{
    return (FLAMEGPU->getStepCounter() % YEARLY == 0);
}


FLAMEGPU_AGENT_FUNCTION(\
forester_take_action_, flamegpu::MessageNone, flamegpu::MessageBucket)
{
    FLAMEGPU->message_out.setKey(FLAMEGPU->getVariable<unsigned>("parcel_id"));

    const unsigned action_usage = onehot_split2(0, 1);
    const unsigned action_prod = onehot_split3(2, 3, 4);

    xor_split(usage, 0, 1, "using_amp", 1);
    switch_split(prod, 2, 3, 4);

    return flamegpu::ALIVE;
}


/// Send an update message out to all neighbors
FLAMEGPU_AGENT_FUNCTION(\
forest_message_neighbors_, flamegpu::MessageNone, flamegpu::MessageBucket)
{
    const unsigned used_amp = FLAMEGPU->getVariable<unsigned>("using_amp");
    const float loss = FLAMEGPU->getVariable<float>("loss");
    FLAMEGPU->message_out.setKey(FLAMEGPU->getVariable<unsigned>("parcel_id"));
    FLAMEGPU->message_out.setVariable<unsigned>("used_amp", used_amp);
    FLAMEGPU->message_out.setVariable<float>("had_loss", loss);
    return flamegpu::ALIVE;
}

/// Update information about neighbors
FLAMEGPU_AGENT_FUNCTION(\
forest_update_neighbors_, flamegpu::MessageBucket, flamegpu::MessageNone)
{
    recinfo_forest_neighbor(0);
    recinfo_forest_neighbor(1);
    recinfo_forest_neighbor(2);
    recinfo_forest_neighbor(3);
    recinfo_forest_neighbor(4);
    return flamegpu::ALIVE;
}


FLAMEGPU_AGENT_FUNCTION(\
forest_step_, flamegpu::MessageNone, flamegpu::MessageNone)
{
    /// USING @f fn_agent_steps
    /// @TODO CLEANUP SO IT WORKS HERE
}

