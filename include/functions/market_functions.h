#pragma once

/// TODO FIX FOR DEVICE CODE
/// TODO VERIFY FUNCTIONS FROM ECONOMIC MODEL
namespace farmer
{
typedef unsigned production_type_t;
const production_type_t corn = 0;
const production_type_t hay = 1;
const production_type_t beef = 2;
const production_type_t dairy = 3;
};

__host__ __device__ __inline__ float
farmer_get_profit_corn(const float& factor_prod, const bool& using_bmp,
    const unsigned& cropland, const unsigned& year)
{
    float factor_bmp = using_bmp ? 0.6f : 1.1f;
    float area_f = nlcd::cell_to_area(cropland);
    float year_f = static_cast<float>(year + 2001.0);
    return factor_prod * pow(area_f, factor_bmp) * (11.433 * year_f - 86.826);
}

__host__ __device__ __inline__ float
farmer_get_profit_hay(const float& factor_prod, const bool& using_bmp,
    const unsigned& cell_count, const unsigned& year)
{
    float factor_bmp = using_bmp ? 0.6f : 1.1f;
    float area_f = nlcd::cell_to_area(cell_count);
    float year_f = static_cast<float>(year + 2001.0);
    return factor_prod * pow(area_f, factor_bmp) * 1e-32 * exp(year_f * 0.0358);
}

__host__ __device__ __inline__ float
farmer_get_profit_beef(const float& factor_prod, const bool& using_bmp,
    const unsigned& cell_count, const unsigned& year)
{
    float factor_bmp = using_bmp ? 0.6f : 1.1f;
    float area_f = nlcd::cell_to_area(cell_count);
    float year_f = static_cast<float>(year + 2001.0);
    return factor_prod * pow(area_f, factor_bmp) * 2e-20 * exp(year_f * 0.0234);
}

__host__ __device__ __inline__ float
farmer_get_profit_dairy(const float& factor_prod, const bool& using_bmp,
    const unsigned& cell_count, const unsigned& year)
{
    float factor_bmp = using_bmp ? 0.6f : 1.1f;
    float area_f = nlcd::cell_to_area(cell_count);
    float year_f = static_cast<float>(year + 2001.0);
    return factor_prod * pow(area_f, factor_bmp) * 2e-9 * exp(year * 0.0114);
}


__host__ __device__ __inline__ float
farmer_get_profit(const farmer::production_type_t& prod_type,
    const float& factor_prod, const bool& using_bmp,
    const unsigned & cell_count, const unsigned& year)
{
    float factor_bmp = using_bmp ? 0.6f : 1.1f;
    float factor_lc = pow(nlcd::cell_to_area(cell_count), factor_bmp);
    float year_f = static_cast<float>(year + 2001.0);
    switch (prod_type)
    {
        case 0:
            return factor_prod * factor_lc * (11.433 * year_f - 86.826);
        case 1:
            return factor_prod * factor_lc * (1e-32 * exp(year_f * 0.0358));
        case 2:
            return factor_prod * factor_lc * (2e-9 * exp(year_f * 0.0114));
        case 3:
            return factor_prod * factor_lc * (2e-20 * exp(year_f * 0.0234));
    }
    return 0.0f;
}
