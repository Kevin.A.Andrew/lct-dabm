#pragma once
#include "config.h"
#include "functions/business_functions.h"

/// TODO CLEANUP AND RESTRUCTURE AS IN SPEC/ODD+D

FLAMEGPU_AGENT_FUNCTION_CONDITION(\
is_seeking_house)
{
    return FLAMEGPU->getVariable<int>("wants_house") > 0;
}

FLAMEGPU_AGENT_FUNCTION_CONDITION(\
has_job)
{
    return FLAMEGPU->getVariable<unsigned>("employer_id") != 0;
}

FLAMEGPU_AGENT_FUNCTION(\
residence_raised_, flamegpu::MessageBucket, flamegpu::MessageNone)
{
    const unsigned key_id = FLAMEGPU->getVariable<unsigned>("employer_id");
    for (const auto& message : FLAMEGPU->message_in(key_id))
    {
        FLAMEGPU->setVariable<float>("salary",
            FLAMEGPU->getVariable<float>("salary")
            * message.getVariable<float>("mult"));
        break;
    }
    return flamegpu::ALIVE;
}

FLAMEGPU_AGENT_FUNCTION(\
residence_fired_, flamegpu::MessageBucket, flamegpu::MessageNone)
{
    const unsigned key_in = FLAMEGPU->getVariable<unsigned>("parcel_id");
    for (const auto& message : FLAMEGPU->message_in(key_in))
    {
        FLAMEGPU->setVariable<unsigned>("employer_id", 0u);
        FLAMEGPU->setVariable<float>("salary", 0.0f);
        break;
    }
    return flamegpu::ALIVE;
}

FLAMEGPU_AGENT_FUNCTION(\
residence_take_action_, flamegpu::MessageNone, flamegpu::MessageNone)
{
    using flamemath::onehot;
    unsigned action0 = onehot(FLAMEGPU->getVariable<float>(RESACT_NOJOB),
        FLAMEGPU->getVariable<float>(RESACT_JOB));
    unsigned action1 = onehot(FLAMEGPU->getVariable<float>(RESACT_NOAPT),
        FLAMEGPU->getVariable<float>(RESACT_APT));

    using namespace residence;
    if (action0 == action_nojob)
    {
        FLAMEGPU->setVariable<float>(RRESACT_NOJOB, 1.0f);
        FLAMEGPU->setVariable<float>(RRESACT_JOB, 0.0f);
        FLAMEGPU->setVariable<unsigned>("wants_job", false);
    }
    else
    {
        FLAMEGPU->setVariable<float>(RRESACT_NOJOB, 0.0f);
        FLAMEGPU->setVariable<float>(RRESACT_JOB, 1.0f);
        FLAMEGPU->setVariable<unsigned>("wants_job", true);
    }

    if (action1 == action_noapt)
    { FLAMEGPU->setVariable<float>(RRESACT_NOAPT, 1.0f);
        FLAMEGPU->setVariable<float>(RRESACT_APT, 0.0f);
        FLAMEGPU->setVariable<unsigned>("wants_house", false);
    }
    else
    {
        FLAMEGPU->setVariable<float>(RRESACT_NOAPT, 0.0f);
        FLAMEGPU->setVariable<float>(RRESACT_APT, 1.0f);
        FLAMEGPU->setVariable<unsigned>("wants_house", true);
    }
    return flamegpu::ALIVE;
}


FLAMEGPU_AGENT_FUNCTION_CONDITION(\
residence_is_vacant)
{
    return (FLAMEGPU->getVariable<unsigned>("is_vacant") > 0);
}

FLAMEGPU_AGENT_FUNCTION(\
residence_list_house_, flamegpu::MessageNone, flamegpu::MessageBruteForce)
{
    FLAMEGPU->message_out.setVariable<unsigned>("house",
        FLAMEGPU->getVariable<unsigned>("vacant_parcel_id"));
    FLAMEGPU->message_out.setVariable<float>("rent",
        FLAMEGPU->getVariable<float>("vacant_rent"));
    return flamegpu::ALIVE;
}

FLAMEGPU_AGENT_FUNCTION(\
residence_find_house_, flamegpu::MessageBruteForce, flamegpu::MessageBucket)
{
    const unsigned key = FLAMEGPU->getVariable<unsigned>("parcel_id");
    const float income = FLAMEGPU->getVariable<float>("salary");
    const float current_rent = FLAMEGPU->getVariable<float>("rent");
    const float current_prop = current_rent / income;
    float target_prop = flamerand::uniform<float>(0.4f, 0.7f, U);
    unsigned target_house = 0u;
    for (const auto& message : FLAMEGPU->message_in)
    {
        const float message_rent = message.getVariable<float>("rent");
        const float message_prop = message_rent / income;
        if (message_prop < target_prop)
        {
            target_prop = message_prop;
            target_house = message.getVariable<unsigned>("house");
        }
    }
    if (target_house != 0)
    {
        FLAMEGPU->message_out.setKey(target_house);
        FLAMEGPU->message_out.setVariable<unsigned>("owner_id", key);
    }
    return flamegpu::ALIVE;
}


FLAMEGPU_AGENT_FUNCTION(\
residence_accept_offer_, flamegpu::MessageBucket, flamegpu::MessageBucket)
{
    const unsigned key = FLAMEGPU->getVariable<unsigned>("vacant_parcel_id");
    unsigned new_owner = 0;
    for (const auto& message : FLAMEGPU->message_in(key))
    {
        new_owner = message.getVariable<unsigned>("owner_id");
    }
    if (new_owner != 0)
    {
        FLAMEGPU->message_out.setKey(new_owner);
        FLAMEGPU->message_out.setVariable<unsigned>("house", key);
        FLAMEGPU->message_out.setVariable<float>("rent",
            FLAMEGPU->getVariable<float>("vacant_rent"));
        FLAMEGPU->setVariable<unsigned>("is_vacant", false);
    }
    return flamegpu::ALIVE;
}

FLAMEGPU_AGENT_FUNCTION(\
residence_move_house_, flamegpu::MessageBucket, flamegpu::MessageNone)
{
    unsigned new_parcel = 0u;
    const unsigned old_parcel = FLAMEGPU->getVariable<unsigned>("parcel_id");
    for (const auto& message : FLAMEGPU->message_in(old_parcel))
    {
        FLAMEGPU->setVariable<unsigned>("is_vacant", true);
        FLAMEGPU->setVariable<unsigned>("vacant_parcel_id",
            FLAMEGPU->getVariable<unsigned>("parcel_id"));
        FLAMEGPU->setVariable<float>("vacant_rent",
            FLAMEGPU->getVariable<float>("rent"));

        FLAMEGPU->setVariable<unsigned>("parcel_id",
            message.getVariable<unsigned>("house"));
        FLAMEGPU->setVariable<float>("rent",
            message.getVariable<float>("rent"));
        break;
    }
    return flamegpu::ALIVE;
}

FLAMEGPU_AGENT_FUNCTION(\
residence_step_, flamegpu::MessageNone, flamegpu::MessageNone)
{
    using namespace residence;
    bool has_job;
    has_job = static_cast<bool>(FLAMEGPU->getVariable<unsigned>("employer_id"));

    float balance = 0.0f;
    float budget = 0.0f;
    if (has_job)
    {
        const float income = FLAMEGPU->getVariable<float>("salary");
        const float expenses = flamerand::uniform<float>(0.0f, 100.0f, U);
        FLAMEGPU->setVariable<float>("debug_expenses", expenses);
        const float rent = FLAMEGPU->getVariable<float>("rent");
        balance = income - expenses - rent;
        if ( balance < 0 )
        {
            balance /= 3.0f;
        }
        FLAMEGPU->setVariable<float>("debug_balance", balance);
        budget = FLAMEGPU->getVariable<float>("budget");
        budget += balance;
        FLAMEGPU->setVariable<float>("budget", (budget + balance));
    }
    unsigned time_in_state = FLAMEGPU->getVariable<unsigned>("time_in_state");

    mood_t mood, new_mood;
    mood = FLAMEGPU->getVariable<mood_t>("mood");
    new_mood = mood;
    switch (mood)
    {
        case happy:
            if (budget < 250.0f)
                new_mood = waiting;
            break;
        case waiting:
            if (budget >= 250.0f)
                new_mood = happy;
            else if ((budget < 50.0f) or (balance < 50.0f))
                new_mood = extreme;
            break;
        case recovery:
            new_mood = waiting;
            FLAMEGPU->setVariable<float>("budget", 100.0f);
            break;
        case extreme:
            if ((budget > 50.0f) and (balance > 50.0f))
                new_mood = waiting;
            break;
    }
    if (mood == new_mood) ++time_in_state;
    FLAMEGPU->setVariable<float>(RES_SS_BUDGET, budget / 250.f);
    switch (new_mood)
    {
        case happy:
            FLAMEGPU->setVariable<float>(RES_SS_MOOD, -1.0f);
            break;
        case waiting:
            FLAMEGPU->setVariable<float>(RES_SS_MOOD, -0.3f);
            break;
        case recovery:
            FLAMEGPU->setVariable<float>(RES_SS_MOOD, 0.3f);
            break;
        case extreme:
            FLAMEGPU->setVariable<float>(RES_SS_MOOD, 1.0f);
            break;
    }
    FLAMEGPU->setVariable<float>("next_state2", 3.0f);
    FLAMEGPU->setVariable<mood_t>("mood", new_mood);
    FLAMEGPU->setVariable<unsigned>("time_in_state", time_in_state);
    if (budget > 0.0f)
        FLAMEGPU->setVariable<float>("reward", 10.0f);
    else if (budget <= 0.0f)
        FLAMEGPU->setVariable<float>("reward", -5.0f);
    else
        FLAMEGPU->setVariable<float>("reward", -3.0f);
    return flamegpu::ALIVE;
}


FLAMEGPU_AGENT_FUNCTION_CONDITION(\
is_seeking_job)
{
    return FLAMEGPU->getVariable<unsigned>("wants_job") > 0;
}

FLAMEGPU_AGENT_FUNCTION(\
residence_find_job_, flamegpu::MessageBruteForce, flamegpu::MessageBucket)
{
    const unsigned worker_id = FLAMEGPU->getVariable<unsigned>("worker_id");
    const unsigned current_employer = FLAMEGPU->getVariable<unsigned>("employer_id");
    float target_salary = FLAMEGPU->getVariable<float>("salary");
    unsigned target_employer = 0u;
    for (const auto& message : FLAMEGPU->message_in)
    {
        const unsigned potential = message.getVariable<unsigned>("business_id");
        if (potential == current_employer)
            continue;

        const float new_salary = get_random_salary(U);
        if (new_salary > target_salary)
        {
            target_employer = potential;
            target_salary = new_salary;
        }
    }
    if (target_employer > 0)
    {
        FLAMEGPU->message_out.setKey(target_employer);
        FLAMEGPU->message_out.setVariable<float>("target_salary", target_salary);
        FLAMEGPU->message_out.setVariable<unsigned>("worker_id", worker_id);
    }
    return flamegpu::ALIVE;
}

FLAMEGPU_AGENT_FUNCTION(\
residence_add_job_, flamegpu::MessageBucket, flamegpu::MessageBucket)
{
    const unsigned key = FLAMEGPU->getVariable<unsigned>("worker_id");
    for (const auto& message : FLAMEGPU->message_in(key))
    {
        const unsigned old_employer
            = FLAMEGPU->getVariable<unsigned>("employer_id");
        const float old_salary
            = FLAMEGPU->getVariable<float>("salary");

        FLAMEGPU->setVariable<unsigned>("employer_id",
            message.getVariable<unsigned>("business_id"));
        FLAMEGPU->setVariable<float>("salary",
            message.getVariable<float>("salary"));

        if (old_employer != 0)
        {
            FLAMEGPU->message_out.setKey(old_employer);
            FLAMEGPU->message_out.setVariable<unsigned>("worker_id", key);
            FLAMEGPU->message_out.setVariable<float>("salary", old_salary);
        }
    }
    return flamegpu::ALIVE;
}
