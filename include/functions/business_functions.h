#pragma once
#include "config.h"

#define test_ret(X, C, Y, Z) \
    C += Y; \
    if (X < (C / 8.2)) return Z;

__device__ __forceinline__
float
get_random_salary(const float& rand_num)
{
    float cum_sum = 0.0f;
    test_ret(rand_num, cum_sum, 0.1f, 250.0f);
    test_ret(rand_num, cum_sum, 0.2f, 300.0f);
    test_ret(rand_num, cum_sum, 0.3f, 350.0f);
    test_ret(rand_num, cum_sum, 0.5f, 400.0f);
    test_ret(rand_num, cum_sum, 0.7f, 450.0f);
    test_ret(rand_num, cum_sum, 0.85f, 500.0f);
    test_ret(rand_num, cum_sum, 0.9f, 550.0f);
    test_ret(rand_num, cum_sum, 0.85f, 600.0f);
    test_ret(rand_num, cum_sum, 0.7f, 650.0f);
    test_ret(rand_num, cum_sum, 0.6f, 700.0f);
    test_ret(rand_num, cum_sum, 0.5f, 750.0f);
    test_ret(rand_num, cum_sum, 0.5f, 800.0f);
    test_ret(rand_num, cum_sum, 0.5f, 850.0f);
    test_ret(rand_num, cum_sum, 0.5f, 900.0f);
    test_ret(rand_num, cum_sum, 0.3f, 950.0f);
    return 1000.0f;
}


FLAMEGPU_AGENT_FUNCTION_CONDITION(\
is_updating_sals)
{
    return (FLAMEGPU->getVariable<unsigned>("days_operating") % 12) == 0;
}

FLAMEGPU_AGENT_FUNCTION(\
business_update_salaries_, flamegpu::MessageNone, flamegpu::MessageBucket)
{
    float rcent = 1.1f;
    for (size_t e_idx = 0; e_idx < MAX_EMPLOYEES; ++e_idx)
    {
        set_employee_pay(e_idx, get_employee_pay(e_idx) * rcent);
    }
    const unsigned business_id = FLAMEGPU->getVariable<unsigned>("parcel_id");
    FLAMEGPU->message_out.setKey(business_id);
    FLAMEGPU->message_out.setVariable<float>("mult", rcent);
    return flamegpu::ALIVE;
}


FLAMEGPU_AGENT_FUNCTION_CONDITION(\
is_hiring)
{
    return (FLAMEGPU->getVariable<unsigned>("employee_capacity")
            > FLAMEGPU->getVariable<unsigned>("employee_count"))
           and (FLAMEGPU->getVariable<unsigned>("is_hiring") > 0);
}

FLAMEGPU_AGENT_FUNCTION_CONDITION(\
is_firing)
{
    return (FLAMEGPU->getVariable<unsigned>("employee_count") > 0)
           and (FLAMEGPU->getVariable<unsigned>("is_firing") > 0);
}


FLAMEGPU_AGENT_FUNCTION(\
business_cell_stat_, flamegpu::MessageBucket, flamegpu::MessageNone)
{
    const unsigned key_id = FLAMEGPU->getVariable<unsigned>("parcel_id");
    unsigned total_cells = 0u;
    unsigned other_cells = 0u;
    unsigned forest_cells = 0u;
    unsigned forest_cells_d = 0u;
    unsigned forest_cells_c = 0u;
    unsigned forest_cells_m = 0u;
    unsigned urban_cells = 0u;
    unsigned urban_cells_o = 0u;
    unsigned urban_cells_l = 0u;
    unsigned urban_cells_m = 0u;
    unsigned urban_cells_h = 0u;
    for (const auto& message : FLAMEGPU->message_in(key_id))
    {
        ++total_cells;
        const nlcd_t cover = message.getVariable<unsigned>("cover");
        switch (cover)
        {
            case nlcd::forest_dec:
                ++forest_cells_d;
                ++forest_cells;
                break;
            case nlcd::forest_con:
                ++forest_cells_c;
                ++forest_cells;
                break;
            case nlcd::forest_mix:
                ++forest_cells_m;
                ++forest_cells;
                break;
            case nlcd::urban_open:
                ++urban_cells_o;
                ++urban_cells;
                break;
            case nlcd::urban_low:
                ++urban_cells_l;
                ++urban_cells;
                break;
            case nlcd::urban_medium:
                ++urban_cells_m;
                ++urban_cells;
                break;
            case nlcd::urban_high:
                ++urban_cells_h;
                ++urban_cells;
                break;
            default:
                ++other_cells;
        }
    }

    FLAMEGPU->setVariable<unsigned>(BIZ_COV_OTHER, other_cells);
    FLAMEGPU->setVariable<unsigned>(BIZ_COV_UO, urban_cells_o);
    FLAMEGPU->setVariable<unsigned>(BIZ_COV_UL, urban_cells_l);
    FLAMEGPU->setVariable<unsigned>(BIZ_COV_UM, urban_cells_m);
    FLAMEGPU->setVariable<unsigned>(BIZ_COV_UH, urban_cells_h);
    FLAMEGPU->setVariable<unsigned>(BIZ_COV_FD, forest_cells_d);
    FLAMEGPU->setVariable<unsigned>(BIZ_COV_FC, forest_cells_c);
    FLAMEGPU->setVariable<unsigned>(BIZ_COV_FM, forest_cells_m);
    FLAMEGPU->setVariable<unsigned>("cells_total", total_cells);
    FLAMEGPU->setVariable<unsigned>("cells_forest", forest_cells);
    FLAMEGPU->setVariable<unsigned>("cells_urban", urban_cells);
}


FLAMEGPU_AGENT_FUNCTION(\
business_list_jobs_, flamegpu::MessageNone, flamegpu::MessageBruteForce)
{
    FLAMEGPU->message_out.setVariable<unsigned>("business_id",
        FLAMEGPU->getVariable<unsigned>("parcel_id"));
    return flamegpu::ALIVE;
}


FLAMEGPU_AGENT_FUNCTION(\
business_fill_job_, flamegpu::MessageBucket, flamegpu::MessageBucket)
{
    const unsigned key_in = FLAMEGPU->getVariable<unsigned>("parcel_id");
    for (const auto& message : FLAMEGPU->message_in(key_in))
    {
        const unsigned worker_id = message.getVariable<unsigned>("worker_id");
        const float salary = message.getVariable<float>("target_salary");
        bool found_slot = false;
        size_t idx = 0;
        for (idx = 0; (idx < MAX_EMPLOYEES) and (not found_slot); ++idx)
        {
            found_slot = (get_employee_id(idx) == 0);
        }
        set_employee_id(idx-1, worker_id);
        set_employee_pay(idx-1, salary);

        FLAMEGPU->setVariable<unsigned>("employee_count",
            FLAMEGPU->getVariable<unsigned>("employee_count") + 1);

        FLAMEGPU->message_out.setKey(worker_id);
        FLAMEGPU->message_out.setVariable<unsigned>("business_id", key_in);
        FLAMEGPU->message_out.setVariable<float>("salary", salary);
        return flamegpu::ALIVE;
    }
    return flamegpu::ALIVE;
}

FLAMEGPU_AGENT_FUNCTION(\
business_fire_, flamegpu::MessageNone, flamegpu::MessageBucket)
{
    const unsigned key_out = FLAMEGPU->getVariable<unsigned>("parcel_id");
    float min_sal = 1.0e9;
    unsigned tgt_id;
    size_t tgt_idx;
    for (size_t idx = 0; (idx < MAX_EMPLOYEES); ++idx)
    {
        const unsigned this_id = get_employee_id(idx);
        if (this_id == 0)
            continue;

        const float this_pay = get_employee_pay(idx);
        if (this_pay > min_sal)
            continue;

        tgt_id = this_id;
        tgt_idx = idx;
        min_sal = this_pay;
    }
    set_employee_id(tgt_idx, 0u);
    set_employee_pay(tgt_idx, 0.0f);
    FLAMEGPU->setVariable<unsigned>("employee_count",
        FLAMEGPU->getVariable<unsigned>("employee_count")-1);
    FLAMEGPU->message_out.setKey(tgt_id);
    FLAMEGPU->message_out.setVariable<unsigned>("fire", true);
    return flamegpu::ALIVE;
}

FLAMEGPU_AGENT_FUNCTION(\
business_quits_, flamegpu::MessageBucket, flamegpu::MessageNone)
{
    const unsigned key_in = FLAMEGPU->getVariable<unsigned>("parcel_id");
    for (const auto& message : FLAMEGPU->message_in(key_in))
    {
        const unsigned worker_id
            = message.getVariable<unsigned>("worker_id");
        for (size_t employee = 0; employee < MAX_EMPLOYEES; ++employee)
        {
            if (get_employee_id(employee) == worker_id)
            {
                set_employee_id(employee, 0u);
                set_employee_pay(employee, 0.0f);
                FLAMEGPU->setVariable<unsigned>("employee_count",
                    FLAMEGPU->getVariable<unsigned>("employee_count")-1);
            }
        }
    }
    return flamegpu::ALIVE;
}


/// Take Action
FLAMEGPU_AGENT_FUNCTION(\
business_take_action_, flamegpu::MessageNone, flamegpu::MessageNone)
{
    using namespace business;
    using flamemath::onehot;

    size_t action0, action1;

    action0 = onehot(FLAMEGPU->getVariable<float>(BIZACT_DEC),
                     FLAMEGPU->getVariable<float>(BIZACT_MTN),
                     FLAMEGPU->getVariable<float>(BIZACT_INC));
    unsigned capacity = FLAMEGPU->getVariable<unsigned>("employee_capacity");
    switch (action0)
    {
        case action_deccap:
            FLAMEGPU->setVariable<float>(RBIZACT_DEC, 1.0f);
            FLAMEGPU->setVariable<float>(RBIZACT_MTN, 0.0f);
            FLAMEGPU->setVariable<float>(RBIZACT_INC, 0.0f);
            if (capacity > 0)
                FLAMEGPU->setVariable<unsigned>("employee_capacity",
                                                capacity - 1);
            break;
        case action_keepcap:
            FLAMEGPU->setVariable<float>(RBIZACT_DEC, 0.0f);
            FLAMEGPU->setVariable<float>(RBIZACT_MTN, 1.0f);
            FLAMEGPU->setVariable<float>(RBIZACT_INC, 0.0f);
            break;
        case action_inccap:
            FLAMEGPU->setVariable<float>(RBIZACT_DEC, 0.0f);
            FLAMEGPU->setVariable<float>(RBIZACT_MTN, 0.0f);
            FLAMEGPU->setVariable<float>(RBIZACT_INC, 1.0f);
            if (capacity < MAX_EMPLOYEES)
                FLAMEGPU->setVariable<unsigned>("employee_capacity",
                                                capacity + 1);
            break;
    }

    action1 = onehot(FLAMEGPU->getVariable<float>(BIZACT_FIR),
                     FLAMEGPU->getVariable<float>(BIZACT_STY),
                     FLAMEGPU->getVariable<float>(BIZACT_HIR));
    switch (action1)
    {
        case action_fire:
            FLAMEGPU->setVariable<float>(RBIZACT_FIR, 1.0f);
            FLAMEGPU->setVariable<float>(RBIZACT_STY, 0.0f);
            FLAMEGPU->setVariable<float>(RBIZACT_HIR, 0.0f);
            FLAMEGPU->setVariable<unsigned>("is_firing", true);
            FLAMEGPU->setVariable<unsigned>("is_hiring", false);
            break;
        case action_stay:
            FLAMEGPU->setVariable<float>(RBIZACT_FIR, 0.0f);
            FLAMEGPU->setVariable<float>(RBIZACT_STY, 1.0f);
            FLAMEGPU->setVariable<float>(RBIZACT_HIR, 0.0f);
            FLAMEGPU->setVariable<unsigned>("is_firing", false);
            FLAMEGPU->setVariable<unsigned>("is_hiring", false);
            break;
        case action_hire:
            FLAMEGPU->setVariable<float>(RBIZACT_FIR, 0.0f);
            FLAMEGPU->setVariable<float>(RBIZACT_STY, 0.0f);
            FLAMEGPU->setVariable<float>(RBIZACT_HIR, 1.0f);
            FLAMEGPU->setVariable<unsigned>("is_firing", false);
            FLAMEGPU->setVariable<unsigned>("is_hiring", true);
            break;
    }
    return flamegpu::ALIVE;
}


FLAMEGPU_AGENT_FUNCTION(\
business_step_year_, flamegpu::MessageNone, flamegpu::MessageNone)
{
    return flamegpu::ALIVE;
}


FLAMEGPU_AGENT_FUNCTION(\
business_step_quarter_, flamegpu::MessageNone, flamegpu::MessageNone)
{
    return flamegpu::ALIVE;
}


FLAMEGPU_AGENT_FUNCTION(\
business_step_, flamegpu::MessageNone, flamegpu::MessageNone)
{
    unsigned employee_count, employee_capacity;
    employee_count = FLAMEGPU->getVariable<unsigned>("employee_count");
    employee_capacity = FLAMEGPU->getVariable<unsigned>("employee_capacity");

    mood_t mood = FLAMEGPU->getVariable<mood_t>("mood");
    // if no employees, company is bankrupt and gets reset
    if (mood == business::shrink and employee_count == 0)
    {
        FLAMEGPU->setVariable<mood_t>("mood", business::init);
        FLAMEGPU->setVariable<unsigned>("fcount", 1u);
        FLAMEGPU->setVariable<unsigned>("employee_capacity", INIT_ECAP);
        FLAMEGPU->setVariable<float>(NBIZST_FIN, 0.0f);
        FLAMEGPU->setVariable<float>(NBIZST_EMP, 0.0f);
        FLAMEGPU->setVariable<float>(NBIZST_CAP, 0.0f);
        FLAMEGPU->setVariable<float>(NBIZST_ECAP, 0.0f);
        FLAMEGPU->setVariable<float>("reward", -10.0f);
        FLAMEGPU->setVariable<unsigned>("days_operating", 0u);
        return flamegpu::ALIVE;
    }

    float state_count, state_capacity, rcapacity;

    state_count = (2.0f * static_cast<float>(employee_count)
                        / MAX_EMPLOYEES) - 1.0f;
    state_capacity = (2.0f * static_cast<float>(employee_capacity)
                            / MAX_EMPLOYEES) - 1.0f;
    rcapacity = static_cast<float>(employee_capacity);

    unsigned fcount
        = FLAMEGPU->getVariable<unsigned>("fcount");
    float state_fmood = 0.0f;
    float reward = 1.0f;
    if (fcount == 0)
    {
        switch (mood)
        {
            case business::init:
                fcount = FLAMERANDI(3, 4);
                mood = business::working;
                break;
            case business::working:
                fcount = FLAMERANDI(3, 5);
                if (flamerand::test_lt(0.5f, U))
                    mood = business::grow;
                else
                    mood = business::shrink;
                break;
            case business::grow:
            case business::shrink:
                fcount = FLAMERANDI(3, 4);
                mood = business::working;
                break;
        }
    }
    else
    {
        --fcount;
    }
    switch (mood)
    {
        case business::grow:
            state_fmood = 1.0f;
            rcapacity = rcapacity + 2.0f;
            reward = employee_count / rcapacity;
            break;
        case business::shrink:
            state_fmood = -1.0f;
            rcapacity = rcapacity - 2.0f;
            reward = (rcapacity > 0.0f) ? employee_count / rcapacity : -1.0f;
            break;
    }

    float state_rcapacity = ((2.0f * rcapacity) / MAX_EMPLOYEES) - 1.0f;
    FLAMEGPU->setVariable<mood_t>("mood", mood);
    FLAMEGPU->setVariable<unsigned>("fcount", fcount);
    FLAMEGPU->setVariable<float>(NBIZST_FIN, state_fmood);
    FLAMEGPU->setVariable<float>(NBIZST_EMP, state_count);
    FLAMEGPU->setVariable<float>(NBIZST_CAP, state_capacity);
    FLAMEGPU->setVariable<float>(NBIZST_ECAP, state_rcapacity);
    FLAMEGPU->setVariable<float>("reward", reward);

    unsigned days_operating = FLAMEGPU->getVariable<unsigned>("days_operating");
    FLAMEGPU->setVariable<unsigned>("days_operating", (days_operating + 1));
    return flamegpu::ALIVE;
}

