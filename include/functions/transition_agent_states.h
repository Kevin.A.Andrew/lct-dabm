#pragma once
#include "config.h"


FLAMEGPU_AGENT_FUNCTION(\
forest_transition_state_, flamegpu::MessageNone, flamegpu::MessageNone)
{
    FLAMEGPU->setVariable<unsigned>("used_amp", FLAMEGPU->getVariable<unsigned>("using_amp"));
    FLAMEGPU->setVariable<float>("state0", FLAMEGPU->getVariable<float>("next_state0"));
    FLAMEGPU->setVariable<float>("state1", FLAMEGPU->getVariable<float>("next_state1"));
    FLAMEGPU->setVariable<float>("state2", FLAMEGPU->getVariable<float>("next_state2"));
    FLAMEGPU->setVariable<float>("state3", FLAMEGPU->getVariable<float>("next_state3"));
    FLAMEGPU->setVariable<float>("state4", FLAMEGPU->getVariable<float>("next_state4"));
    FLAMEGPU->setVariable<float>("state5", FLAMEGPU->getVariable<float>("next_state5"));
    FLAMEGPU->setVariable<float>("state6", FLAMEGPU->getVariable<float>("next_state6"));
    FLAMEGPU->setVariable<float>("state7", FLAMEGPU->getVariable<float>("next_state7"));
    FLAMEGPU->setVariable<float>("state8", FLAMEGPU->getVariable<float>("next_state8"));
    FLAMEGPU->setVariable<float>("state9", FLAMEGPU->getVariable<float>("next_state9"));
}


FLAMEGPU_AGENT_FUNCTION(\
business_transition_state_, flamegpu::MessageNone, flamegpu::MessageNone)
{
        FLAMEGPU->setVariable<float>(BIZST_FIN,
                FLAMEGPU->getVariable<float>(NBIZST_FIN));
        FLAMEGPU->setVariable<float>(BIZST_EMP,
                FLAMEGPU->getVariable<float>(NBIZST_EMP));
        FLAMEGPU->setVariable<float>(BIZST_CAP,
                FLAMEGPU->getVariable<float>(NBIZST_CAP));
        FLAMEGPU->setVariable<float>(BIZST_ECAP,
                FLAMEGPU->getVariable<float>(NBIZST_ECAP));
        return flamegpu::ALIVE;
}


FLAMEGPU_AGENT_FUNCTION(\
residence_transition_state_, flamegpu::MessageNone, flamegpu::MessageNone)
{
    FLAMEGPU->setVariable<float>(RES_S_BUDGET, FLAMEGPU->getVariable<float>(RES_SS_BUDGET));
    FLAMEGPU->setVariable<float>(RES_S_MOOD, FLAMEGPU->getVariable<float>(RES_SS_MOOD));
    FLAMEGPU->setVariable<float>("state2", FLAMEGPU->getVariable<float>("next_state2"));
    FLAMEGPU->setVariable<float>("state3", FLAMEGPU->getVariable<float>("next_state3"));
    FLAMEGPU->setVariable<float>("state4", FLAMEGPU->getVariable<float>("next_state4"));
    return flamegpu::ALIVE;
}


FLAMEGPU_AGENT_FUNCTION(\
farmer_transition_state_, flamegpu::MessageNone, flamegpu::MessageNone)
{
        FLAMEGPU->setVariable<float>(FARM_S_CORN_PROD,
                FLAMEGPU->getVariable<float>(FARM_SS_CORN_PROD));
        FLAMEGPU->setVariable<float>(FARM_S_PROD_HAY,
                FLAMEGPU->getVariable<float>(FARM_SS_PROD_HAY));
        FLAMEGPU->setVariable<float>(FARM_S_PROD_BEEF,
                FLAMEGPU->getVariable<float>(FARM_SS_PROD_BEEF));
        FLAMEGPU->setVariable<float>(FARM_S_PROD_DAIRY,
                FLAMEGPU->getVariable<float>(FARM_SS_PROD_DAIRY));
        FLAMEGPU->setVariable<float>(FARM_S_NEIGHBOR_BMP,
                FLAMEGPU->getVariable<float>(FARM_SS_NEIGHBOR_BMP));
        FLAMEGPU->setVariable<float>(FARM_S_NEIGHBOR_LOSS,
                FLAMEGPU->getVariable<float>(FARM_SS_NEIGHBOR_LOSS));
        FLAMEGPU->setVariable<float>("state6", FLAMEGPU->getVariable<float>("next_state6"));
        FLAMEGPU->setVariable<float>("state7", FLAMEGPU->getVariable<float>("next_state7"));
        FLAMEGPU->setVariable<float>("state8", FLAMEGPU->getVariable<float>("next_state8"));
        FLAMEGPU->setVariable<float>("state9", FLAMEGPU->getVariable<float>("next_state9"));
        FLAMEGPU->setVariable<float>("state10", FLAMEGPU->getVariable<float>("next_state10"));
        FLAMEGPU->setVariable<float>("state11", FLAMEGPU->getVariable<float>("next_state11"));
        FLAMEGPU->setVariable<float>("state12", FLAMEGPU->getVariable<float>("next_state12"));
        FLAMEGPU->setVariable<float>("state13", FLAMEGPU->getVariable<float>("next_state13"));
        FLAMEGPU->setVariable<float>("state14", FLAMEGPU->getVariable<float>("next_state14"));
        return flamegpu::ALIVE;
}

