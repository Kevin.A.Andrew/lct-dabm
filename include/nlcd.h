#pragma once
/// @TODO Include info for NLCD Here
/// This is from the NLCD 2011 (?) Categories
/// Only Categories found in Study Areas
/// Assumes standard 30m x 30m grid for conversions

typedef unsigned nlcd_t;

namespace nlcd
{

const float nlcd_h = 30.0f;
const float nlcd_w = 30.0f;
const float nlcd_a = 30.0f * 30.0f;

const nlcd_t water = 11;
const nlcd_t urban_open = 21;
const nlcd_t urban_low = 22;
const nlcd_t urban_medium = 23;
const nlcd_t urban_high = 24;
const nlcd_t barren = 31;
const nlcd_t forest_dec = 41;
const nlcd_t forest_con = 42;
const nlcd_t forest_mix = 43;
const nlcd_t scrub = 52;
const nlcd_t grassland = 71;
const nlcd_t pasture = 81;
const nlcd_t crops = 82;
const nlcd_t woody_wetlands = 90;
const nlcd_t wetlands = 95;

__host__ __device__ float
cell_to_area(const unsigned& cover)
{
    return nlcd_h * nlcd_w * static_cast<float>(cover);
}

__host__ __device__
bool
cover_match(nlcd_t a, nlcd_t b)
{
    return a == b;
}

__host__ __device__
bool
is_forested(nlcd_t cover)
{
    return (cover == nlcd::forest_dec)
        or (cover == nlcd::forest_con)
        or (cover == nlcd::forest_mix);
}

__host__ __device__
bool
is_farmland(nlcd_t cover)
{
    return (cover == nlcd::crops)
        or (cover == nlcd::pasture);
}

__host__ __device__
bool
is_urban(nlcd_t cover)
{
    return (cover >= urban_open)
        or (cover <= urban_high);
}

};
