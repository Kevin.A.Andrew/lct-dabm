#pragma once
#ifndef LEARNING_DECIDE_ACTION_H_
#define LEARNING_DECIDE_ACTION_H_
#include "config.h"
#include "learning.h"

/// @TODO fix this bool and activation terminal activation testing
bool l_decide_action_ss = true;

__host__ void
print_header(const char* agt, float* in, float* l1)
{
#ifdef PRINT_TRIALS_MU
    flamelog::debug("—— trial %s ——", agt);
    flamednn::print_first_3n("mu::in", in);
    flamednn::print_first_3n("mu::weights0", l1);
#endif
}

__host__ void
print_footer(float* out)
{
#ifdef PRINT_TRIALS_MU
    flamednn::print_first_3n("mu::out", out);
    flamelog::debug("———————————————\n");
#endif
}

__host__ void
biz_decide()
{
    print_header("biz", tensor_data::business::mu::in, weights::business::mu::l1_host);
    flamednn::toggle_device();
    flamednn_sync_dev_data(business, mu, in, business::num_agents, business::width_mu_in);
    flamednn_convolve(business, mu, in, l1, l1);
    flamednn_activate(business, mu, l1);
    flamednn_convolve(business, mu, l1, l2, l2);
    flamednn_activate(business, mu, l2);
    flamednn_convolve(business, mu, l2, out, out);
    flamednn_sync_data(business, mu, out, business::num_agents, business::width_mu_out);
    flamednn::toggle_device();
    print_footer(tensor_data::business::mu::out);
}

__host__ void
res_decide()
{
    print_header("res", tensor_data::residence::mu::in, weights::residence::mu::l1_host);
    flamednn::toggle_device();
    flamednn_sync_dev_data(residence, mu, in, residence::num_agents, residence::width_mu_in);
    flamednn_convolve(residence, mu, in, l1, l1);
    flamednn_activate(residence, mu, l1);
    flamednn_convolve(residence, mu, l1, l2, l2);
    flamednn_activate(residence, mu, l2);
    flamednn_convolve(residence, mu, l2, out, out);
    flamednn_sync_data(residence, mu, out, residence::num_agents, residence::width_mu_out);
    flamednn::toggle_device();
    print_footer(tensor_data::residence::mu::out);
}

__host__ void
for_decide()
{
    print_header("for", tensor_data::forester::mu::in, weights::forester::mu::l1_host);
    flamednn::toggle_device();
    flamednn_sync_dev_data(forester, mu, in, forester::num_agents,
                           forester::width_mu_in);
    flamednn_convolve(forester, mu, in, l1, l1);
    flamednn_activate(forester, mu, l1);
    flamednn_convolve(forester, mu, l1, l2, l2);
    flamednn_activate(forester, mu, l2);
    flamednn_convolve(forester, mu, l2, l3, l3);
    flamednn_activate(forester, mu, l3);
    flamednn_convolve(forester, mu, l3, l4, l4);
    flamednn_activate(forester, mu, l4);
    flamednn_convolve(forester, mu, l4, out, out);
    flamednn_sync_data(forester, mu, out, forester::num_agents,
                       forester::width_mu_out);
    flamednn::toggle_device();
    print_footer(tensor_data::forester::mu::out);
}

__host__ void
farm_decide()
{
    print_header("frm", tensor_data::farmer::mu::in, weights::residence::mu::l1_host);
    flamednn::toggle_device();
    flamednn_sync_dev_data(farmer, mu, in, farmer::num_agents,
                           farmer::width_mu_in);
    flamednn_convolve(farmer, mu, in, l1, l1);
    flamednn_activate(farmer, mu, l1);
    flamednn_convolve(farmer, mu, l1, l2, l2);
    flamednn_activate(farmer, mu, l2);
    flamednn_convolve(farmer, mu, l2, l3, l3);
    flamednn_activate(farmer, mu, l3);
    flamednn_convolve(farmer, mu, l3, l4, l4);
    flamednn_activate(farmer, mu, l4);
    flamednn_convolve(farmer, mu, l4, out, out);
    flamednn_sync_data(farmer, mu, l1, farmer::num_agents, farmer::width_mu_l1);
    flamednn_sync_data(farmer, mu, out, farmer::num_agents,
                       farmer::width_mu_out);
    flamednn::toggle_device();
    print_footer(tensor_data::farmer::mu::out);
}


FLAMEGPU_HOST_FUNCTION(\
agents_decide_actions_)
{
    const unsigned step = FLAMEGPU->getStepCounter();
    if (l_decide_action_ss) { l_decide_action_ss=false; return; }
    /** MONTHLY ACTORS */
    macro_for_agents(urban_business, business, biz_id)
    {
        for (size_t sid = 0; sid < business::width_mu_in; ++sid)
        {
            *tensor_data::business::mu::in_addr(biz_id, sid)
                = business.getVariable<float>(state_factor(sid));
        }
        ++biz_id;
    }
    macro_endfor_agents;
    biz_decide();

    macro_for_agents(urban_residence, residence, res_id)
    {
        for (size_t sid = 0; sid < residence::width_mu_in; ++sid)
        {
            *tensor_data::residence::mu::in_addr(res_id, sid)
                = residence.getVariable<float>(state_factor(sid));
        }
        ++res_id;
    }
    macro_endfor_agents;
    res_decide();

    /** YEARLY ACTORS */
    if (step != 0 and step % 12 == 0)
    {
        macro_for_agents(forester, forester, for_id)
        {
            for (size_t sid = 0; sid < forester::width_mu_in; ++sid)
            {
                *tensor_data::forester::mu::in_addr(for_id, sid)
                    = forester.getVariable<float>(state_factor(sid));
            }
            ++for_id;
        }
        macro_endfor_agents;
        for_decide();

        macro_for_agents(farmer, farmer, farm_id)
        {
            for (size_t sid = 0; sid < farmer::width_mu_in; ++sid)
            {
                *tensor_data::farmer::mu::in_addr(farm_id, sid)
                    = farmer.getVariable<float>(state_factor(sid));
            }
            ++farm_id;
        }
        macro_endfor_agents;
        farm_decide();
    }

    /** STORE RESULTS */
    const float exploration_rate
        = FLAMEGPU->environment.getProperty<float>("global_exploration_rate");
    macro_for_agents(urban_business, business, biz_id)
    {
        bool exploring = flamerand::test_lt(exploration_rate, U);
        for (size_t af = 0; exploring and (af < business::width_mu_out); ++af)
        {
            *tensor_data::business::mu::out_addr(biz_id, af)
                = flamerand::uniform<float>(U);
        }
        for (size_t aid = 0; aid < business::width_mu_out; ++aid)
        {
            business.setVariable<float>(action_factor(aid),
                *tensor_data::business::mu::out_addr(biz_id, aid));
        }
        ++biz_id;
    }
    macro_endfor_agents;
    macro_for_agents(urban_residence, residence, res_id)
    {
        bool exploring = flamerand::test_lt(exploration_rate, U);
        for (size_t af = 0; exploring and (af < residence::width_mu_out); ++af)
        {
            *tensor_data::residence::mu::out_addr(res_id, af)
                = flamerand::uniform<float>(U);
        }
        for (size_t aid = 0; aid < residence::width_mu_out; ++aid)
        {
            residence.setVariable<float>(action_factor(aid),
                *tensor_data::residence::mu::out_addr(res_id, aid));
        }
        ++res_id;
    }
    macro_endfor_agents;
    flamednn_sync_dev_data(residence, mu, out, residence::num_agents,
        residence::width_mu_out);
    if (step != 0 and step % 12 == 0)
    {
        macro_for_agents(forester, forester, for_id)
        {
            bool exploring = flamerand::test_lt(exploration_rate, U);
            for (size_t af = 0; exploring and (af < forester::width_mu_out); ++af)
            {
                *tensor_data::forester::mu::out_addr(for_id, af)
                    = flamerand::uniform<float>(U);
            }
            for (size_t aid = 0; aid < forester::width_mu_out; ++aid)
            {
                forester.setVariable<float>(action_factor(aid),
                    *tensor_data::forester::mu::out_addr(for_id, aid));
            }
            ++for_id;
        }
        macro_endfor_agents;
        macro_for_agents(farmer, farmer, farm_id)
        {
            bool exploring = flamerand::test_lt(exploration_rate, U);
            for (size_t af = 0; exploring and (af < farmer::width_mu_out); ++af)
            {
                *tensor_data::farmer::mu::out_addr(farm_id, af)
                    = flamerand::uniform<float>(U);
            }
            for (size_t aid = 0; aid < farmer::width_mu_out; ++aid)
            {
                farmer.setVariable<float>(action_factor(aid),
                    *tensor_data::farmer::mu::out_addr(farm_id, aid));
            }
            ++farm_id;
        }
        macro_endfor_agents;
    }
}
#endif
