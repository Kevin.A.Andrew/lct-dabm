#pragma once
#ifndef NETCONFIG_FILE
#define NETCONFIG_FILE config_nns.h
#include "config.h"
#include "macros_learning.h"

/// TODO VERIFY DEFAULTS


namespace tensors
{
namespace farmer
{
namespace mu
{
flamednn::Tensor in;
flamednn::Tensor l1, l2, l3, l4;
flamednn::Tensor out;
};
namespace q
{
flamednn::Tensor in;
flamednn::Tensor l1, l2, l3;
flamednn::Tensor out;
};
namespace memory
{
flamednn::Tensor bank;
flamednn::Tensor buffer;
};
};
namespace forester
{
namespace q
{
flamednn::Tensor in;
flamednn::Tensor l1, l2, l3;
flamednn::Tensor out;
};
namespace mu
{
flamednn::Tensor in;
flamednn::Tensor l1, l2, l3, l4;
flamednn::Tensor out;
};
namespace memory
{
flamednn::Tensor bank;
flamednn::Tensor buffer;
};
};
namespace residence {
namespace q {
flamednn::Tensor in;
flamednn::Tensor l1;
flamednn::Tensor l2;
flamednn::Tensor out;
};
namespace mu {
flamednn::Tensor in;
flamednn::Tensor l1;
flamednn::Tensor l2;
flamednn::Tensor out;
};
namespace memory {
flamednn::Tensor bank;
flamednn::Tensor buffer;
};
};
namespace business {
namespace q {
flamednn::Tensor in;
flamednn::Tensor l1;
flamednn::Tensor l2;
flamednn::Tensor out;
};
namespace mu {
flamednn::Tensor in;
flamednn::Tensor l1;
flamednn::Tensor l2;
flamednn::Tensor out;
};
namespace memory {
flamednn::Tensor bank;
flamednn::Tensor buffer;
};
};
};


namespace tensor_data
{
namespace farmer {
namespace q {
float* in;
float* in_dev;
float* l1;
float* l1_dev;
float* l2;
float* l2_dev;
float* l3;
float* l3_dev;
float* out;
float* out_dev;
};
namespace mu {
float* in;
float*
in_addr(const size_t& agt, const size_t& factor)
{
    return (in) ? in + agt * ::farmer::width_mu_in + factor : nullptr;
}
float* in_dev;
float* l1;
float* l1_dev;
float* l2;
float* l2_dev;
float* l3;
float* l3_dev;
float* l4;
float* l4_dev;
float* out;
float*
out_addr(const size_t& agt, const size_t& factor)
{
    return (out) ? out + agt * ::farmer::width_mu_out + factor : nullptr;
}
float* out_dev;
};
namespace memory {
float* bank;
float* buffer;
};
};
namespace forester {
namespace q {
float* in;
float* in_dev;
float* l1;
float* l1_dev;
float* l2;
float* l2_dev;
float* l3;
float* l3_dev;
float* out;
float* out_dev;
};
namespace mu {
float* in;
float*
in_addr(const size_t& agt, const size_t& factor)
{
    return (in) ? in + agt * ::forester::width_mu_in + factor : nullptr;
}
float* in_dev;
float* l1;
float* l1_dev;
float* l2;
float* l2_dev;
float* l3;
float* l3_dev;
float* l4;
float* l4_dev;
float* out;
float*
out_addr(const size_t& agt, const size_t& factor)
{
    return (out) ? out + agt * ::forester::width_mu_out + factor : nullptr;
}
float* out_dev;
};
namespace memory {
float* bank;
float* buffer;
};
};
namespace residence
{
namespace q
{
float* in;
float* in_dev;
float*
in_dev_addr(const size_t agt, const size_t offset=0)
{
    return (in_dev) ? in_dev + agt * ::residence::width_q_in + offset
                    : nullptr;
}
float* l1;
float* l1_dev;
float* l2;
float* l2_dev;
float* out;
float* out_dev;
float*
out_dev_addr(const size_t agt, const size_t offset=0)
{
    return (out_dev) ? out_dev + agt * ::residence::width_q_out + offset
                    : nullptr;
}
};
namespace mu
{
float* in;
float*
in_addr(const size_t& agt, const size_t& factor)
{
    return (in) ? in + agt * ::residence::width_mu_in + factor : nullptr;
}
float* in_dev;
float*
in_dev_addr(const size_t agt, const size_t offset=0)
{
    return (in_dev) ? in_dev + agt * ::residence::width_mu_in + offset
                    : nullptr;
}
float* l1;
float* l1_dev;
float* l2;
float* l2_dev;
float* out;
float*
out_addr(const size_t& agt, const size_t& factor=0)
{
    return (out) ? out + agt * ::residence::width_mu_out + factor : nullptr;
}
float* out_dev;
float*
out_dev_addr(const size_t& agt, const size_t& factor=0)
{
    return (out_dev) ? out_dev + agt * ::residence::width_mu_out + factor : nullptr;
}
};
namespace memory
{
float* bank;
float* bank_dev;
float*
bank_dev_addr(const size_t agt, const size_t mem)
{
    return (bank_dev) ? bank_dev + agt * (::residence::memory_depth
                                          * ::residence::width_mem)
                        + (mem * ::residence::width_mem)
                      : nullptr;
}
float* buffer;
float* buffer_dev;
float* buffer_dev_addr(const size_t agt, const size_t idx)
{
    return (buffer_dev) ? buffer_dev + agt * (::residence::replay_depth
                                              * ::residence::width_mem)
                          + (idx * ::residence::width_mem)
                        : nullptr;
}
};
};
namespace business
{
namespace q
{
float* in;
float* in_dev;
float*
in_dev_addr(const size_t agt, const size_t offset=0)
{
    return (in_dev) ? in_dev + agt * ::business::width_q_in + offset
                    : nullptr;
}
float* l1;
float* l1_dev;
float* l2;
float* l2_dev;
float* out;
float* out_dev;
float*
out_dev_addr(const size_t agt)
{
    return (out_dev) ? out_dev + agt * ::business::width_q_out
                     : nullptr;
}
};
namespace mu
{
float* in;
float*
in_addr(const size_t& agt, const size_t& factor)
{
    return (in) ? in + agt * ::business::width_mu_in + factor : nullptr;
}
float* in_dev;
float*
in_dev_addr(const size_t agt, const size_t offset=0)
{
    return (in_dev) ? in_dev + agt * ::business::width_mu_in + offset
                    : nullptr;
}
float* l1;
float* l1_dev;
float* l2;
float* l2_dev;
float* out;
float*
out_addr(const size_t& agt, const size_t& factor)
{
    return (out) ? out + agt * ::business::width_mu_out + factor : nullptr;
}
float* out_dev;
float*
out_dev_addr(const size_t agt)
{
    return (out_dev) ? out_dev + agt * ::business::width_mu_out
                     : nullptr;
}
};
namespace memory
{
float* bank;
float* bank_dev;
float*
bank_dev_addr(const size_t agt, const size_t mem)
{
    return (bank_dev) ? bank_dev + agt * (::business::memory_depth
                                          * ::business::width_mem)
                        + (mem * ::business::width_mem)
                      : nullptr;
}
float* buffer;
float* buffer_dev;
float* buffer_dev_addr(const size_t agt, const size_t idx)
{
    return (buffer_dev) ? buffer_dev + agt * (::business::replay_depth
                                              * ::business::width_mem)
                          + (idx * ::business::width_mem)
                        : nullptr;
}
};
};
};


namespace weights
{
namespace farmer
{
namespace q
{
flamednn::FilterDescriptor l1, l2, l3, out;
float* l1_host;
float* l1_dev;
float* l2_host;
float* l2_dev;
float* l3_host;
float* l3_dev;
float* out_host;
float* out_dev;
};
namespace mu
{
flamednn::FilterDescriptor l1, l2, l3, l4, out;
float* l1_host;
float* l1_dev;
float* l2_host;
float* l2_dev;
float* l3_host;
float* l3_dev;
float* l4_host;
float* l4_dev;
float* out_host;
float* out_dev;
};
};
namespace forester
{
namespace q
{
flamednn::FilterDescriptor l1, l2, l3, out;
float* l1_host;
float* l1_dev;
float* l2_host;
float* l2_dev;
float* l3_host;
float* l3_dev;
float* out_host;
float* out_dev;
};
namespace mu
{
flamednn::FilterDescriptor l1, l2, l3, l4, out;
float* l1_host;
float* l1_dev;
float* l2_host;
float* l2_dev;
float* l3_host;
float* l3_dev;
float* l4_host;
float* l4_dev;
float* out_host;
float* out_dev;
};
};
namespace business
{
namespace q
{
flamednn::FilterDescriptor l1, l2, out;
float* l1_host;
float* l1_dev;
float* l2_host;
float* l2_dev;
float* out_host;
float* out_dev;
};
namespace mu
{
flamednn::FilterDescriptor l1, l2, out;
float* l1_host;
float* l1_dev;
float* l2_host;
float* l2_dev;
float* out_host;
float* out_dev;
};
};
namespace residence
{
namespace q
{
flamednn::FilterDescriptor l1, l2, out;
float* l1_host;
float* l1_dev;
float* l2_host;
float* l2_dev;
float* out_host;
float* out_dev;
};
namespace mu
{
flamednn::FilterDescriptor l1, l2, out;
float* l1_host;
float* l1_dev;
float* l2_host;
float* l2_dev;
float* out_host;
float* out_dev;
};
};
};
#endif /* NETCONFIG_FILE */
