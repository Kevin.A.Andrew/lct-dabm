#pragma once
#include "config.h"
#include "macros_learning.h"
#include "config_nns.h"
#include "config_nns_tgt.h"

// TODO CLEANUP
// TODO FIX FREE / DEL SECTION SO ALL MEMORY IS FREED

flamednn::Handle dnn_handler;
flamednn::ActivationDescriptor relu_desc;
flamednn::ConvolutionDescriptor basic_conv;

float scale_alpha = 1.0f;
float scale_beta = 0.0f;

#define macro_save_weights_for(AGT, NET) \
    save_weights_ ## AGT ## _ ## NET


__host__
void
save_weights_to_file(const std::string& filename)
{
    flamelog::debug("all nets: writing weights");

    float* biz_mu_weights = new float[business::mu_size];
    float* biz_q_weights = new float[business::q_size];
    float* res_mu_weights = new float[residence::mu_size];
    float* res_q_weights = new float[residence::q_size];
    float* frm_mu_weights = new float[farmer::mu_size];
    float* frm_q_weights = new float[farmer::q_size];
    float* for_mu_weights = new float[forester::mu_size];
    float* for_q_weights = new float[forester::q_size];

    flamelog::debug("business-mu: writing weights");
    memcpy(biz_mu_weights, weights::business::mu::l1_host,
           business::l1_memsize);
    memcpy(biz_mu_weights + business::l1_size,
           weights::business::mu::l2_host, business::l2_memsize);
    memcpy(biz_mu_weights + business::l1_size + business::l2_size,
           weights::business::mu::out_host, business::out_memsize);
    flamednn::write_weights_to_file(biz_mu_weights, business::mu_size,
        filename.c_str());

    flamelog::debug("business-q: writing weights");
    memcpy(biz_q_weights, weights::business::q::l1_host,
           business::q1_memsize);
    memcpy(biz_q_weights + business::q1_size,
           weights::business::q::l2_host, business::q2_memsize);
    memcpy(biz_q_weights + business::q1_size + business::q2_size,
           weights::business::q::out_host, business::qout_memsize);
    flamednn::append_weights_to_file(biz_q_weights, business::q_size,
        filename.c_str());

    flamelog::debug("residence-mu: writing weights");
    memcpy(res_mu_weights, weights::residence::mu::l1_host,
           residence::l1_memsize);
    memcpy(res_mu_weights + residence::l1_size,
           weights::residence::mu::l2_host, residence::l2_memsize);
    memcpy(res_mu_weights + residence::l1_size + residence::l2_size,
           weights::residence::mu::out_host, residence::out_memsize);
    flamednn::append_weights_to_file(res_mu_weights, residence::mu_size,
        filename.c_str());

    flamelog::debug("residence-q: writing weights");
    memcpy(res_q_weights, weights::residence::q::l1_host,
           residence::q1_memsize);
    memcpy(res_q_weights + residence::q1_size,
           weights::residence::q::l2_host, residence::q2_memsize);
    memcpy(res_q_weights + residence::q1_size + residence::q2_size,
           weights::residence::q::out_host, residence::qo_memsize);
    flamednn::append_weights_to_file(res_q_weights, residence::q_size,
        filename.c_str());

    flamelog::debug("farmer-mu: writing weights");
    memcpy(frm_mu_weights, weights::farmer::mu::l1_host,
           farmer::l1_memsize);
    memcpy(frm_mu_weights + farmer::l1_size,
           weights::farmer::mu::l2_host, farmer::l2_memsize);
    memcpy(frm_mu_weights + farmer::l1_size + farmer::l2_size,
           weights::farmer::mu::l3_host, farmer::out_memsize);
    memcpy(frm_mu_weights + farmer::l1_size + farmer::l2_size
           + farmer::l3_size,
           weights::farmer::mu::l4_host, farmer::out_memsize);
    memcpy(frm_mu_weights + farmer::l1_size + farmer::l2_size
           + farmer::l3_size + farmer::l4_size,
           weights::farmer::mu::out_host, farmer::out_memsize);
    flamednn::append_weights_to_file(frm_mu_weights, farmer::mu_size,
        filename.c_str());

    flamelog::debug("farmer-q: writing weights");
    memcpy(frm_q_weights, weights::farmer::q::l1_host,
           farmer::q1_memsize);
    memcpy(frm_q_weights + farmer::q1_size,
           weights::farmer::q::l2_host, farmer::q2_memsize);
    memcpy(frm_q_weights + farmer::q1_size + farmer::q2_size,
           weights::farmer::q::l3_host, farmer::q3_memsize);
    memcpy(frm_q_weights + farmer::q1_size + farmer::q2_size
           + farmer::q3_size,
           weights::farmer::q::out_host, farmer::qo_memsize);
    flamednn::append_weights_to_file(frm_q_weights, farmer::q_size,
        filename.c_str());

    flamelog::debug("forester-mu: writing weights");
    memcpy(for_mu_weights, weights::forester::mu::l1_host,
           forester::l1_memsize);
    memcpy(for_mu_weights + forester::l1_size,
           weights::forester::mu::l2_host, forester::l2_memsize);
    memcpy(for_mu_weights + forester::l1_size + forester::l2_size,
           weights::forester::mu::l3_host, forester::out_memsize);
    memcpy(for_mu_weights + forester::l1_size + forester::l2_size
           + forester::l3_size,
           weights::forester::mu::l4_host, forester::out_memsize);
    memcpy(for_mu_weights + forester::l1_size + forester::l2_size
           + forester::l3_size + forester::l4_size,
           weights::forester::mu::out_host, forester::out_memsize);
    flamednn::append_weights_to_file(for_mu_weights, forester::mu_size,
        filename.c_str());

    flamelog::debug("forester-q: writing weights");
    memcpy(for_q_weights, weights::forester::q::l1_host,
           forester::q1_memsize);
    memcpy(for_q_weights + forester::q1_size,
           weights::forester::q::l2_host, forester::q2_memsize);
    memcpy(for_q_weights + forester::q1_size + forester::q2_size,
           weights::forester::q::l3_host, forester::q3_memsize);
    memcpy(for_q_weights + forester::q1_size + forester::q2_size
           + forester::q3_size,
           weights::forester::q::out_host, forester::qo_memsize);
    flamednn::append_weights_to_file(for_q_weights, forester::q_size,
        filename.c_str());
}


__host__
void
save_weights_business_mu(const std::string& fname)
{
    fprintf(stdout, "business-mu: writing weights to `%s'\n", fname.c_str());
    float* biz_mu_weights = new float[business::mu_size];
    memcpy(biz_mu_weights, weights::business::mu::l1_host,
           business::l1_memsize);
    memcpy(biz_mu_weights + business::l1_size,
           weights::business::mu::l2_host, business::l2_memsize);
    memcpy(biz_mu_weights + business::l1_size + business::l2_size,
           weights::business::mu::out_host, business::out_memsize);
    flamednn::write_weights_to_file(biz_mu_weights, business::mu_size,
        fname.c_str());
}


__host__
void
save_weights_business_q(const std::string& fname)
{
    fprintf(stdout, "business-q : writing weights to `%s'\n", fname.c_str());
    float* biz_q_weights = new float[business::q_size];
    memcpy(biz_q_weights, weights::business::q::l1_host,
           business::q1_memsize);
    memcpy(biz_q_weights + business::q1_size,
           weights::business::q::l2_host, business::q2_memsize);
    memcpy(biz_q_weights + business::q1_size + business::q2_size,
           weights::business::q::out_host, business::qout_memsize);
    flamednn::write_weights_to_file(biz_q_weights, business::q_size,
        fname.c_str());
}


__host__
void
free_memory_cudnn()
{
    /// Becaue this is only called at the end, don't actually /need/ to free
    /// anything, so this can be pushed back as an if-needed

    flamednn::destroy(dnn_handler);
}

