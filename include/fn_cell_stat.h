#pragma once
#include "config.h"

FLAMEGPU_AGENT_FUNCTION(\
cell_stat_, flamegpu::MessageNone, flamegpu::MessageBucket)
{
    FLAMEGPU->message_out.setKey(FLAMEGPU->getVariable<unsigned>("parcel_id"));
    FLAMEGPU->message_out.setVariable<unsigned>("cover",
        FLAMEGPU->getVariable<unsigned>("land_cover"));
}
