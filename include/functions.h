#pragma once
// TODO CLEANUP / RESTRUCTURE
#include "config.h"
#include "functions/transition_agent_states.h"
#include "functions/business_functions.h"
#include "fn_cell_stat.h"
#include "functions/forest_functions.h"
#include "functions/residence_functions.h"
#include "farm_funcs.h"
#include "cell_funcs.h"
#include "initialize_learning.h"
#include "initialize_agents.h"
