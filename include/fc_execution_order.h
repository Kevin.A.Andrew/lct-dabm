#pragma once
#ifndef FLAMEGPU_CONFIG_EXECUTION_ORDER_H_
#define FLAMEGPU_CONFIG_EXECUTION_ORDER_H_
#include "config.h"
#include "functions.h"
#include "fc_support.h"

/// TODO REMOVE REDUNDANT CALLS AND DOUBLE-CHECK LAYERING
///      CLEANUP FILE

extern flamegpu::ModelDescription model;

void
define_execution_order()
{
    model.addInitFunction(initialize_agents);
    model.addInitFunction(initialize_learning_flame);

    auto cell_agt = model.Agent("landcell");
    auto frm_agt = model.Agent("farmer");
    auto for_agt = model.Agent("forester");
    auto res_agt = model.Agent("urban_residence");
    auto com_agt = model.Agent("urban_business");

    // # Quick Stat
    auto cell_stat = cell_agt.newFunction("cell_stat", cell_stat_);
    auto business_cell_stat = com_agt.newFunction("business_cell_stat", business_cell_stat_);
    auto farm_cell_stat = frm_agt.newFunction("farm_cell_stat", farm_cell_stat_);

    cell_stat.setMessageOutput("cell_update");
    business_cell_stat.setMessageInput("cell_update");
    farm_cell_stat.setMessageInput("cell_update");

    model.addExecutionRoot(cell_stat);
    business_cell_stat.dependsOn(cell_stat);
    farm_cell_stat.dependsOn(cell_stat);

    // # Agent Decisions
    flamegpu::HostFunctionDescription agents_decide_actions(
        "agents_decide_actions", agents_decide_actions_);

    // # Taking Actions
    auto forester_take_action
        = for_agt.newFunction("forester_take_action", forester_take_action_);
    forester_take_action.setMessageOutput("forest_landuse");
    auto business_take_action
        = com_agt.newFunction("business_take_action", business_take_action_);
    auto residence_take_action
        = res_agt.newFunction("residence_take_action", residence_take_action_);

    // # Business Functions

    // ## Job Market
    auto business_update_salaries
        = com_agt.newFunction("business_update_salaries",
                              business_update_salaries_);
    business_update_salaries.setFunctionCondition(is_updating_sals);
    business_update_salaries.setMessageOutput("biz_raise");

    auto residence_raised = res_agt.newFunction("residence_raised",
                                                residence_raised_);
    residence_raised.setFunctionCondition(has_job);
    residence_raised.setMessageInput("biz_raise");

    auto business_fire = com_agt.newFunction("business_fire", business_fire_);
    business_fire.setFunctionCondition(is_firing);
    business_fire.setMessageOutput("biz_fire");

    auto residence_fired = res_agt.newFunction("residence_fired", residence_fired_);
    residence_fired.setFunctionCondition(has_job);
    residence_fired.setMessageInput("biz_fire");

    auto business_list_jobs = com_agt.newFunction("business_list_jobs", business_list_jobs_);
    business_list_jobs.setFunctionCondition(is_hiring);
    business_list_jobs.setMessageOutput("biz_is_hiring");
    business_list_jobs.setMessageOutputOptional(true);

    auto residence_find_job = res_agt.newFunction("residence_find_job",
                                                  residence_find_job_);
    residence_find_job.setFunctionCondition(is_seeking_job);
    residence_find_job.setMessageInput("biz_is_hiring");
    residence_find_job.setMessageOutput("apply_to_job");
    residence_find_job.setMessageOutputOptional(true);

    auto business_fill_job = com_agt.newFunction("business_fill_job",
                                                 business_fill_job_);
    business_fill_job.setMessageInput("apply_to_job");
    business_fill_job.setMessageOutput("job_fill");
    business_fill_job.setMessageOutputOptional(true);

    auto residence_add_job = res_agt.newFunction("residence_add_job",
                                                 residence_add_job_);
    residence_add_job.setFunctionCondition(is_seeking_job);
    residence_add_job.setMessageInput("job_fill");
    residence_add_job.setMessageOutput("job_leave");
    residence_add_job.setMessageOutputOptional(true);

    auto business_quits = com_agt.newFunction("business_quits",
                                              business_quits_);
    business_quits.setMessageInput("job_leave");

    /// # Housing Market
    auto residence_list_house = res_agt.newFunction("residence_list_house",
                                                    residence_list_house_);
    residence_list_house.setFunctionCondition(residence_is_vacant);
    residence_list_house.setMessageOutput("apt_listing");

    auto residence_find_house = res_agt.newFunction("residence_find_house",
                                                    residence_find_house_);
    residence_find_house.setFunctionCondition(is_seeking_house);
    residence_find_house.setMessageInput("apt_listing");
    residence_find_house.setMessageOutput("apt_offer");
    residence_find_house.setMessageOutputOptional(true);

    auto residence_accept_offer = res_agt.newFunction("residence_accept_offer",
                                                      residence_accept_offer_);
    residence_accept_offer.setFunctionCondition(residence_is_vacant);
    residence_accept_offer.setMessageInput("apt_offer");
    residence_accept_offer.setMessageOutput("apt_sale");
    residence_accept_offer.setMessageOutputOptional(true);

    auto residence_move_house = res_agt.newFunction("residence_move_house",
                                                    residence_move_house_);
    residence_move_house.setFunctionCondition(is_seeking_house);
    residence_move_house.setMessageInput("apt_sale");

    /// # Forestry Functions
    auto forest_message_neighbors
        = for_agt.newFunction("forest_message_neighbors", forest_message_neighbors_);
    forest_message_neighbors.setFunctionCondition(is_forestry_step);
    forest_message_neighbors.setMessageOutput("forest_update");

    auto forest_update_neighbors
        = for_agt.newFunction("forest_update_neighbors", forest_update_neighbors_);
    forest_update_neighbors.setFunctionCondition(is_forestry_step);
    forest_update_neighbors.setMessageInput("forest_update");

    /// # Farm Functions
    auto farms_use_land = frm_agt.newFunction("farms_use_land", farms_use_land_);
    farms_use_land.setFunctionCondition(farms_step);
    farms_use_land.setMessageOutput("farm_landuse");

    auto update_farm_use = cell_agt.newFunction("update_farm_use", update_farm_use_);
    update_farm_use.setMessageInput("farm_landuse");
    update_farm_use.setMessageOutput("cell_update");

    auto farms_message_neighbors
        = frm_agt.newFunction("farms_message_neighbors", farms_message_neighbors_);
    farms_message_neighbors.setFunctionCondition(farms_step);
    farms_message_neighbors.setMessageOutput("farm_update");

    auto farms_update_neighbors
        = frm_agt.newFunction("farms_update_neighbors", farms_update_neighbors_);
    farms_update_neighbors.setFunctionCondition(farms_step);
    farms_update_neighbors.setMessageInput("farm_update");

    /// # Step Functions
    auto business_step = com_agt.newFunction("business_step", business_step_);
    auto residence_step = res_agt.newFunction("residence_step", residence_step_);
    auto forest_step = for_agt.newFunction("forest_step", forest_step_);
    auto farm_step = frm_agt.newFunction("farm_step", farms_step_);

    /// # Learning Interface
    flamegpu::HostFunctionDescription business_evaluate_action(
        "business_evaluate_action", business_evaluate_action_);
    flamegpu::HostFunctionDescription residence_evaluate_action(
        "residence_evaluate_action", residence_evaluate_action_);

    /// # State Transition
    auto business_transition_state = com_agt.newFunction(
        "business_transition_state", business_transition_state_);
    auto forest_transition_state = for_agt.newFunction(
        "forest_transition_state", forest_transition_state_);
    auto residence_transition_state = res_agt.newFunction(
        "residence_transtion_state", residence_transition_state_);
    auto farmer_transition_state = frm_agt.newFunction(
        "farmer_transition_state", farmer_transition_state_);

    /// # Execution Dependency Tree
    model.addExecutionRoot(agents_decide_actions);

    business_take_action.dependsOn(agents_decide_actions);
    residence_take_action.dependsOn(agents_decide_actions);
    forester_take_action.dependsOn(agents_decide_actions);

    /// ## Job Market
    business_fire.dependsOn(business_take_action);
    residence_fired.dependsOn(business_fire, residence_take_action);

    business_update_salaries.dependsOn(business_take_action);
    residence_raised.dependsOn(business_update_salaries);

    business_list_jobs.dependsOn(business_take_action);
    residence_find_job.dependsOn(business_list_jobs, residence_take_action);
    business_fill_job.dependsOn(residence_find_job);
    residence_add_job.dependsOn(business_fill_job);
    business_quits.dependsOn(residence_add_job);

    /// ## Housing Market
    model.addExecutionRoot(residence_list_house);
    residence_find_house.dependsOn(residence_list_house, residence_take_action);
    residence_accept_offer.dependsOn(residence_find_house);
    residence_move_house.dependsOn(residence_accept_offer);

    forest_message_neighbors.dependsOn(forester_take_action);
    forest_update_neighbors.dependsOn(forest_message_neighbors);

    model.addExecutionRoot(farms_message_neighbors);
    farms_update_neighbors.dependsOn(farms_message_neighbors);

    model.addExecutionRoot(farms_use_land);
    update_farm_use.dependsOn(farms_use_land);

    business_step.dependsOn(business_take_action, business_fill_job,
                            business_fire, business_update_salaries);
    forest_step.dependsOn(forest_update_neighbors);
    residence_step.dependsOn(residence_move_house, residence_add_job,
                             residence_fired, residence_raised);
    farm_step.dependsOn(farms_update_neighbors, update_farm_use);

    business_evaluate_action.dependsOn(business_step);
    residence_evaluate_action.dependsOn(residence_step);

    business_transition_state.dependsOn(business_evaluate_action);
    forest_transition_state.dependsOn(forest_step);
    residence_transition_state.dependsOn(residence_evaluate_action);
    farmer_transition_state.dependsOn(farm_step);

    model.addStepFunction(model_step);
    model.addExitFunction(end_run);
    model.addExitCondition(training_is_finished);

    model.generateLayers();

//    std::string actualLayers = model.getConstructedLayersString();
//    std::cout << actualLayers << std::endl;
}

#endif
