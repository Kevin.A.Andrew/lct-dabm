#pragma once 
#ifndef FLAMEGPU_CONFIG_SUPPORT_H_
#define FLAMEGPU_CONFIG_SUPPORT_H_
#include "config.h"
#include <ctime>

/// TODO CLEANUP AND FIX CALLS / PARAMS
/// USING SHORT VERSIONS FROM TESTING BUT CAN MOVE TO LONG VERSIONS

#include <future>

#define LCT_RM_BIN 0
#define LCT_RM_CSV 1

extern int pipe_mode;
extern const char *pipe_name;
extern int raster_mode;

extern flamegpu::ModelDescription model;

FLAMEGPU_EXIT_CONDITION(\
training_is_finished)
{
    // TODO REMOVE TEST VALUES
#define STEP_MAX 4000
    if (FLAMEGPU->getStepCounter() >= STEP_MAX)
        return flamegpu::EXIT;
    else
        return flamegpu::CONTINUE;
}

/* Write the land-cover raster to a binary file, ideally a pipe, for use.
 * The data has the following format:
 * TODO: A header block on the first transmission: [ zu:VERSION , zu:YEAR , zu:LENGTH , zu:WIDTH ]
 *      Followed by LENGTH lines of WIDTH uint32 values: [ X0Y0 ; X1Y0 ; X2Y0 ;; ]
 * TODO: Fix and cleanup so that it actually writes to pipes and not just files
 */
static bool has_sent_header = false;
static bool is_writing_file = false;
__host__ void
write_raster_to_pipe(const std::string& filename,
    const size_t& length, const size_t& width, const size_t& year,
    unsigned* cover_stream)
{
    while (is_writing_file) {}
    flamelog::debug("writing raster (%zu) to pipe/file `%s'", year, filename.c_str());
    is_writing_file = true;
    std::ofstream file(filename.c_str(), std::ios::binary|std::ios::app);
    if (file.is_open())
    {
        /* FIXME -- fix header so not requiring hard coded sizes
        if (not has_sent_header)
        {
            file.write(0, sizeof(size_t));
            file.write(reinterpret_cast<char*>(year), sizeof(size_t));
            file.write(std::to_string(length).c_str(), sizeof(size_t));
            file.write(reinterpret_cast<char*>(width), sizeof(size_t));
            has_sent_header = true;
        }
        */
        file.write(reinterpret_cast<char*>(cover_stream), length * width * sizeof(unsigned));
        file.close();
    }
    is_writing_file = false;
}

__host__ void
csvwrite_raster_to_pipe(const std::string& filename,
    const size_t& length, const size_t& width, const size_t& year,
    unsigned* cover_stream)
{
    while (is_writing_file) {}
    flamelog::debug("writing raster (%zu) to pipe/file `%s'", year, filename.c_str());
    std::ofstream file(filename.c_str(), std::ios::app);
    if (file.is_open())
    {
        for (size_t y = 0; y < length; ++y)
        {
            for (size_t x = 0; x < width; ++x)
            {
                if (x != 0) file << ",";
                file << cover_stream[x + (width * y)];
            }
            file << std::endl;
        }
        file.close();
    }
    is_writing_file = false;
}

FLAMEGPU_STEP_FUNCTION(\
model_step)
{
    using namespace lct_abm;
    unsigned current_step, current_month, current_year;
    bool first_step, year_updating, is_checkpoint_step;
    unsigned slength_x, slength_y, sarea;

    current_step = FLAMEGPU->getStepCounter();
    current_month = FLAMEGPU->environment.getProperty<unsigned>("model_month");
    current_year = FLAMEGPU->environment.getProperty<unsigned>("model_year");
    slength_x = FLAMEGPU->environment.getProperty<unsigned>("slength_x");
    slength_y = FLAMEGPU->environment.getProperty<unsigned>("slength_y");
    sarea = FLAMEGPU->environment.getProperty<unsigned>("sarea");

    first_step = (current_step == 0);
    year_updating = (not first_step) and (current_month % 12 == 0);
    is_checkpoint_step = (not first_step)
                         and (current_step % checkpoint_step == 0);

    tm *t;
    t = new tm;
    t->tm_year = current_year + 100;
    t->tm_mon = current_month % 12;
    char buffer[100];
    strftime(buffer, 100, "%Y %b", t);
//    flamelog::info("%s", buffer);
//    flamelog::info("Year %u, Month %u", current_year, current_month % 12 + 1);

    FLAMEGPU->environment.setProperty<unsigned>("model_month", current_month+1);
    if (year_updating)
    {
        FLAMEGPU->environment.setProperty<unsigned>("model_year", current_year+1);
        /// TODO: Make optional
        /// TODO: Remove hard coded values
        unsigned *cover_stream;
        cover_stream = (unsigned*)xnmalloc(sarea, sizeof(unsigned));
//        unsigned cover_stream[315 * 237];
        HostAgentAPI cells_api = FLAMEGPU->agent("landcell");
        DeviceAgentVector cells_vec = cells_api.getPopulationData();
        size_t counter = 0;
        for (auto cell : cells_vec)
        {
            cover_stream[counter] = cell.getVariable<unsigned>("land_cover");
            ++counter;
        }
        // TODO | NEED TO TEST THIS
        if (pipe_mode != 0)
        {
            future<void> write_raster;
            switch (raster_mode)
            {
                case LCT_RM_CSV:
                    write_raster = async(launch::async,
                        csvwrite_raster_to_pipe, pipe_name, slength_y, slength_x,
                        current_year, cover_stream);
                    break;
                case LCT_RM_BIN:
                    write_raster = async(launch::async,
                        write_raster_to_pipe, pipe_name, slength_y, slength_x,
                        current_year, cover_stream);
                    break;
            }
        }
    }

    if (is_checkpoint_step)
    {
        string filename = string("checkpoint-")
            + to_string(current_step) + string(".bin");
        future<void> write_checkpoint
            = async(launch::async, save_weights_to_file,
                         filename);
        transfer_learning_business_mu(0.01f);
    }
}

FLAMEGPU_EXIT_FUNCTION(\
end_run)
{
    using namespace lct_abm;
    if (operating_mode != testing_mode)
    {
        future<void> write_weights
            = async(launch::async, save_weights_to_file, "weights.bin");
        write_weights.wait();
    }
    flamelog::info("End of Run");
}

#endif
