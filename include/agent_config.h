#pragma once
#include "config.h"
#include "defaults.h"

typedef unsigned mood_t;


/** FARMER CONFIG ———————————————————————————————————————————————————————————*/
/*** @TODO Fix Includes and 'mood' location */
#ifndef FARMER_CONFIG
namespace farmer
{
/* global info */
const unsigned num_agents = FARM_NUM;
const size_t max_neighbors = FARM_MAX_NEIGHBORS;

/* actor networks (mu) */
const size_t width_mu_in = FARM_MU_NODES_IN;
const size_t width_mu_l1 = FARM_MU_NODES_L1;
const size_t width_mu_l2 = FARM_MU_NODES_L2;
const size_t width_mu_l3 = FARM_MU_NODES_L3;
const size_t width_mu_l4 = FARM_MU_NODES_L4;
const size_t width_mu_out = FARM_MU_NODES_OUT;
const size_t l1_size = FARM_MU_NODES_IN * FARM_MU_NODES_L1;
const size_t l2_size = FARM_MU_NODES_L1 * FARM_MU_NODES_L2;
const size_t l3_size = FARM_MU_NODES_L2 * FARM_MU_NODES_L3;
const size_t l4_size = FARM_MU_NODES_L3 * FARM_MU_NODES_L4;
const size_t out_size = FARM_MU_NODES_L4 * FARM_MU_NODES_OUT;
const size_t l1_memsize = l1_size * sizeof(float);
const size_t l2_memsize = l2_size * sizeof(float);
const size_t l3_memsize = l3_size * sizeof(float);
const size_t l4_memsize = l4_size * sizeof(float);
const size_t out_memsize = out_size * sizeof(float);
const size_t mu_size = l1_size + l2_size + l3_size + l4_size + out_size;

/* critic networks (q) */
const size_t width_q_in = FARM_Q_NODES_IN;
const size_t width_q_l1 = FARM_Q_NODES_L1;
const size_t width_q_l2 = FARM_Q_NODES_L2;
const size_t width_q_l3 = FARM_Q_NODES_L3;
const size_t width_q_out = FARM_Q_NODES_OUT;
const size_t q1_size = FARM_Q_NODES_IN * FARM_Q_NODES_L1;
const size_t q2_size = FARM_Q_NODES_L1 * FARM_Q_NODES_L2;
const size_t q3_size = FARM_Q_NODES_L2 * FARM_Q_NODES_L3;
const size_t qo_size = FARM_Q_NODES_L3 * FARM_Q_NODES_OUT;
const size_t q1_memsize = q1_size * sizeof(float);
const size_t q2_memsize = q2_size * sizeof(float);
const size_t q3_memsize = q3_size * sizeof(float);
const size_t qo_memsize = qo_size * sizeof(float);
const size_t q_size = q1_size + q2_size + q3_size + qo_size;

/* agent memory */
const size_t local_action_memory = FARM_MEM_LOCAL;
const size_t local_action_memory_min = FARM_MEM_LOCAL_MIN;
const size_t local_action_memory_max = FARM_MEM_LOCAL_MAX;
const size_t width_mem = FARM_Q_NODES_IN + 1;
const size_t memory_depth = FARM_MEMORY_M;
const size_t replay_depth = FARM_MEMORY_B;

/* land cover categorization */
const unsigned nlcd_cats = NLCD_CATS_AG;
const size_t nlcdi_x = 0;
const size_t nlcdi_cr = 1;
const size_t nlcdi_ps = 2;
const size_t nlcdi_gr = 3;
const size_t nlcdi_fx = 4;

#define FARM_COV_OTHER      COVCAT(0)
#define FARM_COV_CROP       COVCAT(1)
#define FARM_COV_PASTURE    COVCAT(2)
#define FARM_COV_GRASS      COVCAT(3)
#define FARM_COV_FOREST     COVCAT(4)
#define FARM_COV_TOTAL      COVCAT(5)

/* meta-state params */
const mood_t fin_good = 0;
const mood_t fin_bad = 1;
const mood_t fin_worse = 2;

/* shifted action indices */
const size_t action_nobmp = 0;
const size_t action_bmp = 1;

/* parameterization */
const float corn_prod_min = FARMER_CORN_MIN;
const float corn_prod_max = FARMER_CORN_MAX;
const float corn_prod_mean = FARMER_CORN_MEAN;
const float hay_prod_min = FARMER_HAY_MIN;
const float hay_prod_max = FARMER_HAY_MAX;
const float hay_prod_mean = FARMER_HAY_MEAN;
const float prod_beef_min = FARMER_MEAT_MIN;
const float prod_beef_max = FARMER_MEAT_MAX;
const float prod_beef_mean = FARMER_MEAT_MEAN;
const float prod_dairy_min = FARMER_MILK_MIN;
const float prod_dairy_max = FARMER_MILK_MAX;
const float prod_dairy_mean = FARMER_MILK_MEAN;

#define FARM_S_CORN_PROD        STTCAT(0)
#define FARM_S_PROD_HAY         STTCAT(1)
#define FARM_S_PROD_BEEF        STTCAT(2)
#define FARM_S_PROD_DAIRY       STTCAT(3)
#define FARM_S_NEIGHBOR_BMP     STTCAT(4)
#define FARM_S_NEIGHBOR_LOSS    STTCAT(5)
#define FARM_S_COV_PROP_CROP    STTCAT(6)
#define FARM_S_COV_PROP_PASTURE STTCAT(7)
#define FARM_S_COV_PROP_FOREST  STTCAT(8)
#define FARM_SS_CORN_PROD           NSTCAT(0)
#define FARM_SS_PROD_HAY            NSTCAT(1)
#define FARM_SS_PROD_BEEF           NSTCAT(2)
#define FARM_SS_PROD_DAIRY          NSTCAT(3)
#define FARM_SS_NEIGHBOR_BMP        NSTCAT(4)
#define FARM_SS_NEIGHBOR_LOSS       NSTCAT(5)
#define FARM_SS_COV_PROP_CROP       NSTCAT(6)
#define FARM_SS_COV_PROP_PASTURE    NSTCAT(7)
#define FARM_SS_COV_PROP_FOREST     NSTCAT(8)
#define FARM_A_NOBMP        ACTCAT(0)
#define FARM_A_BMP          ACTCAT(1)
#define FARM_A_EXP_CORN     ACTCAT(2)
#define FARM_A_MTN_CORN     ACTCAT(3)
#define FARM_A_SHR_CORN     ACTCAT(4)
#define FARM_A_EXP_HAY      ACTCAT(5)
#define FARM_A_MTN_HAY      ACTCAT(6)
#define FARM_A_SHR_HAY      ACTCAT(7)
#define FARM_A_EXP_BEEF     ACTCAT(8)
#define FARM_A_MTN_BEEF     ACTCAT(9)
#define FARM_A_SHR_BEEF     ACTCAT(10)
#define FARM_A_EXP_DAIRY    ACTCAT(11)
#define FARM_A_MTN_DAIRY    ACTCAT(12)
#define FARM_A_SHR_DAIRY    ACTCAT(13)
#define FARM_AA_NOBMP       RACTCAT(0)
#define FARM_AA_BMP         RACTCAT(1)
#define FARM_AA_EXP_CORN    RACTCAT(2)
#define FARM_AA_MTN_CORN    RACTCAT(3)
#define FARM_AA_SHR_CORN    RACTCAT(4)
#define FARM_AA_EXP_HAY     RACTCAT(5)
#define FARM_AA_MTN_HAY     RACTCAT(6)
#define FARM_AA_SHR_HAY     RACTCAT(7)
#define FARM_AA_EXP_BEEF    RACTCAT(8)
#define FARM_AA_MTN_BEEF    RACTCAT(9)
#define FARM_AA_SHR_BEEF    RACTCAT(10)
#define FARM_AA_EXP_DAIRY   RACTCAT(11)
#define FARM_AA_MTN_DAIRY   RACTCAT(12)
#define FARM_AA_SHR_DAIRY   RACTCAT(13)
};
#endif /* FARMER_CONFIG */


/** FOREST CONFIG ———————————————————————————————————————————————————————————*/
#ifndef FOREST_CONFIG
namespace forester
{
/* global info */
const unsigned num_agents = FOR_NUM;
const size_t max_neighbors = FOREST_MAX_NEIGHBORS;

/* actor networks (mu) */
const size_t width_mu_in = FOREST_MU_NODES_IN;
const size_t width_mu_l1 = FOREST_MU_NODES_L1;
const size_t width_mu_l2 = FOREST_MU_NODES_L2;
const size_t width_mu_l3 = FOREST_MU_NODES_L3;
const size_t width_mu_l4 = FOREST_MU_NODES_L4;
const size_t width_mu_out = FOREST_MU_NODES_OUT;
const size_t l1_size = FOREST_MU_NODES_IN * FOREST_MU_NODES_L1;
const size_t l2_size = FOREST_MU_NODES_L1 * FOREST_MU_NODES_L2;
const size_t l3_size = FOREST_MU_NODES_L2 * FOREST_MU_NODES_L3;
const size_t l4_size = FOREST_MU_NODES_L3 * FOREST_MU_NODES_L4;
const size_t out_size = FOREST_MU_NODES_L4 * FOREST_MU_NODES_OUT;
const size_t l1_memsize = l1_size * sizeof(float);
const size_t l2_memsize = l2_size * sizeof(float);
const size_t l3_memsize = l3_size * sizeof(float);
const size_t l4_memsize = l4_size * sizeof(float);
const size_t out_memsize = out_size * sizeof(float);
const size_t mu_size = l1_size + l2_size + l3_size + l4_size + out_size;

/* critic networks (q) */
const size_t width_q_in = FOREST_Q_NODES_IN;
const size_t width_q_l1 = FOREST_Q_NODES_L1;
const size_t width_q_l2 = FOREST_Q_NODES_L2;
const size_t width_q_l3 = FOREST_Q_NODES_L3;
const size_t width_q_out = FOREST_Q_NODES_OUT;
const size_t q1_size = width_q_in * width_q_l1;
const size_t q2_size = width_q_l1 * width_q_l2;
const size_t q3_size = width_q_l2 * width_q_l3;
const size_t qo_size = width_q_l3 * width_q_out;
const size_t q1_memsize = q1_size * sizeof(float);
const size_t q2_memsize = q2_size * sizeof(float);
const size_t q3_memsize = q3_size * sizeof(float);
const size_t qo_memsize = qo_size * sizeof(float);
const size_t q_size = q1_size + q2_size + q3_size + qo_size;

/* agent memory */
const size_t local_action_memory = FOREST_MEM_LOCAL;
const size_t local_action_memory_min = FOREST_MEM_LOCAL_MIN;
const size_t local_action_memory_max = FOREST_MEM_LOCAL_MAX;
const size_t width_mem = FOREST_Q_NODES_IN + 1;
const size_t memory_depth = FOREST_MEMORY_M;
const size_t replay_depth = FOREST_MEMORY_B;

/* land cover categorization */
const unsigned nlcd_cats = 6;
const size_t nlcdi_x = 0;
const size_t nlcdi_fx = 1;
const size_t nlcdi_fd = 2;
const size_t nlcdi_fm = 3;
const size_t nlcdi_fc = 4;

/* meta-state params */
const mood_t fin_good = 0;
const mood_t fin_bad = 1;
const mood_t fin_worse = 2;

/* shifted action indices */
const size_t action_noamp = 0;
const size_t action_amp = 1;
const size_t action_expand = 0;
const size_t action_maintain = 1;
const size_t action_contract = 2;

/* asociated defines */
#define FORACT_NOAMP    ACTCAT(0)
#define FORACT_AMP      ACTCAT(1)
#define FORACT_EXP      ACTCAT(2)
#define FORACT_MTN      ACTCAT(3)
#define FORACT_SHR      ACTCAT(4)
#define RFORACT_NOAMP   RACTCAT(0)
#define RFORACT_AMP     RACTCAT(1)
#define RFORACT_EXP     RACTCAT(2)
#define RFORACT_MTN     RACTCAT(3)
#define RFORACT_SHR     RACTCAT(4)

};
#endif /* FOREST_CONFIG */

/** BUSINESS CONFIG —————————————————————————————————————————————————————————*/
#ifndef BUSINESS_CONFIG
namespace business
{
/* global info */
const unsigned num_agents = BIZ_NUM;

/* actor networks (mu) */
const size_t width_mu_in = BIZ_MU_NODES_IN;
const size_t width_mu_l1 = BIZ_MU_NODES_L1;
const size_t width_mu_l2 = BIZ_MU_NODES_L2;
const size_t width_mu_out = BIZ_MU_NODES_OUT;
const size_t l1_size = BIZ_MU_NODES_IN * BIZ_MU_NODES_L1;
const size_t l1_memsize = BIZ_MU_NODES_IN * BIZ_MU_NODES_L1 * sizeof(float);
const size_t l2_size = BIZ_MU_NODES_L1 * BIZ_MU_NODES_L2;
const size_t l2_memsize = BIZ_MU_NODES_L1 * BIZ_MU_NODES_L2 * sizeof(float);
const size_t out_size = BIZ_MU_NODES_L2 * BIZ_MU_NODES_OUT;
const size_t out_memsize = BIZ_MU_NODES_L2 * BIZ_MU_NODES_OUT * sizeof(float);
const size_t mu_size = BIZ_MU_NODES_IN * BIZ_MU_NODES_L1
                       + BIZ_MU_NODES_L1 * BIZ_MU_NODES_L2
                       + BIZ_MU_NODES_L2 * BIZ_MU_NODES_OUT;

/* critic networks (q) */
const size_t width_q_in = BIZ_Q_NODES_IN;
const size_t width_q_l1 = BIZ_Q_NODES_L1;
const size_t width_q_l2 = BIZ_Q_NODES_L2;
const size_t width_q_out = BIZ_Q_NODES_OUT;
const size_t q1_size = BIZ_Q_NODES_IN * BIZ_Q_NODES_L1;
const size_t q1_memsize = BIZ_Q_NODES_IN * BIZ_Q_NODES_L1 * sizeof(float);
const size_t q2_size = BIZ_Q_NODES_L1 * BIZ_Q_NODES_L2;
const size_t q2_memsize = BIZ_Q_NODES_L1 * BIZ_Q_NODES_L2 * sizeof(float);
const size_t qout_memsize = BIZ_Q_NODES_L2 * BIZ_Q_NODES_OUT * sizeof(float);
const size_t q_size = BIZ_Q_NODES_IN * BIZ_Q_NODES_L1
                      + BIZ_Q_NODES_L1 * BIZ_Q_NODES_L2
                      + BIZ_Q_NODES_L2 * BIZ_Q_NODES_OUT;

/* agent memory */
const size_t width_mem = BIZ_Q_NODES_IN + 1;
const size_t memory_depth = CORP_MEMORY_M;
const size_t replay_depth = CORP_MEMORY_B;

/* land cover categorization */
const unsigned nlcd_cats = NLCD_CATS_UB;
const size_t nlcdi_x = 0;
const size_t nlcdi_uo = 1;
const size_t nlcdi_ul = 2;
const size_t nlcdi_um = 3;
const size_t nlcdi_uh = 4;
const size_t nlcdi_fd = 5;
const size_t nlcdi_fc = 6;
const size_t nlcdi_fm = 7;

/* associated defines */
#define BIZ_COV_OTHER   COVCAT(0)
#define BIZ_COV_UO      COVCAT(1)
#define BIZ_COV_UL      COVCAT(2)
#define BIZ_COV_UM      COVCAT(3)
#define BIZ_COV_UH      COVCAT(4)
#define BIZ_COV_FD      COVCAT(5)
#define BIZ_COV_FC      COVCAT(6)
#define BIZ_COV_FM      COVCAT(7)

/* meta-state params */
const mood_t init = 0;
const mood_t working = 1;
const mood_t grow = 2;
const mood_t shrink = 3;

/* state factor defines */
#define BIZST_FIN   STTCAT(0)
#define BIZST_EMP   STTCAT(1)
#define BIZST_CAP   STTCAT(2)
#define BIZST_ECAP  STTCAT(3)
#define NBIZST_FIN  NSTCAT(0)
#define NBIZST_EMP  NSTCAT(1)
#define NBIZST_CAP  NSTCAT(2)
#define NBIZST_ECAP NSTCAT(3)

/* action indices */
const size_t action_deccap = 0;
const size_t action_keepcap = 1;
const size_t action_inccap = 2;
const size_t action_fire = 0;
const size_t action_stay = 1;
const size_t action_hire = 2;

/* associated defines */
#define BIZACT_DEC  ACTCAT(0)
#define BIZACT_MTN  ACTCAT(1)
#define BIZACT_INC  ACTCAT(2)
#define BIZACT_FIR  ACTCAT(3)
#define BIZACT_STY  ACTCAT(4)
#define BIZACT_HIR  ACTCAT(5)
#define RBIZACT_DEC RACTCAT(0)
#define RBIZACT_MTN RACTCAT(1)
#define RBIZACT_INC RACTCAT(2)
#define RBIZACT_FIR RACTCAT(3)
#define RBIZACT_STY RACTCAT(4)
#define RBIZACT_HIR RACTCAT(5)

};
#endif /* BUSINESS_CONFIG */

/** RESIDENCE CONFIG ————————————————————————————————————————————————————————*/
#ifndef RESIDENCE_CONFIG
namespace residence
{
/* global info */
const unsigned num_agents = RES_NUM;

/* actor networks (mu) */
const size_t width_mu_in = RES_MU_NODES_IN;
const size_t width_mu_l1 = RES_MU_NODES_L1;
const size_t width_mu_l2 = RES_MU_NODES_L2;
const size_t width_mu_out = RES_MU_NODES_OUT;
const size_t l1_size = width_mu_in * width_mu_l1;
const size_t l2_size = width_mu_l1 * width_mu_l2;
const size_t out_size = width_mu_l2 * width_mu_out;
const size_t l1_memsize = l1_size * sizeof(float);
const size_t l2_memsize = l2_size * sizeof(float);
const size_t out_memsize = out_size * sizeof(float);
const size_t mu_size = l1_size + l2_size + out_size;

/* critic networks (q) */
const size_t width_q_in = RES_Q_NODES_IN;
const size_t width_q_l1 = RES_Q_NODES_L1;
const size_t width_q_l2 = RES_Q_NODES_L2;
const size_t width_q_out = RES_Q_NODES_OUT;
const size_t q1_size = width_q_in * width_q_l1;
const size_t q2_size = width_q_l1 * width_q_l2;
const size_t qo_size = width_q_l2 * width_q_out;
const size_t q1_memsize = q1_size * sizeof(float);
const size_t q2_memsize = q2_size * sizeof(float);
const size_t qo_memsize = qo_size * sizeof(float);
const size_t q_size = q1_size + q2_size + qo_size;

/* agent memory */
const size_t width_mem = RES_Q_NODES_IN + 1;
const size_t memory_depth = RES_MEMORY_M;
const size_t replay_depth = RES_MEMORY_B;

/* land cover categorization */
const unsigned nlcd_cats = 2;

/* meta-state params */
const mood_t happy = 0;
const mood_t waiting = 1;
const mood_t extreme = 2;
const mood_t recovery = 3;

/* shifted action indices */
const size_t action_nojob = 0;
const size_t action_job = 1;
const size_t action_noapt = 0;
const size_t action_apt = 1;

/* associated defines */
#define RES_S_BUDGET    STTCAT(0)
#define RES_S_MOOD      STTCAT(1)
#define RES_SS_BUDGET   NSTCAT(0)
#define RES_SS_MOOD     NSTCAT(1)

#define RESACT_NOJOB    ACTCAT(0)
#define RESACT_JOB      ACTCAT(1)
#define RESACT_NOAPT    ACTCAT(2)
#define RESACT_APT      ACTCAT(3)
#define RRESACT_NOJOB   RACTCAT(0)
#define RRESACT_JOB     RACTCAT(1)
#define RRESACT_NOAPT   RACTCAT(2)
#define RRESACT_APT     RACTCAT(3)
};
#endif /* RESIDENCE_CONFIG */
