#pragma once
/// TODO IMPLEMENT THIS SO THAT PARAMS WORK ON COMMAND LINE

#define MODE_TRAINING 0
#define MODE_TESTING 1

enum operating_mode_t
{
training_mode,
checkpoint_mode,
testing_mode,
};

namespace runtime
{
operating_mode_t operating_mode = training_mode;
bool is_training = true;
bool is_checkpointing = false;
bool is_testing = false;
unsigned episode_length = 60u;
unsigned episode_count = 10u;
unsigned checkpoint_step = 60u;
};

using runtime::operating_mode;
using runtime::checkpoint_step;
