#pragma once
#ifndef FLAMEGPU_CONFIG_ENVIRONMENT_H_
#define FLAMEGPU_CONFIG_ENVIRONMENT_H_

#include "config.h"

extern flamegpu::ModelDescription model;

void
define_environment()
{
    using flameabm::bool_t;

    /*** @TODO: FIX FILE IMPORT FOR THIS */
    auto env = model.Environment();

    /// Input Parameters
    env.newProperty<unsigned>("slength_x", 237);
    env.newProperty<unsigned>("slength_y", 315);
    env.newProperty<unsigned>("sarea", 237 * 315);

    /// Operating Parameters
    env.newProperty<bool_t>("global_training_mode", true);
    env.newProperty<bool_t>("global_checkpoint_mode", true);
    env.newProperty<bool_t>("global_testing_mode", false);
    env.newProperty<unsigned>("global_from_checkpoint_at", 0);
    env.newProperty<unsigned>("operating_mode", MODE_TRAINING);

    /// Learning Parameters
    env.newProperty<float>("global_discount_rate", GLOBAL_GAMMA);
    env.newProperty<float>("global_transfer_rate", GLOBAL_TAU);
    env.newProperty<float>("global_exploration_rate", GLOBAL_EPSILON);
    env.newProperty<float>("global_learning_rate", GLOBAL_ALPHA);
    env.newProperty<float>("global_memory_accuracy", 1.0f);
    env.newProperty<unsigned>("global_episode_length", runtime::episode_length);
    env.newProperty<unsigned>("training_episode_reps", 2);
    env.newProperty<unsigned>("training_episode_length", runtime::episode_length);

    /// Model State
    env.newProperty<unsigned>("model_year", 0);
    env.newProperty<unsigned>("model_month", 0);

    /// Extra Log Fields
    env.newProperty<unsigned>("global_broken_links_farms", 0u);
    env.newProperty<unsigned>("global_broken_links_forests", 0u);
    env.newProperty<size_t>("residence_agents_count", residence::num_agents);
    env.newProperty<size_t>("residence_state_factors", residence::width_mu_in);
    env.newProperty<size_t>("residence_action_factors", residence::width_mu_out);
}

#endif
