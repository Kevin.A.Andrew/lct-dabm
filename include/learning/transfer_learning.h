#pragma once
#include "config.h"
#include "learning.h"
#include <future>

__host__
void
transfer_learning_business_mu(const float& transfer_rate)
{
    flamednn::toggle_device();
    float* l1_output_dev;
    float* diff_dev;
    float* l1_output = (float*)calloc(6*5, sizeof(float));
    flamednn::malloc(&l1_output_dev, business::l1_memsize);
    flamednn::malloc(&diff_dev, business::l1_memsize);
    flamednn::sync_device<float>(l1_output_dev, l1_output, business::l1_size);
    flamednn::compute_transfer_result<<<1,1>>>(weights::business::mu::l1_dev,
        l1_output_dev, transfer_rate, l1_output_dev);
    flamednn::sync_host<float>(l1_output_dev, l1_output, business::l1_size);
    flamednn::toggle_device();
}

__host__
void
do_transfer_learning(const float& transfer_rate)
{
    std::future<void> transfer_biz_mu = std::async(std::launch::async,
        transfer_learning_business_mu, transfer_rate);
    transfer_biz_mu.wait();
}

FLAMEGPU_HOST_FUNCTION(\
transfer_learning_)
{
    if ((FLAMEGPU->getStepCounter() % 
        FLAMEGPU->environment.getProperty<unsigned>("training_episode_length"))
        == 0)
        return;

    const float transfer_rate
        = FLAMEGPU->environment.getProperty<float>("global_transfer_rate");
    fprintf(stderr, "DO TRANSFER LEARNING\n");
    fprintf(stderr, "THETA' <== THETA' + TAU * (THETA - THETA')\n");
}
