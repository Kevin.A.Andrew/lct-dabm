#pragma once
#include "config.h"
#include "learning.h"
#include "lrn_policy_gradient.h"

FLAMEGPU_HOST_FUNCTION(\
residence_evaluate_action_)
{
    unsigned step = FLAMEGPU->getStepCounter();
    if (step == 0) return;

    int agent_num, agent_index;
    int width_mu_in, width_mu_out;
    int width_q_in, width_q_out;
    int memory_index, memory_index_max, memory_depth, memory_width;
    int sample_index, sample_depth;
    size_t *memory_sample;
    int action_index, action_offset, action_width, action_size;
    int threads_per_block, num_blocks;
    float *gradient, *gradient_normalized;

    agent_num = residence::num_agents;
    width_mu_in = residence::width_mu_in;
    width_mu_out = residence::width_mu_out;
    width_q_in = residence::width_q_in;
    width_q_out = residence::width_q_out;

    float *reward_vec;
    cudaMallocManaged(&reward_vec, agent_num * sizeof(float));
    flamegpu::HostAgentAPI agents_api = FLAMEGPU->agent("urban_residence");
    flamegpu::DeviceAgentVector agents_vec = agents_api.getPopulationData();

    agent_index = 0;
//    fprintf(stdout, "reward vec:\t");
    for (auto agent : agents_vec)
    {
        reward_vec[agent_index] = agent.getVariable<float>("reward");
//        fprintf(stdout, "%f ", reward_vec[agent_index]);
        ++agent_index;
    }
//    fprintf(stdout, "\n");
    cudaDeviceSynchronize();

//    flamelog::debug("Res-Eval-Action");
    flamednn::toggle_device();
    for (agent_index = 0; agent_index < agent_num; ++agent_index)
    {
        flamednn::memcpy_device<float>(
            tensor_data::residence::q::in_dev_addr(agent_index),
            tensor_data::residence::mu::in_dev_addr(agent_index),
            width_mu_in);
        flamednn::memcpy_device<float>(
            tensor_data::residence::q::in_dev_addr(agent_index, width_mu_in),
            tensor_data::residence::mu::out_dev_addr(agent_index),
            width_mu_out);
    }
    flamednn_sync_data(residence, q, in, agent_num, width_q_in);

    flamednn_convolve(residence, q, in, l1, l1);
    flamednn_activate(residence, q, l1);
    flamednn_convolve(residence, q, l1, l2, l2);
    flamednn_activate(residence, q, l2);
    flamednn_convolve(residence, q, l2, out, out);
    flamednn_sync_data(residence, q, out, agent_num, width_q_out);

    memory_depth = residence::memory_depth;
    memory_width = residence::width_mem;

    memory_index = step % memory_depth;
    memory_index_max = (step > memory_depth) ? memory_depth : step;
    for (agent_index = 0; agent_index < agent_num; ++agent_index)
    {
        flamednn::memcpy_device<float>(
            tensor_data::residence::memory::bank_dev_addr(agent_index,
                                                          memory_index),
            tensor_data::residence::q::in_dev_addr(agent_index),
            width_q_in);
        flamednn::memcpy_device<float>(
            tensor_data::residence::memory::bank_dev_addr(agent_index,
                memory_index) + width_q_in,
             reward_vec+agent_index, 1);
//            tensor_data::residence::q::out_dev_addr(agent_index),
//            width_q_out);
    }

    sample_depth = residence::replay_depth;
    memory_sample = (size_t*)malloc(sample_depth * sizeof(size_t));

    flamednn::toggle_device();
    if (step <= sample_depth)
        return;
    for (sample_index = 0; sample_index < sample_depth; ++sample_index)
    {
        size_t random_index;
        random_index  = flamerand::uniform<size_t>(0, memory_index_max, U);
        memory_sample[sample_index] = random_index;
        for (int other_index = 0; other_index < sample_index; ++other_index)
        {
            if (random_index == memory_sample[other_index])
            {
                --sample_index;
                break;
            }
        }
    }
    flamednn::toggle_device();

    for (agent_index = 0; agent_index < agent_num; ++agent_index)
    {
        for (sample_index = 0; sample_index < sample_depth; ++sample_index)
        {
            flamednn::memcpy_device<float>(
                tensor_data::residence::memory::buffer_dev_addr(agent_index,
                        sample_index),
                tensor_data::residence::memory::bank_dev_addr(agent_index,
                        memory_sample[sample_index]),
                memory_width);
        }
    }
    flamednn_sync_data(residence, memory, buffer, agent_num, memory_width);
/*
    fprintf(stdout, "SING MEM (%zu):\t", memory_sample[0]);
    for (int i = 0; i < memory_width; ++i)
    {
        fprintf(stdout, "%f ", *(tensor_data::residence::memory::buffer + i));
    }
    fprintf(stdout, "\n");
*/

    action_offset = residence::width_mu_in;
    action_width = residence::width_mu_out;
    action_size = action_width * sizeof(float);

    cudaMallocManaged(&gradient, agent_num * action_size);
    cudaMallocManaged(&gradient_normalized, action_size);
    for (action_index = 0; action_index < action_width; ++action_index)
    {
        for (agent_index = 0; agent_index < agent_num; ++agent_index)
        {
            gradient[agent_index * action_width + action_index] = 0.0f;
        }
        gradient_normalized[action_index] = 0.0f;
    }
    cudaDeviceSynchronize();

    threads_per_block = 256;
    num_blocks = (action_width + threads_per_block - 1) / threads_per_block;

    policy_gradient_kernel<<<num_blocks, threads_per_block>>>(
        tensor_data::residence::q::in_dev,
        tensor_data::residence::memory::buffer_dev, gradient,
        gradient_normalized, agent_num, sample_depth, memory_width,
        action_offset, action_width, 0.05f);
    cudaDeviceSynchronize();

//    flamednn::print_first_3n("gradient_normalized", gradient_normalized);

    cudaFree(gradient);
    cudaFree(gradient_normalized);

    flamednn::toggle_device();
}
