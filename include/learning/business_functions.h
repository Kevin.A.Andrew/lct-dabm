#pragma once
#include "config.h"
#include "learning.h"
#include "lrn_policy_gradient.h"

FLAMEGPU_HOST_FUNCTION(\
business_evaluate_action_)
{
    int agent_num;
    int net_width_q_in, net_width_q_out;
    agent_num = business::num_agents;
    net_width_q_in = business::width_q_in;
    net_width_q_out = business::width_q_out;

    const unsigned step = FLAMEGPU->getStepCounter();

    /// # Backward pass for mu
//    fprintf(stderr, "—— correct mu ——\n");
    flamednn::toggle_device();

    /* Load input for Q */
//    flamelog::debug("trial Q");
    for (size_t biz_id = 0; biz_id < business::num_agents; ++biz_id)
    {
        flamednn::memcpy_device<float>(
            tensor_data::business::q::in_dev_addr(biz_id),
            tensor_data::business::mu::in_dev_addr(biz_id),
            business::width_mu_in);
        flamednn::memcpy_device<float>(
            tensor_data::business::q::in_dev_addr(biz_id,
                business::width_mu_in),
            tensor_data::business::mu::out_dev_addr(biz_id),
            business::width_mu_out);
    }
    flamednn_sync_data(business, q, in, agent_num, net_width_q_in);
//    flamednn::print_first_3n("q::in", tensor_data::business::q::in);

    /* Do Forward Pass */
    flamednn_convolve(business, q, in, l1, l1);
    flamednn_activate(business, q, l1);
    flamednn_convolve(business, q, l1, l2, l2);
    flamednn_activate(business, q, l2);
    flamednn_convolve(business, q, l2, out, out);
    flamednn_sync_data(business, q, out, business::num_agents, net_width_q_out);
//    flamednn::print_first_3n("q::out", tensor_data::business::q::out);

    /* Store output from Q */

    /* Push Memory Bank */
    const size_t memory_index = step % business::memory_depth;
    for (size_t biz_id = 0; biz_id < business::num_agents; ++biz_id)
    {
        flamednn::memcpy_device<float>(
            tensor_data::business::memory::bank_dev_addr(biz_id, memory_index),
            tensor_data::business::q::in_dev_addr(biz_id),
            business::width_q_in);
    }
//    fprintf(stderr, "——————————————————————\n");
    flamednn::toggle_device();

    /// ## Create Memory Buffer
    if (step <= business::replay_depth)
        return;
    size_t *memory_sample, step_sz, memory_size;
    size_t sample_idx, sample_jdx, buffer_idx;
    memory_sample = (size_t*)malloc(business::replay_depth * sizeof(size_t));
    step_sz = static_cast<size_t>(step);
    memory_size = std::min(step_sz, business::memory_depth);
    for (sample_idx = 0; sample_idx < business::replay_depth; ++sample_idx)
    {
        buffer_idx = flamerand::uniform<size_t>(0, memory_size, U);
        memory_sample[sample_idx] = buffer_idx;
        for (sample_jdx = 0; sample_jdx < sample_idx; ++sample_jdx)
        {
            if (buffer_idx == memory_sample[sample_jdx])
            {
                --sample_idx;
                break;
            }
        }
    }

    flamednn::toggle_device();
    int agent_index;
    for (agent_index = 0; agent_index < agent_num; ++agent_index)
    {
        for (sample_idx = 0; sample_idx < business::replay_depth; ++sample_idx)
        {
            flamednn::memcpy_device<float>(
                tensor_data::business::memory::buffer_dev_addr(agent_index,
                    sample_idx),
                tensor_data::business::memory::bank_dev_addr(agent_index,
                    memory_sample[sample_idx]), business::width_q_in);
        }
    }

    // -- Policy Gradient --
    size_t action_size;

    int action_offset, action_width, action_index;
    int memory_sample_depth, memory_sample_width;
    int threads_per_block, num_blocks;

    float *gradient, *gradient_normalized;
    float eta;

    action_offset = CORP_STATE_WIDTH;
    action_width = CORP_ACTION_WIDTH;
    action_size = action_width * sizeof(float);

    memory_sample_depth = business::replay_depth;
    memory_sample_width = business::width_mem;

    threads_per_block = 32;
    num_blocks = (action_width + threads_per_block - 1) / threads_per_block;

    eta = 0.05f;
    
    cudaMallocManaged(&gradient, agent_num * action_size);
    cudaMallocManaged(&gradient_normalized, action_size);

    for (action_index = 0; action_index < action_width; ++action_index)
    {
        for (agent_index = 0; agent_index < agent_num; ++agent_index)
        {
            gradient[agent_index * action_width + action_index] = 0.0f;
        }
        gradient_normalized[action_index] = 0.0f;
    }
    cudaDeviceSynchronize();

    policy_gradient_kernel<<<num_blocks, threads_per_block>>>(
        tensor_data::business::q::in_dev,
        tensor_data::business::memory::buffer_dev, gradient,
        gradient_normalized, agent_num, memory_sample_depth,
        memory_sample_width, action_offset, action_width, eta);
    cudaDeviceSynchronize();
//    flamednn::print_first_3n("gradient_normalized", gradient_normalized);

    cudaFree(gradient);
    cudaFree(gradient_normalized);

    flamednn::toggle_device();
//    fprintf(stderr, "—————————————————————\n");
}

