#pragma once
#include "config.h"
#include "learning.h"

FLAMEGPU_HOST_CONDITION(\
is_forestry_step_host)
{
    return (FLAMEGPU->getStepCounter() % YEARLY == 0) ? flamegpu::CONTINUE : flamegpu::EXIT;
}

#define flameext_ret_stepcond(CONDITION_FUNC) \
    { if (CONDITION_FUNC(FLAMEGPU->getStepCounter())) return; }
    

#define stateInS(X) "state" #X
#define cpyForStateInO(idx, jdx, scode) \
    tensor_data::forester::mu::in[(idx * forester::width_mu_in) + jdx] \
        = forester.getVariable<float>(#scode)
#define cpyForStateIn(idx, jdx) \
    tensor_data::forester::mu::in[(idx * forester::width_mu_in) + jdx] \
        = forester.getVariable<float>("state" #jdx)

FLAMEGPU_HOST_FUNCTION(\
forester_decide_action_)
{
    flameext_ret_stepcond(([](unsigned u)->bool{return (u%12!=0)||(u==0);}));
    /*
    macro_for_agents(forester, forester, for_id)
    {
        cpyForStateIn(for_id, 0);
        cpyForStateIn(for_id, 1);
        cpyForStateIn(for_id, 2);
        cpyForStateIn(for_id, 3);
        cpyForStateIn(for_id, 4);
        cpyForStateIn(for_id, 5);
        cpyForStateIn(for_id, 6);
        cpyForStateIn(for_id, 7);
        cpyForStateIn(for_id, 8);
        cpyForStateIn(for_id, 9);
        ++for_id;
    }
    macro_endfor_agents;
    fprintf(stderr, "--foral--\n");
    flamednn::print_first_3n("mu::in", tensor_data::forester::mu::in);
    flamednn::toggle_device();
    flamednn_sync_dev_data(forester, mu, in, forester::num_agents,
                           forester::width_mu_in);
    flamednn_convolve(forester, mu, in, l1, l1);
    flamednn_activate(forester, mu, l1);
    flamednn_convolve(forester, mu, l1, l2, l2);
    flamednn_activate(forester, mu, l2);
    flamednn_convolve(forester, mu, l2, out, out);
    flamednn_activate(forester, mu, out);
    flamednn_sync_data(forester, mu, out, forester::num_agents,
                       forester::width_mu_out);
    flamednn::toggle_device();
    flamednn::print_first_3n("mu::out", tensor_data::forester::mu::out);
    fprintf(stderr, "---------\n");

    macro_for_agents(forester, forester, for_id)
    {
        using namespace forester;
        forester.setVariable<float>(storage_action_noamp, flamerand::uniform<float>(U));
        forester.setVariable<float>(ACTION_AMP, flamerand::uniform<float>(U));
        forester.setVariable<float>(ACTION_FOREST_EXP, flamerand::uniform<float>(U));
        forester.setVariable<float>(ACTION_FOREST_MTN, flamerand::uniform<float>(U));
        forester.setVariable<float>(ACTION_FOREST_SHR, flamerand::uniform<float>(U));
        ++for_id;
    }
    macro_endfor_agents;
    */
}
