#pragma once
#include "config.h"
#include "functions/market_functions.h"

/// TODO FINISH RESTRUC AND MOVE TO SEPARATE FILES W/OTHERS

FLAMEGPU_AGENT_FUNCTION_CONDITION(\
farms_step)
{
    return FLAMEGPU->getStepCounter() % YEARLY;
}

/// Take land-use action on parcels
FLAMEGPU_AGENT_FUNCTION(\
farms_use_land_, flamegpu::MessageNone, flamegpu::MessageBucket)
{
    const unsigned action_usage = onehot_split2(0, 1);
    const unsigned action_corn = onehot_split3(2, 3, 4);
    const unsigned action_dairy = onehot_split3(5, 6, 7);
    const unsigned action_beef = onehot_split3(8, 9, 10);
    const unsigned action_hay = onehot_split3(11, 12, 13);
    const unsigned action_other = onehot_split3(14, 15, 16);

    FLAMEGPU->message_out.setKey(FLAMEGPU->getVariable<unsigned>("parcel_id"));

    if (action_usage == farmer::action_bmp)
    {
        FLAMEGPU->setVariable<float>(FARM_AA_BMP, 1.0f);
        FLAMEGPU->setVariable<float>(FARM_AA_NOBMP, 0.0f);
        FLAMEGPU->setVariable<flameabm::bool_t>("using_bmp", true);
    }
    else
    {
        FLAMEGPU->setVariable<float>(FARM_AA_BMP, 0.0f);
        FLAMEGPU->setVariable<float>(FARM_AA_NOBMP, 1.0f);
        FLAMEGPU->setVariable<flameabm::bool_t>("using_bmp", false);
    }

    switch_split(corn, 2, 3, 4);
    switch_split(dairy, 5, 6, 7);
    switch_split(beef, 8, 9, 10);
    switch_split(hay, 11, 12, 13);
    switch_split(other, 14, 15, 16);

    return flamegpu::ALIVE;
}


/// Update local land-cover information
FLAMEGPU_AGENT_FUNCTION(\
farm_land_update, flamegpu::MessageBucket, flamegpu::MessageNone)
{
    const unsigned key = FLAMEGPU->getVariable<unsigned>("parcel_id");
    flameext_get_array(land_cover, land_cover, unsigned, NLCD_CATS_AG);

    for (size_t category = 0; category < NLCD_CATS_AG; ++category)
    {
        land_cover[category] = 0;
    }
    for (const auto& message : FLAMEGPU->message_in(key))
    {
        const unsigned category = message.getVariable<unsigned>("cover");
        land_cover[category]++;
    }

    flameext_set_array(land_cover, land_cover, unsigned, NLCD_CATS_AG);
    return flamegpu::ALIVE;
}


FLAMEGPU_AGENT_FUNCTION(\
farm_cell_stat_, flamegpu::MessageBucket, flamegpu::MessageNone)
{
    const unsigned parcel_id = FLAMEGPU->getVariable<unsigned>("parcel_id");
    unsigned cells_total = 0u;
    unsigned cells_other = 0u;
    unsigned cells_crop = 0u;
    unsigned cells_pasture = 0u;
    unsigned cells_grass = 0u;
    unsigned cells_forest = 0u;
    for (const auto& message : FLAMEGPU->message_in(parcel_id))
    {
        using namespace nlcd;
        const nlcd_t cover = message.getVariable<unsigned>("cover");
        ++cells_total;
        switch (cover)
        {
            case forest_dec:
            case forest_con:
            case forest_mix:
                ++cells_forest;
                break;
            case grassland:
                ++cells_grass;
                break;
            case pasture:
                ++cells_pasture;
                break;
            case crops:
                ++cells_crop;
                break;
            default:
                ++cells_other;
        }
    }
    FLAMEGPU->setVariable<unsigned>(FARM_COV_TOTAL, cells_total);
    FLAMEGPU->setVariable<unsigned>(FARM_COV_OTHER, cells_other);
    FLAMEGPU->setVariable<unsigned>(FARM_COV_CROP, cells_crop);
    FLAMEGPU->setVariable<unsigned>(FARM_COV_PASTURE, cells_pasture);
    FLAMEGPU->setVariable<unsigned>(FARM_COV_GRASS, cells_grass);
    FLAMEGPU->setVariable<unsigned>(FARM_COV_FOREST, cells_forest);
}


/// Send an update message out to all neighbors
FLAMEGPU_AGENT_FUNCTION(\
farms_message_neighbors_, flamegpu::MessageNone, flamegpu::MessageBucket)
{
    const unsigned key = FLAMEGPU->getVariable<unsigned>("parcel_id");
    const unsigned using_bmp = FLAMEGPU->getVariable<unsigned>("using_bmp");
    const float loss = FLAMEGPU->getVariable<unsigned>("loss");

    FLAMEGPU->message_out.setKey(key);
    FLAMEGPU->message_out.setVariable<unsigned>("used_bmp", using_bmp);
    FLAMEGPU->message_out.setVariable<float>("had_loss", loss);

    return flamegpu::ALIVE;
}


/// Update information about neighbors
FLAMEGPU_AGENT_FUNCTION(\
farms_update_neighbors_, flamegpu::MessageBucket, flamegpu::MessageNone)
{
    recinfo_farm_neighbor(0);
    recinfo_farm_neighbor(1);
    recinfo_farm_neighbor(2);
    recinfo_farm_neighbor(3);
    recinfo_farm_neighbor(4);
    return flamegpu::ALIVE;
}

FLAMEGPU_AGENT_FUNCTION(\
farms_step_, flamegpu::MessageNone, flamegpu::MessageNone)
{
    unsigned year = FLAMEGPU->environment.getProperty<unsigned>("model_year");
    float production_factor_corn, profit_corn;
    float production_factor_hay, profit_hay;
    float production_factor_dairy, profit_dairy;
    float production_factor_beef, profit_beef;
    float profit_total;
    unsigned cover_crops, cover_pasture;
    bool using_bmp = FLAMEGPU->getVariable<float>(FARM_AA_BMP) > 0.0f;
    production_factor_corn = FLAMEGPU->getVariable<float>("corn_production_factor");
    production_factor_hay = FLAMEGPU->getVariable<float>("hay_production_factor");
    production_factor_beef = FLAMEGPU->getVariable<float>("beef_production_factor");
    production_factor_dairy = FLAMEGPU->getVariable<float>("dairy_production_factor");

    using namespace farmer;
    using flamemath::min, flamemath::max;

    if (FLAMEGPU->getVariable<float>(FARM_AA_EXP_CORN) > 0.0f)
    {
        production_factor_corn = min<float>(corn_prod_max,
            production_factor_corn * 1.1f);
    }
    else if (FLAMEGPU->getVariable<float>(FARM_AA_SHR_CORN) > 0.0f)
    {
        production_factor_corn = max<float>(corn_prod_min,
            production_factor_corn * 0.9f);
    }
    if (FLAMEGPU->getVariable<float>(FARM_AA_EXP_HAY) > 0.0f)
    {
        production_factor_hay = min<float>(hay_prod_max,
            production_factor_hay * 1.1f);
    }
    else if (FLAMEGPU->getVariable<float>(FARM_AA_SHR_HAY) > 0.0f)
    {
        production_factor_hay = max<float>(hay_prod_min,
            production_factor_hay * 0.9f);
    }
    if (FLAMEGPU->getVariable<float>(FARM_AA_EXP_BEEF) > 0.0f)
    {
        production_factor_beef = min<float>(prod_beef_max,
            production_factor_beef * 1.1f);
    }
    else if (FLAMEGPU->getVariable<float>(FARM_AA_SHR_BEEF) > 0.0f)
    {
        production_factor_beef = max<float>(prod_beef_min,
            production_factor_beef * 0.9f);
    }
    if (FLAMEGPU->getVariable<float>(FARM_AA_EXP_DAIRY) > 0.0f)
    {
        production_factor_dairy = min<float>(prod_dairy_max,
            production_factor_dairy * 1.1f);
    }
    else if (FLAMEGPU->getVariable<float>(FARM_AA_SHR_DAIRY) > 0.0f)
    {
        production_factor_dairy = max<float>(prod_dairy_min,
            production_factor_dairy * 0.9f);
    }

    cover_crops = FLAMEGPU->getVariable<unsigned>(FARM_COV_CROP);
    cover_pasture = FLAMEGPU->getVariable<unsigned>(FARM_COV_PASTURE);

    /// TODO: Fix enum
    profit_corn = farmer_get_profit(0, production_factor_corn, using_bmp,
                    cover_crops, year);
    profit_hay = farmer_get_profit(1, production_factor_hay, using_bmp,
                    cover_pasture, year);
    profit_beef = farmer_get_profit(2, production_factor_beef, using_bmp,
                    cover_pasture, year);
    profit_dairy = farmer_get_profit(3, production_factor_dairy, using_bmp,
                    cover_pasture, year);

    profit_total = profit_corn + profit_hay + profit_beef + profit_dairy;

    FLAMEGPU->setVariable<float>("profit_corn", profit_corn);
    FLAMEGPU->setVariable<float>("profit_hay", profit_hay);
    FLAMEGPU->setVariable<float>("profit_beef", profit_beef);
    FLAMEGPU->setVariable<float>("profit_dairy", profit_dairy);
    FLAMEGPU->setVariable<float>("profit_total", profit_total);

    float s_prodfact_corn, s_prodfact_hay, s_prodfact_beef, s_prodfact_dairy;
    s_prodfact_corn = ((2.0f * (production_factor_corn - farmer::corn_prod_min))
                      / (farmer::corn_prod_max - farmer::corn_prod_min)) - 1.0f;
    s_prodfact_hay = ((2.0 * (production_factor_hay - farmer::hay_prod_min))
                      / (farmer::hay_prod_max - farmer::hay_prod_min)) - 1.0f;
    s_prodfact_beef = ((2.0f * (production_factor_beef - farmer::prod_beef_min))
                       / (farmer::prod_beef_max - farmer::prod_beef_min)) - 1.0f;
    s_prodfact_dairy = ((2.0f * (production_factor_dairy - farmer::prod_dairy_min))
                       / (farmer::prod_dairy_max - farmer::prod_dairy_min)) - 1.0f;

    float s_neighbor_bmp, s_neighbor_loss;
    s_neighbor_bmp =  static_cast<float>(
        FLAMEGPU->getVariable<flameabm::bool_t>("neighbor_bmp0")
        + FLAMEGPU->getVariable<flameabm::bool_t>("neighbor_bmp1")
        + FLAMEGPU->getVariable<flameabm::bool_t>("neighbor_bmp2")
        + FLAMEGPU->getVariable<flameabm::bool_t>("neighbor_bmp3")
        + FLAMEGPU->getVariable<flameabm::bool_t>("neighbor_bmp4")) / 2.5f
        - 1.0f;
    s_neighbor_loss = 0.0f;

    FLAMEGPU->setVariable<float>(FARM_SS_CORN_PROD, s_prodfact_corn);
    FLAMEGPU->setVariable<float>(FARM_SS_PROD_HAY, s_prodfact_hay);
    FLAMEGPU->setVariable<float>(FARM_SS_PROD_BEEF, s_prodfact_beef);
    FLAMEGPU->setVariable<float>(FARM_SS_PROD_DAIRY, s_prodfact_dairy);
    FLAMEGPU->setVariable<float>(FARM_SS_NEIGHBOR_BMP, s_neighbor_bmp);
    FLAMEGPU->setVariable<float>(FARM_SS_NEIGHBOR_LOSS, s_neighbor_loss);
}

