#pragma once
#include "config.h"
#include "functions.h"
#include "learning_functions.h"

#include "fc_agents.h"
#include "fc_messages.h"
#include "fc_environment.h"
#include "fc_execution_order.h"
#include "fc_support.h"

flamegpu::ModelDescription model("lct-dabm");

__host__ void
flamegpu_define_everything()
{
    define_environment();       // @f fc_environment.h
    define_all_agents();        // @f fc_agents.h
    define_all_messages();      // @f fc_messages.h
    define_execution_order();   // @f fc_execution_order.h
}

