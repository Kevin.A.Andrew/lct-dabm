#pragma once
#ifndef SHARED_H_
#define SHARED_H_
#include "config.h"

#include <algorithm>
#include <array>
#include <iostream>
#include <string>
#include <future>
#include <random>
#include <sysexits.h>
#include <vector>

namespace lct_abm
{
// from <flamegpu/flamegpu.h>
using flamegpu::CUDASimulation;
using flamegpu::HostAgentAPI;
using flamegpu::DeviceAgentVector;
// from <algorithm>
using std::copy_n;
using std::fill;
using std::iota;
using std::shuffle;
using std::min;
// from <array>
using std::array;
// from <iostream>
using std::ostream;
using std::endl;
// from <random>
using std::mt19937;
using std::random_device;
// from <string>
using std::string;
using std::to_string;
// from <future>
using std::future;
using std::async;
using std::launch;
// from <vector>
using std::vector;
// basic shared functions
template <typename T>
    inline typename vector<T>::iterator
    start_of(vector<T> &v)
{
    return v.begin();
}
template <typename T, size_t sz>
    inline T*
    start_of(T (&array)[sz])
{
    return array;
}
template <typename T>
    inline T*
    start_of(T *ptr)
{
    return ptr;
}

template <typename T>
    inline typename vector<T>::iterator
    end_of(vector<T> &v)
{
    return v.end();
}
template <typename T, size_t sz>
    inline T*
    end_of(T (&array)[sz])
{
    return array + sz;
}
template <typename T>
    inline T*
    end_of(T *ptr, size_t sz)
{
    return ptr + sz;
}

void *
xmalloc(size_t size)
{
    void *ptr = malloc(size);
    if (ptr == nullptr)
    {
        fprintf(stderr, "malloc: virtual memory exhausted\n");
        exit(EX_OSERR);
    }
    return ptr;
}
void *
xnmalloc(size_t n, size_t size)
{
    return xmalloc(n * size);
}
};

#endif // SHARED_H_
