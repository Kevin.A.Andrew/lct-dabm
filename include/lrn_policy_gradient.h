#pragma once
#include "config.h"
#include "learning.h"

__global__ void
internal_policy_gradient_kernel(float*, float*, float*, int, int, int, int);

__global__ void
policy_gradient_kernel(float*, float*, float*, float*, int, int, int, int, int,
                       float);


__global__ void
policy_gradient_kernel(float* base, float* memory_sample, float* result,
    float* result2,
    int agent_count,
    int memory_sample_depth, int memory_width,
    int action_offset, int action_width, float eta)
{
    int agent_index;
    agent_index = blockIdx.x * blockDim.x + threadIdx.x;
    if (agent_index > agent_count) return;

    float *base_start, *agent_memory_start, *result_start;

    base_start = base;
    agent_memory_start = memory_sample
        + (agent_index * (memory_sample_depth * memory_width));
    result_start = result + (agent_index * action_width);

    internal_policy_gradient_kernel<<<1, 8>>>(base_start, agent_memory_start,
        result_start,
        memory_sample_depth, memory_width,
        action_offset, action_width);

    __syncthreads();

    int action_factor, result_index;
    for (action_factor = 0; action_factor < action_width; ++action_factor)
    {
        result_index = agent_index * action_width + action_factor;
        result2[result_index] = result[result_index] * eta / agent_count;
    }
}


__global__ void
internal_policy_gradient_kernel(float* base, float* memory_sample, float* result,
    int memory_sample_depth, int memory_width, int action_offset,
    int action_width)
{
    int action_factor;
    action_factor = blockIdx.x * blockDim.x + threadIdx.x;
    if (action_factor > action_width) return;

    int sample_index_base, sample_index;
    int sample_action_factor_index, sample_reward_index;
    float base_value, sample_value, sample_reward_value;
    float sum_pos, sum_neg, sum_eq;
    float num_pos, num_neg, num_eq;
    sum_pos = 0.0f;
    sum_neg = 0.0f;
    sum_eq = 0.0f;
    num_pos = 0.0f;
    num_neg = 0.0f;
    num_eq = 0.0f;

    base_value = base[action_factor];
    for (sample_index = 0; sample_index < memory_sample_depth; ++sample_index)
    {
        sample_index_base = memory_width * sample_index + action_offset;
        sample_action_factor_index = sample_index_base + action_factor;
        sample_reward_index = sample_index_base + action_width;

        sample_value = memory_sample[sample_action_factor_index];
        sample_reward_value = memory_sample[sample_reward_index];

        if (base_value > sample_value)
        {
            sum_pos += sample_reward_value;
            num_pos += 1.0f;
        }
        else if (base_value < sample_value)
        {
            sum_neg += sample_reward_value;
            num_neg += 1.0f;
        }
        else
        {
            sum_eq += sample_reward_value;
            num_eq += 1.0f;
        }
    }
    if (num_pos > 0.0f) sum_pos /= num_pos;
    if (num_neg > 0.0f) sum_neg /= num_neg;
    if (num_eq > 0.0f) sum_eq /= num_eq;
    if (num_pos > num_eq or num_neg > num_eq)
        result[action_factor] = (num_pos - num_neg);
    else
        result[action_factor] = 0.0f;
}
