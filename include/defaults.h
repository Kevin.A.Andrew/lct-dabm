#pragma once


/** GLOBAL PARAMS ———————————————————————————————————————————————————————————*/
#define GLOBAL_GAMMA 0.99f
#define GLOBAL_TAU 0.001f
#define GLOBAL_EPSILON 0.1f
#define GLOBAL_ALPHA 0.00025f

// TODO: Remove this Hard Coding
#define MIN_PID 81124
#define MAX_PID 93351

#define TIMESTEP_SCALE "M"
#define MONTH 1
#define MONTYLY 1
#define YEAR 12
#define YEARLY 12

#ifdef OTHER_PARAMS
OTHER_PARAMS
#endif /* OTHER_PARAMS */


/** FARMER PARAMS ———————————————————————————————————————————————————————————*/
#ifndef FARMER_CONFIG
#define FARM_NUM 487
#define FARM_MAX_NEIGHBORS 5
#define NLCD_CATS_AG 7

#define FARM_STATE_WIDTH 15
#define FARM_ACTION_WIDTH 17

#define FARM_MU_NODES_IN FARM_STATE_WIDTH
#define FARM_MU_NODES_L1 10
#define FARM_MU_NODES_L2 10
#define FARM_MU_NODES_L3 10
#define FARM_MU_NODES_L4 10
#define FARM_MU_NODES_OUT FARM_ACTION_WIDTH

#define FARM_Q_NODES_IN 32
#define FARM_Q_NODES_L1 16
#define FARM_Q_NODES_L2 16
#define FARM_Q_NODES_L3 16
#define FARM_Q_NODES_OUT 1

#define FARM_MEM_LOCAL 5
#define FARM_MEM_LOCAL_MIN 5
#define FARM_MEM_LOCAL_MAX 5
#define FARM_MEMORY_M 100
#define FARM_MEMORY_B 5

#define FARMER_CORN_MIN 3.362551f
#define FARMER_CORN_MAX 4.259232f
#define FARMER_CORN_MEAN 3.8108915f
#define FARMER_HAY_MIN 0.358672f
#define FARMER_HAY_MAX 0.470757f
#define FARMER_HAY_MEAN 0.4147145f
#define FARMER_MEAT_MIN 900.0f
#define FARMER_MEAT_MAX 1200.0f
#define FARMER_MEAT_MEAN 1050.0f
#define FARMER_MILK_MIN 210.0f
#define FARMER_MILK_MAX 240.0f
#define FARMER_MILK_MEAN 225.0f
#define FARMER_CORN_PHOS_MIN 2.02e-4f
#define FARMER_CORN_PHOS_MAX 6.17e-4f
#define FARMER_CORN_PHOS_MEAN 4.095e-4f
#define FARMER_COW_PHOS_MIN 3.366e-4f
#define FARMER_COW_PHOS_MAX 7.853e-4f
#define FARMER_COW_PHOS_MEAN 5.6095e-4f
#define FARMER_HAY_PHOS_MIN 3.37e-5f
#define FARMER_HAY_PHOS_MAX 1.123e-4f
#define FARMER_HAY_PHOS_MEAN 7.305e-5f
#define FARMER_BOVINE_MIN 3600.0f
#define FARMER_BOVINE_MAX 5400.0f
#define FARMER_BOVINE_MEAN 4800.0f

#endif /* FARMER_CONFIG */


/** FOREST PARAMS ———————————————————————————————————————————————————————————*/
#ifndef FOREST_CONFIG
#define FOR_NUM 111
#define FOREST_MAX_NEIGHBORS 5
#define NLCD_CATS_FR 5

#define FOREST_PRODSCALE_DOWN 1
#define FOREST_PRODSCALE_UP   1

#define FOREST_STATE_WIDTH 10
#define FOREST_ACTION_WIDTH 5

#define FOREST_MU_NODES_IN FOREST_STATE_WIDTH
#define FOREST_MU_NODES_L1 7
#define FOREST_MU_NODES_L2 7
#define FOREST_MU_NODES_L3 7
#define FOREST_MU_NODES_L4 7
#define FOREST_MU_NODES_OUT FOREST_ACTION_WIDTH

#define FOREST_Q_NODES_IN 15
#define FOREST_Q_NODES_L1 7
#define FOREST_Q_NODES_L2 7
#define FOREST_Q_NODES_L3 7
#define FOREST_Q_NODES_OUT 1

#define FOREST_MEM_LOCAL 5
#define FOREST_MEM_LOCAL_MIN 5
#define FOREST_MEM_LOCAL_MAX 5

#define FOREST_MEMORY_M 100
#define FOREST_MEMORY_B 5

#define FOREST_PROD_MIN 0.9f
#define FOREST_PROD_MAX 1.1f
#define FOREST_PROD_MEAN 1.0f
#endif /* FOREST_CONFIG */


/** BUSINESS PARAMS —————————————————————————————————————————————————————————*/
#ifndef BUSINESS_CONFIG
#define BIZ_NUM 31
#define MAX_EMPLOYEES 40
#define INITIAL_ECAP 10
#define INIT_ECAP 3
#define NLCD_CATS_UB 8

#define CORP_STATE_WIDTH 4
#define CORP_ACTION_WIDTH 6

#define BIZ_MU_NODES_IN CORP_STATE_WIDTH
#define BIZ_MU_NODES_L1 5
#define BIZ_MU_NODES_L2 5
#define BIZ_MU_NODES_OUT CORP_ACTION_WIDTH

#define BIZ_Q_NODES_IN 10
#define BIZ_Q_NODES_L1 5
#define BIZ_Q_NODES_L2 5
#define BIZ_Q_NODES_OUT 1

#define CORP_MEMORY_M 100
#define CORP_MEMORY_B 8
#endif /* BUSINESS_CONFIG */


/** RESIDENCE PARAMS ————————————————————————————————————————————————————————*/
#ifndef RESIDENCE_CONFIG
#define RES_NUM 294

#define RES_STATE_WIDTH 5
#define RES_ACTION_WIDTH 4

#define RES_MU_NODES_IN RES_STATE_WIDTH
#define RES_MU_NODES_L1 4
#define RES_MU_NODES_L2 4
#define RES_MU_NODES_OUT RES_ACTION_WIDTH

#define RES_Q_NODES_IN 9
#define RES_Q_NODES_L1 5
#define RES_Q_NODES_L2 5
#define RES_Q_NODES_OUT 1

#define RES_MEMORY_M 100
#define RES_MEMORY_B 8
#endif

/** OTHER DEFAULTS ——————————————————————————————————————————————————————————*/

#include "nlcd.h"
#include "run_config.h"
#include "agent_config.h"

