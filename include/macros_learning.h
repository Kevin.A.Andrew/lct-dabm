#pragma once

// TODO Check that these all make sense / can't be externalized

/* Macros for syncing tensor data */
#define flamednn_sync_dev_data(AGT, NET, L, S1, S2) \
    flamednn::sync_device<float>(tensor_data::AGT::NET::L ## _dev, \
        tensor_data::AGT::NET::L, S1 * S2)

#define flamednn_sync_data(AGT, NET, L, S1, S2) \
    flamednn::sync_host<float>(tensor_data::AGT::NET::L ## _dev, \
        tensor_data::AGT::NET::L, S1 * S2)

/* Macros for tensors */
// better
#define flamednn_create_tensor(AGT, NET, LAYER, N, C) \
    flamednn::create_tensor<float, N, C>(tensors::AGT::NET::LAYER, \
        &tensor_data::AGT::NET::LAYER ## _dev, &tensor_data::AGT::NET::LAYER)

// bad
#define make_tensor_data(NAME, N, W) \
    cudnnCreateTensorDescriptor(&tensors::NAME); \
    cudnnSetTensor4dDescriptor(tensors::NAME, CUDNN_TENSOR_NCHW, CUDNN_DATA_FLOAT, \
        N, 1, 1, W); \
    cudaMallocManaged(&tensor_data::NAME, N * W * sizeof(float))

/* Macros for layers */
#define flamednn_create_simple_layer(AGT, NET, LAYER, IN, OUT) \
    flamednn::create_simple_layer(weights::AGT::NET::LAYER, IN, OUT); \
    flamednn::create_simple_layer_weights(&weights::AGT::NET::LAYER ## _dev, \
        &weights::AGT::NET::LAYER ## _host, IN, OUT)

/* Macros for weights */
#define flamednn_init_weights(AGT, NET, LAYER, DIST, IN, OUT) \
    for (size_t i = 0; i < IN * OUT; ++i) \
        weights::AGT::NET::LAYER ## _host[i] = DIST(rng);

#define flamednn_sync_dev_weights(AGT, NET, L, IN, OUT) \
    flamednn::sync_device<float>(weights::AGT::NET::L ## _dev, \
        weights::AGT::NET::L ## _host, IN * OUT)

/* Network Processing */
#define flamednn_convolve(AGT, NET, IN, W, OUT) \
    cudnnConvolutionForward(dnn_handler, &scale_alpha, tensors::AGT::NET::IN, \
        tensor_data::AGT::NET::IN ## _dev, weights::AGT::NET::W, \
        weights::AGT::NET::W ## _dev, basic_conv,   \
        CUDNN_CONVOLUTION_FWD_ALGO_IMPLICIT_GEMM, nullptr, 0, \
        &scale_beta, tensors::AGT::NET::OUT, tensor_data::AGT::NET::OUT ## _dev)

#define flamednn_activate(AGT, NET, LAYER) \
    cudnnActivationForward(dnn_handler, relu_desc, \
        &scale_alpha, tensors::AGT::NET::LAYER, tensor_data::AGT::NET::LAYER##_dev, \
        &scale_beta, tensors::AGT::NET::LAYER, tensor_data::AGT::NET::LAYER##_dev);

#define flamednn_relu(AGT, NET, LAYER) \
    cudnnActivationForward(dnn_handler, relu_desc, \
        &scale_alpha, tensors::AGT::NET::LAYER, tensor_data::AGT::NET::LAYER##_dev, \
        &scale_beta, tensors::AGT::NET::LAYER, tensor_data::AGT::NET::LAYER##_dev);
