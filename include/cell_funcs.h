#pragma once

/// TODO FINISH COPYING VERSIONS

FLAMEGPU_AGENT_FUNCTION(\
update_neighbors, flamegpu::MessageNone, flamegpu::MessageBucket)
{
}

FLAMEGPU_AGENT_FUNCTION(\
update_farm_use_, flamegpu::MessageBucket, flamegpu::MessageBucket)
{
    FLAMEGPU->message_out.setKey(FLAMEGPU->getVariable<unsigned>("parcel_id"));
    FLAMEGPU->message_out.setVariable<unsigned>("cover", FLAMEGPU->getVariable<unsigned>("land_cover"));
}

FLAMEGPU_AGENT_FUNCTION(\
update_urban_use, flamegpu::MessageBucket, flamegpu::MessageBucket)
{
}

FLAMEGPU_AGENT_FUNCTION(\
update_forest_use, flamegpu::MessageBucket, flamegpu::MessageBucket)
{
}

FLAMEGPU_AGENT_FUNCTION(\
notify_all, flamegpu::MessageNone, flamegpu::MessageBucket)
{
}
