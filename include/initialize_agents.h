#pragma once
#include "config.h"

/// TODO: CLEANUP
/// REMOVE / FIX KERNEL STUFF @c kernin and @c kerno
/// CLEANUP NETWORK LINKS SO GENERALIZABLE AND NOT HARD CODED

curandState* states_x;

__global__
void
kernin(){}

FLAMEGPU_INIT_FUNCTION(\
initialize_agents)
{
    using namespace lct_abm;
    using flamerand::triangle;

    array<unsigned, FARM_NUM> farm_ids;
    size_t num_avl = 0;
    host_for_agents(farmer, farmer)
    {
        farm_ids[num_avl++] = farmer.getVariable<unsigned>("parcel_id");
        farmer.setVariable<float>("corn_production_factor",
            flamerand::triangle(FARMER_CORN_MIN, FARMER_CORN_MAX,
                                FARMER_CORN_MEAN, U));
        farmer.setVariable<float>("hay_production_factor",
            flamerand::triangle(FARMER_HAY_MIN, FARMER_HAY_MAX,
                                FARMER_HAY_MEAN, U));
        farmer.setVariable<float>("beef_production_factor",
            flamerand::triangle(FARMER_MEAT_MIN, FARMER_MEAT_MAX,
                                FARMER_MEAT_MEAN, U));
        farmer.setVariable<float>("dairy_production_factor",
            flamerand::triangle(FARMER_MILK_MIN, FARMER_MILK_MAX,
                                FARMER_MILK_MEAN, U));
        farmer.setVariable<float>("phos_production_factor_corn",
            flamerand::triangle(FARMER_CORN_PHOS_MIN, FARMER_CORN_PHOS_MAX,
                                FARMER_CORN_PHOS_MEAN, U));
        farmer.setVariable<float>("phos_production_factor_cows",
            flamerand::triangle(FARMER_COW_PHOS_MIN, FARMER_COW_PHOS_MAX,
                                FARMER_COW_PHOS_MEAN, U));
        farmer.setVariable<float>("phos_production_factor_hay",
            flamerand::triangle(FARMER_HAY_PHOS_MIN, FARMER_HAY_PHOS_MAX,
                                FARMER_HAY_PHOS_MEAN, U));
        farmer.setVariable<unsigned>("cow_population",
            static_cast<unsigned>(flamerand::triangle(FARMER_BOVINE_MIN,
                                  FARMER_BOVINE_MAX, FARMER_BOVINE_MEAN, U)));
    }
    array<unsigned, FARM_NUM> farm_edges;
    farm_edges.fill(FARM_MAX_NEIGHBORS);
    array<bool, FARM_NUM> has_edges;
    has_edges.fill(true);
    while (num_avl > 1)
    {
        size_t node_left, node_right;
        node_left = flamerand::uniform<size_t>(0, FARM_NUM, U);
        node_right = flamerand::uniform<size_t>(0, FARM_NUM, U);
        while (not has_edges[node_left])
        {
            node_left = (node_left + 1) % FARM_NUM;
        }
        while ((not has_edges[node_right]) or (node_left == node_right))
        {
            node_right = (node_right + 1) % FARM_NUM;
        }

        // @TODO Need to guarentee unique links
        /*
            if links[idx_l].contains(idx_r)
        */
        has_edges[node_left] = (farm_edges[node_left] = farm_edges[node_left] - 1) != 0;
        has_edges[node_right] = (farm_edges[node_right] = farm_edges[node_right] - 1) != 0;
        string var_name_neighbor, edge_name_left, edge_name_right;
        var_name_neighbor = "neighbor";
        edge_name_left = var_name_neighbor + to_string(4 - farm_edges[node_left]);
        edge_name_right = var_name_neighbor + to_string(4 - farm_edges[node_right]);
        farmer_vec[node_left].setVariable<unsigned>(edge_name_left, farm_ids[node_right]);
        farmer_vec[node_right].setVariable<unsigned>(edge_name_right, farm_ids[node_left]);
        num_avl -= ((not has_edges[node_left]) + (not has_edges[node_right]));
    }
    FLAMEGPU->environment.setProperty<unsigned>("global_broken_links_farms",
                                                num_avl);
    
    size_t forester_free_links = 0;
    array<unsigned, FOR_NUM> forester_ids;
    host_for_agents(forester, forester)
    {
        forester_ids[forester_free_links++]
            = forester.getVariable<unsigned>("parcel_id");

        host_init_array(forester, land_cover, unsigned, NLCD_CATS_FR, 0u);

        forester.setVariable<unsigned>("history_idx", 0u);
    }

    array<unsigned, FOR_NUM> forester_edges;
    array<bool, FOR_NUM> forester_has_edges;
    forester_edges.fill(FOREST_MAX_NEIGHBORS);
    forester_has_edges.fill(true);
    while (forester_free_links > 1)
    {
        size_t idx_l = FLAMERANDI(0, FOR_NUM);
        while (not forester_has_edges[idx_l])
            idx_l = (idx_l + 1) % FOR_NUM;
        size_t idx_r = FLAMERANDI(0, FOR_NUM);
        while ((not forester_has_edges[idx_r]) or (idx_l == idx_r))
            idx_r = (idx_r + 1) % FOR_NUM;
        forester_has_edges[idx_l]
            = (forester_edges[idx_l] = forester_edges[idx_l] - 1) != 0;
        forester_has_edges[idx_r]
            = (forester_edges[idx_r] = forester_edges[idx_r] - 1) != 0;
        string var_name_neighbor, edge_name_left, edge_name_right;
        var_name_neighbor = "neighbor";
        edge_name_left = var_name_neighbor + to_string(FOREST_MAX_NEIGHBORS - forester_edges[idx_l] - 1);
        edge_name_right = var_name_neighbor + to_string(FOREST_MAX_NEIGHBORS - forester_edges[idx_r] - 1);
        forester_vec[idx_l].setVariable<unsigned>(edge_name_left, forester_ids[idx_r]);
        forester_vec[idx_r].setVariable<unsigned>(edge_name_right, forester_ids[idx_l]);
        /*
        switch (forester_edges[idx_l])
        {
            case 4:
                forester_vec[idx_l].setVariable<unsigned>("neighbor0",
                                                          forester_ids[idx_r]);
                break;
            case 3:
                forester_vec[idx_l].setVariable<unsigned>("neighbor1",
                                                          forester_ids[idx_r]);
                break;
            case 2:
                forester_vec[idx_l].setVariable<unsigned>("neighbor2",
                                                          forester_ids[idx_r]);
                break;
            case 1:
                forester_vec[idx_l].setVariable<unsigned>("neighbor3",
                                                          forester_ids[idx_r]);
                break;
            case 0:
                forester_vec[idx_l].setVariable<unsigned>("neighbor4",
                                                          forester_ids[idx_r]);
                break;
        }
        switch (forester_edges[idx_r])
        {
            case 4:
                forester_vec[idx_r].setVariable<unsigned>("neighbor0",
                                                          forester_ids[idx_l]);
                break;
            case 3:
                forester_vec[idx_r].setVariable<unsigned>("neighbor1",
                                                          forester_ids[idx_l]);
                break;
            case 2:
                forester_vec[idx_r].setVariable<unsigned>("neighbor2",
                                                          forester_ids[idx_l]);
                break;
            case 1:
                forester_vec[idx_r].setVariable<unsigned>("neighbor3",
                                                          forester_ids[idx_l]);
                break;
            case 0:
                forester_vec[idx_r].setVariable<unsigned>("neighbor4",
                                                          forester_ids[idx_l]);
                break;
        }
        */
        forester_free_links -= ((not forester_has_edges[idx_l])
                                + (not forester_has_edges[idx_r]));
    }
    FLAMEGPU->environment.setProperty<unsigned>(
        "global_broken_links_forests", forester_free_links);

    array<unsigned, RES_NUM> ind_of_residence;
    array<unsigned, RES_NUM> pid_of_residence;
    unsigned owner_id = 0u;

    host_for_agents(urban_residence, res)
    {
        ind_of_residence[owner_id] = owner_id;
        pid_of_residence[owner_id] = res.getVariable<unsigned>("parcel_id");
        res.setVariable<unsigned>("worker_id", res.getVariable<unsigned>("parcel_id"));
        res.setVariable<unsigned>("owner_id", owner_id++);
        res.setVariable<unsigned>("wants_job", true);
        res.setVariable<unsigned>("wants_house", false);

        float initial_rent = flamerand::uniform<float>(400.0, 1100.0, U);
        res.setVariable<float>("rent", initial_rent);

        res.setVariable<float>("budget", 0.0f);
    }

    for (size_t i = 0; i < RES_NUM; ++i)
    {
        size_t j = static_cast<unsigned>(flamerand::uniform<size_t>(i, RES_NUM, U));
        unsigned p = pid_of_residence[i];
        pid_of_residence[i] = pid_of_residence[j];
        pid_of_residence[j] = p;
        unsigned t = ind_of_residence[i];
        ind_of_residence[i] = ind_of_residence[j];
        ind_of_residence[j] = t;
    }

    size_t biz_with_open = 0;
    array<unsigned, BIZ_NUM> pid_of_business;
    array<bool, BIZ_NUM> has_open_slots;
    has_open_slots.fill(true);
    array<unsigned, BIZ_NUM> open_slots;
    open_slots.fill(INITIAL_ECAP);
    host_for_agents(urban_business, biz)
    {
        const unsigned parcel_id = biz.getVariable<unsigned>("parcel_id");
        biz.setVariable<mood_t>("mood", business::init);

        const unsigned employee_capacity = INITIAL_ECAP;
        biz.setVariable<unsigned>("employee_capacity", employee_capacity);
        biz.setVariable<unsigned>("days_operating", 1u);

        pid_of_business[biz_with_open++] = biz.getVariable<unsigned>("parcel_id");

        host_init_array(biz, employee_ids, unsigned, MAX_EMPLOYEES, 0u);
        host_init_array(biz, employee_salaries, float, MAX_EMPLOYEES, 0.0f);
    }
    array<float, 16> salary_list
        = {250, 300, 350, 400, 450, 500, 550, 600,
           650, 700, 750, 800, 850, 900, 950, 1000};
    for (size_t eid = 0; (eid < RES_NUM) and (biz_with_open > 0); ++eid)
    {
        size_t employee_id = ind_of_residence[eid];
        unsigned worker_id = pid_of_residence[eid];
        size_t employer_id = FLAMERANDI(0, BIZ_NUM);
        size_t salary_idx
            = flamerand::categorical<float>(16,
                10, 20, 30, 50, 70, 85, 90, 85,
                70, 60, 50, 50, 50, 50, 30, 20);
        float employee_salary = salary_list[salary_idx];
        while (not has_open_slots[employer_id])
            employer_id = (employer_id + 1) % BIZ_NUM;
        has_open_slots[employer_id]
            = (open_slots[employer_id] = open_slots[employer_id] - 1) != 0;
        biz_with_open -= (not has_open_slots[employer_id]);
        const float employer_tsp = urban_business_vec[employer_id]
                                   .getVariable<float>("total_salary_paid");

        urban_business_vec[employer_id].setVariable<unsigned, MAX_EMPLOYEES>(
            "employee_ids", open_slots[employer_id], worker_id);
        urban_business_vec[employer_id].setVariable<unsigned>("employee_count",
            (10 - open_slots[employer_id]));

        /** ## Salary Stuff */
        urban_business_vec[employer_id].setVariable<float, MAX_EMPLOYEES>(
            "employee_salaries", open_slots[employer_id], employee_salary);
        urban_business_vec[employer_id].setVariable<float>("total_salary_paid",
            employer_tsp + employee_salary);
        urban_residence_vec[employee_id].setVariable<float>("salary", employee_salary);

        urban_residence_vec[employee_id].setVariable<unsigned>("employer_id",
            pid_of_business[employer_id]);
        urban_residence_vec[employee_id].setVariable<unsigned>("wants_job", flame_false);
    }
}
