#pragma once
#include "config.h"
#include "learning.h"
#include "learning_functions.h"

// TODO CLEANUP, DOUBLE-CHECK MALLOCs

__host__ void
load_network_weights()
{
    if (operating_mode == training_mode)
        return;

    float* business_weights
        = flamednn::read_network_weights("bizm.weights", business::mu_size);
    flamednn::print_first_3n("bizm.weights", business_weights);
    memcpy(weights::business::mu::l1_host, business_weights,
           business::l1_memsize);
    memcpy(weights::business::mu::l2_host + business::l1_size,
           business_weights, business::l2_memsize);
    memcpy(weights::business::mu::out_host + business::l1_size
           + business::l2_size, business_weights, business::out_memsize);
    
    float* business_weights_q
        = flamednn::read_network_weights("bizq.weights", business::q_size);
    flamednn::print_first_3n("bizq.weights", business_weights_q);
    memcpy(weights::business::q::l1_host, business_weights_q,
           business::q1_memsize);
    memcpy(weights::business::q::l2_host,
           business_weights_q + business::q1_size, business::q2_memsize);
    memcpy(weights::business::q::out_host,
           business_weights_q + business::q1_size + business::q2_size,
           business::qout_memsize);
}

__host__ void
initialize_learning_residence(std::default_random_engine& rng)
{
    using namespace residence;
    using std::random_device, std::default_random_engine;
    using std::uniform_real_distribution;

    float mag_residence_mu, mag_residence_q;
    mag_residence_mu = sqrtf( static_cast<float>(width_mu_out) / width_mu_in );
    mag_residence_q = sqrtf( static_cast<float>(width_q_out) / width_q_in );

    uniform_real_distribution<float> dist_residence_mu(-mag_residence_mu,
                                                        mag_residence_mu);
    uniform_real_distribution<float> dist_residence_q(-mag_residence_q,
                                                       mag_residence_q);
    flamelog::debug("mag_res_mu %f", mag_residence_mu);
    flamelog::debug("mag_res_q %f", mag_residence_q);

    // MU
    flamednn_create_tensor(residence, mu, in, num_agents, width_mu_in);
    flamednn_create_tensor(residence, mu, l1, num_agents, width_mu_l1);
    flamednn_create_tensor(residence, mu, l2, num_agents, width_mu_l2);
    flamednn_create_tensor(residence, mu, out, num_agents, width_mu_out);

    flamednn_create_simple_layer(residence, mu, l1, width_mu_in, width_mu_l1);
    flamednn_create_simple_layer(residence, mu, l2, width_mu_l1, width_mu_l2);
    flamednn_create_simple_layer(residence, mu, out, width_mu_l2, width_mu_out);

    flamednn_init_weights(residence, mu, l1, dist_residence_mu, width_mu_in,
                          width_mu_l1);
    flamednn_init_weights(residence, mu, l2, dist_residence_mu, width_mu_l1,
                          width_mu_l2);
    flamednn_init_weights(residence, mu, out, dist_residence_mu, width_mu_l2,
                          width_mu_out);

    flamednn_sync_dev_weights(residence, mu, l1, width_mu_in, width_mu_l1);
    flamednn_sync_dev_weights(residence, mu, l2, width_mu_l1, width_mu_l2);
    flamednn_sync_dev_weights(residence, mu, out, width_mu_l2, width_mu_out);

    /// ### Critic Netowrk (Q)
    flamednn_create_tensor(residence, q, in, num_agents, width_q_in);
    flamednn_create_tensor(residence, q, l1, num_agents, width_q_l1);
    flamednn_create_tensor(residence, q, l2, num_agents, width_q_l2);
    flamednn_create_tensor(residence, q, out, num_agents, width_q_out);

    flamednn_create_simple_layer(residence, q, l1, width_q_in, width_q_l1);
    flamednn_create_simple_layer(residence, q, l2, width_q_l1, width_q_l2);
    flamednn_create_simple_layer(residence, q, out, width_q_l2, width_q_out);

    flamednn_init_weights(residence, q, l1, dist_residence_q,
        residence::width_q_in, residence::width_q_l1);
    flamednn_init_weights(residence, q, l2, dist_residence_q,
        residence::width_q_l1, residence::width_q_l2);
    flamednn_init_weights(residence, q, out, dist_residence_q,
        residence::width_q_l2, residence::width_q_out);

    flamednn_sync_dev_weights(residence, q, l1,
        residence::width_q_in, residence::width_q_l1);
    flamednn_sync_dev_weights(residence, q, l2,
        residence::width_q_l1, residence::width_q_l2);
    flamednn_sync_dev_weights(residence, q, out,
        residence::width_q_l2, residence::width_q_out);
    
    /// ### Memory Bank
    flamednn::create_tensor<float, num_agents, width_mem, memory_depth>(
        tensors::residence::memory::bank,
        &tensor_data::residence::memory::bank_dev,
        &tensor_data::residence::memory::bank);
    flamednn::create_tensor<float, num_agents, width_mem,
                            replay_depth>(
        tensors::residence::memory::buffer,
        &tensor_data::residence::memory::buffer_dev,
        &tensor_data::residence::memory::buffer);
}

__host__ void
initialize_learning_cudnn()
{
    load_network_weights();
    // fprintf(stdout, "WHAT\n");
    /** Basic Utilities */
    cudnnCreateActivationDescriptor(&relu_desc);
    cudnnSetActivationDescriptor(relu_desc, CUDNN_ACTIVATION_RELU,
        CUDNN_NOT_PROPAGATE_NAN, 0.0f);

    cudnnCreateConvolutionDescriptor(&basic_conv);
    cudnnSetConvolution2dDescriptor(basic_conv, 0, 0, 1, 1, 1, 1,
        CUDNN_CONVOLUTION, CUDNN_DATA_FLOAT);
    
    /** Weight Initializers */
    flamednn::init(dnn_handler, CUDNN_DEVICE, FLAMEGPU_DEVICE);
    float mag_business_mu, mag_business_q;
    float mag_farmer_mu, mag_farmer_q;
    float mag_forester_mu, mag_forester_q;

    mag_business_mu = sqrtf( static_cast<float>(business::width_mu_out)
        / business::width_mu_in );
    mag_business_q = sqrtf( static_cast<float>(business::width_q_out)
        / business::width_q_in );
    mag_farmer_mu = sqrtf( farmer::width_mu_out / farmer::width_mu_in );
    mag_farmer_q = sqrtf( farmer::width_q_out / farmer::width_q_in );

    std::random_device rd;
    std::default_random_engine rng(rd());
    std::uniform_real_distribution<float> dist_business_mu(-mag_business_mu, mag_business_mu);
    std::uniform_real_distribution<float> dist_business_Q(-mag_business_q, mag_business_q);

    /// ## Farmer Networks
    /// ### Actor Network (mu)
//    const float fml = sqrtf( 6.0f / FARM_STATE_WIDTH );
    std::uniform_real_distribution<float> fmd(-mag_farmer_mu, mag_farmer_mu);
    /// *Network Nodes*
    flamednn_create_tensor(farmer, mu, in,
        farmer::num_agents, farmer::width_mu_in);
    flamednn_create_tensor(farmer, mu, l1,
        farmer::num_agents, farmer::width_mu_l1);
    flamednn_create_tensor(farmer, mu, l2,
        farmer::num_agents, farmer::width_mu_l2);
    flamednn_create_tensor(farmer, mu, l3,
        farmer::num_agents, farmer::width_mu_l3);
    flamednn_create_tensor(farmer, mu, l4,
        farmer::num_agents, farmer::width_mu_l4);
    flamednn_create_tensor(farmer, mu, out,
        farmer::num_agents, farmer::width_mu_out);

    /// *Weights between Layers*
    flamednn_create_simple_layer(farmer, mu, l1,
        farmer::width_mu_in, farmer::width_mu_l1);
    flamednn_create_simple_layer(farmer, mu, l2,
        farmer::width_mu_l1, farmer::width_mu_l2);
    flamednn_create_simple_layer(farmer, mu, l3,
        farmer::width_mu_l2, farmer::width_mu_l3);
    flamednn_create_simple_layer(farmer, mu, l4,
        farmer::width_mu_l3, farmer::width_mu_l4);
    flamednn_create_simple_layer(farmer, mu, out,
        farmer::width_mu_l4, farmer::width_mu_out);

    flamednn_init_weights(farmer, mu, l1, fmd,
        farmer::width_mu_in, farmer::width_mu_l1);
    flamednn_init_weights(farmer, mu, l2, fmd,
        farmer::width_mu_l1, farmer::width_mu_l2);
    flamednn_init_weights(farmer, mu, l3, fmd,
        farmer::width_mu_l2, farmer::width_mu_l3);
    flamednn_init_weights(farmer, mu, l4, fmd,
        farmer::width_mu_l3, farmer::width_mu_l4);
    flamednn_init_weights(farmer, mu, out, fmd,
        farmer::width_mu_l4, farmer::width_mu_out);

    flamednn_sync_dev_weights(farmer, mu, l1,
        farmer::width_mu_in, farmer::width_mu_l1);
    flamednn_sync_dev_weights(farmer, mu, l2,
        farmer::width_mu_l1, farmer::width_mu_l2);
    flamednn_sync_dev_weights(farmer, mu, l3,
        farmer::width_mu_l2, farmer::width_mu_l3);
    flamednn_sync_dev_weights(farmer, mu, l4,
        farmer::width_mu_l3, farmer::width_mu_l4);
    flamednn_sync_dev_weights(farmer, mu, out,
        farmer::width_mu_l4, farmer::width_mu_out);

    /// ### Critic Netowrk (Q)
    flamednn_create_tensor(farmer, q, in,
        farmer::num_agents, farmer::width_q_in);
    flamednn_create_tensor(farmer, q, l1,
        farmer::num_agents, farmer::width_q_l1);
    flamednn_create_tensor(farmer, q, l2,
        farmer::num_agents, farmer::width_q_l2);
    flamednn_create_tensor(farmer, q, l3,
        farmer::num_agents, farmer::width_q_l3);
    flamednn_create_tensor(farmer, q, out,
        farmer::num_agents, farmer::width_q_out);

    /// *Weights between Layers*
    flamednn_create_simple_layer(farmer, q, l1,
        farmer::width_q_in, farmer::width_q_l1);
    flamednn_create_simple_layer(farmer, q, l2,
        farmer::width_q_l1, farmer::width_q_l2);
    flamednn_create_simple_layer(farmer, q, l3,
        farmer::width_q_l2, farmer::width_q_l3);
    flamednn_create_simple_layer(farmer, q, out,
        farmer::width_q_l3, farmer::width_q_out);

    flamednn_init_weights(farmer, q, l1, fmd,
        farmer::width_q_in, farmer::width_q_l1);
    flamednn_init_weights(farmer, q, l2, fmd,
        farmer::width_q_l1, farmer::width_q_l2);
    flamednn_init_weights(farmer, q, l3, fmd,
        farmer::width_q_l2, farmer::width_q_l3);
    flamednn_init_weights(farmer, q, out, fmd,
        farmer::width_q_l3, farmer::width_q_out);

    flamednn_sync_dev_weights(farmer, q, l1,
        farmer::width_q_in, farmer::width_q_l1);
    flamednn_sync_dev_weights(farmer, q, l2,
        farmer::width_q_l1, farmer::width_q_l2);
    flamednn_sync_dev_weights(farmer, q, l3,
        farmer::width_q_l2, farmer::width_q_l3);
    flamednn_sync_dev_weights(farmer, q, out,
        farmer::width_q_l3, farmer::width_q_out);

    /// ## Forester Networks
    std::uniform_real_distribution<float> fQd(-mag_farmer_q, mag_farmer_q);
    /// ### Actor Network (mu)
    flamednn_create_tensor(forester, mu, in,
        forester::num_agents, forester::width_mu_in);
    flamednn_create_tensor(forester, mu, l1,
        forester::num_agents, forester::width_mu_l1);
    flamednn_create_tensor(forester, mu, l2,
        forester::num_agents, forester::width_mu_l2);
    flamednn_create_tensor(forester, mu, l3,
        forester::num_agents, forester::width_mu_l3);
    flamednn_create_tensor(forester, mu, l4,
        forester::num_agents, forester::width_mu_l4);
    flamednn_create_tensor(forester, mu, out,
        forester::num_agents, forester::width_mu_out);

    /// *Weights between Layers*
    flamednn_create_simple_layer(forester, mu, l1,
        forester::width_mu_in, forester::width_mu_l1);
    flamednn_create_simple_layer(forester, mu, l2,
        forester::width_mu_l1, forester::width_mu_l2);
    flamednn_create_simple_layer(forester, mu, l3,
        forester::width_mu_l2, forester::width_mu_l3);
    flamednn_create_simple_layer(forester, mu, l4,
        forester::width_mu_l3, forester::width_mu_l4);
    flamednn_create_simple_layer(forester, mu, out,
        forester::width_mu_l4, forester::width_mu_out);

    flamednn_init_weights(forester, mu, l1, dist_business_mu,
        forester::width_mu_in, forester::width_mu_l1);
    flamednn_init_weights(forester, mu, l2, dist_business_mu, 
        forester::width_mu_l1, forester::width_mu_l2);
    flamednn_init_weights(forester, mu, l3, dist_business_mu,
        forester::width_mu_l2, forester::width_mu_l3);
    flamednn_init_weights(forester, mu, l4, dist_business_mu,
        forester::width_mu_l3, forester::width_mu_l4);
    flamednn_init_weights(forester, mu, out, dist_business_mu,
        forester::width_mu_l4, forester::width_mu_out);

    flamednn_sync_dev_weights(forester, mu, l1,
        forester::width_mu_in, forester::width_mu_l1);
    flamednn_sync_dev_weights(forester, mu, l2,
        forester::width_mu_l1, forester::width_mu_l2);
    flamednn_sync_dev_weights(forester, mu, l3,
        forester::width_mu_l2, forester::width_mu_l3);
    flamednn_sync_dev_weights(forester, mu, l4,
        forester::width_mu_l3, forester::width_mu_l4);
    flamednn_sync_dev_weights(forester, mu, out,
        forester::width_mu_l4, forester::width_mu_out);

    /// ### Critic Netowrk (Q)
    flamednn_create_tensor(forester, q, in,
        forester::num_agents, forester::width_q_in);
    flamednn_create_tensor(forester, q, l1,
        forester::num_agents, forester::width_q_l1);
    flamednn_create_tensor(forester, q, l2,
        forester::num_agents, forester::width_q_l2);
    flamednn_create_tensor(forester, q, out,
        forester::num_agents, forester::width_q_out);

    /// *Weights between Layers*
    flamednn_create_simple_layer(forester, q, l1,
        forester::width_q_in, forester::width_q_l1);
    flamednn_create_simple_layer(forester, q, l2,
        forester::width_q_l1, forester::width_q_l2);
    flamednn_create_simple_layer(forester, q, l3,
        forester::width_q_l2, forester::width_q_l3);
    flamednn_create_simple_layer(forester, q, out,
        forester::width_q_l3, forester::width_q_out);

    flamednn_init_weights(forester, q, l1, fmd,
        forester::width_q_in, forester::width_q_l1);
    flamednn_init_weights(forester, q, l2, fmd,
        forester::width_q_l1, forester::width_q_l2);
    flamednn_init_weights(forester, q, l3, fmd,
        forester::width_q_l2, forester::width_q_l3);
    flamednn_init_weights(forester, q, out, fmd,
        forester::width_q_l3, forester::width_q_out);

    flamednn_sync_dev_weights(forester, q, l1,
        forester::width_q_in, forester::width_q_l1);
    flamednn_sync_dev_weights(forester, q, l2,
        forester::width_q_l1, forester::width_q_l2);
    flamednn_sync_dev_weights(forester, q, l3,
        forester::width_q_l2, forester::width_q_l3);
    flamednn_sync_dev_weights(forester, q, out,
        forester::width_q_l3, forester::width_q_out);

    /// ## Business Networks
    /// ### Actor Network (mu)
    /// *Network Nodes*
    flamednn_create_tensor(business, mu, in, business::num_agents,
        business::width_mu_in);
    flamednn_create_tensor(business, mu, l1, business::num_agents,
        business::width_mu_l1);
    flamednn_create_tensor(business, mu, l2, business::num_agents,
        business::width_mu_l2);
    flamednn_create_tensor(business, mu, out, business::num_agents,
        business::width_mu_out);

    /// *Weights between Layers*
    flamednn_create_simple_layer(business, mu, l1, business::width_mu_in,
        business::width_mu_l1);
    flamednn_create_simple_layer(business, mu, l2, business::width_mu_l1,
        business::width_mu_l2);
    flamednn_create_simple_layer(business, mu, out, business::width_mu_l2,
        business::width_mu_out);

    if (operating_mode == training_mode)
    {
        flamednn_init_weights(business, mu, l1, dist_business_mu, business::width_mu_in,
                              business::width_mu_l1);
        flamednn_init_weights(business, mu, l2, dist_business_mu, business::width_mu_l1,
                              business::width_mu_l2);
        flamednn_init_weights(business, mu, out, dist_business_mu, business::width_mu_l2,
                              business::width_mu_out);
    }

    flamednn_sync_dev_weights(business, mu, l1, business::width_mu_in,
                              business::width_mu_l1);
    flamednn_sync_dev_weights(business, mu, l2, business::width_mu_l1,
                              business::width_mu_l2);
    flamednn_sync_dev_weights(business, mu, out, business::width_mu_l2,
                              business::width_mu_out);

    /// ### Critic Netowrk (Q)
    /// *Network Nodes*
    flamednn_create_tensor(business, q, in, business::num_agents,
                           business::width_q_in);
    flamednn_create_tensor(business, q, l1, business::num_agents,
                           business::width_q_l1);
    flamednn_create_tensor(business, q, l2, business::num_agents,
                           business::width_q_l2);
    flamednn_create_tensor(business, q, out, business::num_agents,
                           business::width_q_out);

    /// *Weights between Layers*
    flamednn_create_simple_layer(business, q, l1, business::width_q_in,
                                 business::width_q_l1);
    flamednn_create_simple_layer(business, q, l2, business::width_q_l1,
                                 business::width_q_l2);
    flamednn_create_simple_layer(business, q, out, business::width_q_l2,
                                 business::width_q_out);

    if (operating_mode == training_mode)
    {
        flamednn_init_weights(business, q, l1, dist_business_Q, business::width_q_in,
            business::width_q_l1);
        flamednn_init_weights(business, q, l2, dist_business_Q, business::width_q_l1,
            business::width_q_l2);
        flamednn_init_weights(business, q, out, dist_business_Q, business::width_q_l2, 
            business::width_q_out);
    }

    flamednn_sync_dev_weights(business, q, l1, business::width_q_in,
        business::width_q_l1);
    flamednn_sync_dev_weights(business, q, l2, business::width_q_l1,
        business::width_q_l2);
    flamednn_sync_dev_weights(business, q, out, business::width_q_l2, 
        business::width_q_out);

    /// ## Agent Memory
    /// ### Memory Bank
    flamednn::create_tensor<float, business::num_agents, business::width_mem,
                            business::memory_depth>(
        tensors::business::memory::bank,
        &tensor_data::business::memory::bank_dev,
        &tensor_data::business::memory::bank);
    flamednn::create_tensor<float, business::num_agents, business::width_mem,
                            business::replay_depth>(
        tensors::business::memory::buffer,
        &tensor_data::business::memory::buffer_dev,
        &tensor_data::business::memory::buffer);

    initialize_learning_residence(rng);

    flamednn::toggle_device();
}

FLAMEGPU_INIT_FUNCTION(\
initialize_learning_flame)
{}
