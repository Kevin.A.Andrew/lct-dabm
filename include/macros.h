#pragma once
#include "config.h"

/// TODO THIS FILE NEEDS A LOTTTT OF CLEANUP
/// MOSTLY NEED TO REMOVE REDUNDANCIES
/// AND MOVE SOME OF IT TO PROPER FUNCTIONS
/// FROM WHEN I DIDN"T KNOW HOW TO DO THAT

#ifdef DEBUG
#define CUDA_CALL(X) \
    do { if((X) != cudaSuccess) { \
    printf("Error at %s:%d\n",__FILE__,__LINE__); \
    return exit(EXIT_FAILURE);}} while(0)
#else
#define CUDA_CALL(X) X
#endif

#define tensor_data_addr(ANAME, CNAME, PNAME, ENUM, EWIDTH, OFFSET) \
    (tensor_data::ANAME::CNAME::PNAME + (ENUM * EWIDTH) + OFFSET)

#define biz_addr_mu_in(AGT_ID) \
    tensor_data_addr(business, mu, in, AGT_ID, business::width_mu_in, 0)
#define biz_dev_addr_q_out(AGT_ID) \
    tensor_data_addr(business, q, out_dev, AGT_ID, 10, 0)
#define biz_dev_addr_membank(AGT_ID, MEM_ID) \
    tensor_data_addr(business, memory, bank_dev, AGT_ID, \
        (business::memory_depth * business::width_mem), \
        (MEM_ID * business::width_mem))

//#define biz_addr_mem_in(AGT_ID) 
//    tensor_data_addr(business, memory, bank_dev, AGT_ID, (AGT_ID * business::width_mem), ())

#define macro_put_array(AGT, PRE, TYPE, NUM)     \
    for (size_t m_idx = 0; m_idx < NUM; ++m_idx) \
    {                                            \
        char buff[3]; \
        sprintf(buff, "%zu", m_idx); \
        std::string iname = std::string(#PRE)    \
                            + buff;  \
        AGT.newVariable<TYPE>(iname.c_str());    \
    }

#define macro_put_salary_list_arr \
    std::array<float, 16> salary_opts \
        = {250, 300, 350, 400, 450, 500, 550, 600, \
           650, 700, 750, 800, 850, 900, 950, 1000}

#define host_init_array(AGT, ARRAY, TYPE, WIDTH, VALUE) \
    { \
        std::array<TYPE, WIDTH> AGT ## _ ## ARRAY = AGT.getVariable<TYPE, WIDTH>(#ARRAY); \
        (AGT ## _ ## ARRAY).fill(VALUE); \
        AGT.setVariable<TYPE, WIDTH>(#ARRAY, AGT ## _ ## ARRAY); \
    }

#define host_get_array(AGT, ARRAY, TYPE, WIDTH) \
    AGT.getVariable<TYPE, WIDTH>(#ARRAY)

#define host_set_array(AGT, ARRAY, TYPE, WIDTH, BRRAY) \
    AGT.setVariable<TYPE, WIDTH>(#ARRAY, BRRAY)

#define macro_for_agents(NAME, IDEN, IDX) \
    {\
    size_t IDX = 0; \
    host_for_agents(NAME, IDEN)

#define macro_endfor_agents }

#define host_for_agents(NAME, IDEN) \
    flamegpu::HostAgentAPI NAME ## _api = FLAMEGPU->agent(#NAME); \
    flamegpu::DeviceAgentVector NAME ## _vec = NAME ## _api.getPopulationData(); \
    for (auto IDEN : NAME ## _vec)

#define transition_agent_state(TYPE, WID) \
    for (size_t idx = 0; idx < WID; ++idx) \
        FLAMEGPU->setVariable<TYPE, WID>("dnn_state", idx, \
            FLAMEGPU->getVariable<TYPE, WID>("next_state", idx))

#define transition_biz_state() \
    transition_agent_state(float, CORP_STATE_WIDTH)

#define set_action_x(WIDTH, ACTION) \
    for (size_t idx = 0; idx < WIDTH; ++idx) \
        FLAMEGPU->setVariable<float, WIDTH>("realized_action", idx, ACTION[idx]);

#define set_biz_action(ACTION) \
    set_action_x(CORP_ACTION_WIDTH, ACTION)

#define agent_intent(SIZE, VAL) \
    FLAMEGPU->getVariable<float, SIZE>("action", VAL)

#define biz_intent(VAL) agent_intent(CORP_ACTION_WIDTH, VAL)

#define for_intent(VAL) agent_intent(FOREST_ACTION_WIDTH, VAL)

#define get_employee_id(IDX) \
    FLAMEGPU->getVariable<unsigned, MAX_EMPLOYEES>("employee_ids", IDX)

#define set_employee_id(IDX, VAL) \
    FLAMEGPU->setVariable<unsigned, MAX_EMPLOYEES>("employee_ids", IDX, VAL)

#define get_employee_pay(IDX) \
    FLAMEGPU->getVariable<float, MAX_EMPLOYEES>("employee_salaries", IDX)

#define set_employee_pay(IDX, VAL) \
    FLAMEGPU->setVariable<float, MAX_EMPLOYEES>("employee_salaries", IDX, VAL)


/** Output Reduction Macros */
#define onehot_split2(X, Y) \
    flamemath::onehot(FLAMEGPU->getVariable<float>("action" #X), \
    FLAMEGPU->getVariable<float>("action" #Y))
#define onehot_split3(X, Y, Z) \
    flamemath::onehot(FLAMEGPU->getVariable<float>("action" #X), \
    FLAMEGPU->getVariable<float>("action" #Y), \
    FLAMEGPU->getVariable<float>("action" #Z))


/** Control-flow Macros */
#define xor_split(NAME, X, Y, VAR, TGT) \
    { float a = (action_ ## NAME == 0) ? 1.0f : 0.0f; \
    float b = (action_ ## NAME != 0) ? 1.0f : 0.0f; \
    FLAMEGPU->setVariable<float>("raction" #X, a); \
    FLAMEGPU->setVariable<float>("raction" #Y, b); \
    FLAMEGPU->setVariable<unsigned>(VAR, (action_ ## NAME == TGT)); }
#define switch_split(NAME, X, Y, Z) \
    FLAMEGPU->message_out.setVariable<unsigned>("update_" #NAME, action_ ## NAME); \
    switch (action_ ## NAME) { \
    case 0: FLAMEGPU->setVariable<float>("raction" #X, 1.0f); \
    FLAMEGPU->setVariable<float>("raction" #Y, 0.0f); \
    FLAMEGPU->setVariable<float>("raction" #Z, 0.0f); break; \
    case 1: FLAMEGPU->setVariable<float>("raction" #X, 0.0f); \
    FLAMEGPU->setVariable<float>("raction" #Y, 1.0f); \
    FLAMEGPU->setVariable<float>("raction" #Z, 0.0f); break; \
    case 2: FLAMEGPU->setVariable<float>("raction" #X, 0.0f); \
    FLAMEGPU->setVariable<float>("raction" #Y, 0.0f); \
    FLAMEGPU->setVariable<float>("raction" #Z, 1.0f); break; }


#define rec_neighbor_info_for(X, MPT) \
    { size_t neighbor = FLAMEGPU->getVariable<unsigned>("neighbor" #X); \
      if (neighbor == 0) return flamegpu::ALIVE; \
      for (const auto& message : FLAMEGPU->message_in(neighbor)) \
      { FLAMEGPU->setVariable<unsigned>("neighbor_" #MPT #X, \
                    message.getVariable<unsigned>("used_" #MPT)); \
        FLAMEGPU->setVariable<float>("neighbor_loss" #X, \
                    message.getVariable<float>("had_loss")); break; } }

#define recinfo_forest_neighbor(X) rec_neighbor_info_for(X, amp)
#define recinfo_farm_neighbor(X) rec_neighbor_info_for(X, bmp)

