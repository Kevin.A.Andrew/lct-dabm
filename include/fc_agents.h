#pragma once
#ifndef FLAMEGPU_CONFIG_AGENTS_H_
#define FLAMEGPU_CONFIG_AGENTS_H_
#include "config.h"

/// TODO CLEANUP DATATYPES (OTHERWISE OK (?))

extern flamegpu::ModelDescription model;

void
define_all_agents()
{
    using flameabm::bool_t;
    /// # Agricultural Agents
    auto ag_agent = model.newAgent("farmer");
    ag_agent.newState("default");

    /// ## Local Info
    /// @TODO: Remove unsused/dated variables
    ag_agent.newVariable<bool_t>("using_bmp");
    ag_agent.newVariable<float>("budget");
    ag_agent.newVariable<float>("expected_profit_corn");
    ag_agent.newVariable<float>("expected_profit_dairy");
    ag_agent.newVariable<float>("expected_profit_beef");
    ag_agent.newVariable<float>("expected_profit_hay");
    ag_agent.newVariable<float>("expected_profit_total");
    ag_agent.newVariable<float>("profit_corn");
    ag_agent.newVariable<float>("profit_dairy");
    ag_agent.newVariable<float>("profit_beef");
    ag_agent.newVariable<float>("profit_hay");
    ag_agent.newVariable<float>("profit_total");
    ag_agent.newVariable<float>("costs_bmp");
    ag_agent.newVariable<float>("costs_total");
    ag_agent.newVariable<float>("loss");

    /// ## Land Info
    ag_agent.newVariable<unsigned>("parcel_id");
    macro_put_array(ag_agent, cover, unsigned, farmer::nlcd_cats);
    ag_agent.newVariable<float>("land_area");

    /// ## Connections
    macro_put_array(ag_agent, neighbor, unsigned, farmer::max_neighbors);
    macro_put_array(ag_agent, neighbor_bmp, unsigned, farmer::max_neighbors);
    macro_put_array(ag_agent, neighbor_loss, float, farmer::max_neighbors);

    /// ## Production Factors
    ag_agent.newVariable<float>("corn_production_factor");
    ag_agent.newVariable<float>("beef_production_factor");
    ag_agent.newVariable<float>("dairy_production_factor");
    ag_agent.newVariable<float>("hay_production_factor");
    ag_agent.newVariable<float>("phos_production_factor_corn");
    ag_agent.newVariable<float>("phos_production_factor_cows");
    ag_agent.newVariable<float>("phos_production_factor_hay");
    ag_agent.newVariable<unsigned>("cow_population");

    /// ## On-Agent Memory
    ag_agent.newVariable<float>("exploration_rate");
    macro_put_array(ag_agent, history_bmp, unsigned,
                    farmer::local_action_memory);
    macro_put_array(ag_agent, history_loss, float,
                    farmer::local_action_memory);

    /// ## DNN Memory Exchange
    macro_put_array(ag_agent, state, float, farmer::width_mu_in);
    macro_put_array(ag_agent, action, float, farmer::width_mu_out);
    macro_put_array(ag_agent, raction, float, farmer::width_mu_out);
    macro_put_array(ag_agent, next_state, float, farmer::width_mu_in);
    ag_agent.newVariable<float>("reward");

    /// # Forest Agents
    auto fr_agent = model.newAgent("forester");
    fr_agent.newState("default");

    /// # Local Info
    fr_agent.newVariable<unsigned>("using_amp");
    fr_agent.newVariable<unsigned>("used_amp");
    fr_agent.newVariable<float>("expected_profit");
    fr_agent.newVariable<float>("loss");

    /// ## Land Info
    fr_agent.newVariable<unsigned>("parcel_id");
    fr_agent.newVariable<unsigned>("parcel_size");
    fr_agent.newVariable<unsigned, NLCD_CATS_FR>("land_cover");
    fr_agent.newVariable<float>("forested_area");
    fr_agent.newVariable<float>("new_forested_area");

    /// ## Connections
    macro_put_array(fr_agent, neighbor, unsigned, forester::max_neighbors);
    macro_put_array(fr_agent, neighbor_amp, unsigned, forester::max_neighbors);
    macro_put_array(fr_agent, neighbor_loss, unsigned, forester::max_neighbors);

    /// ## Production Factors
    fr_agent.newVariable<float>("production_factor");
    fr_agent.newVariable<float>("next_production_factor");

    /// ## On-Agent Memory / Learning
    fr_agent.newVariable<float>("exploration_rate");
    macro_put_array(fr_agent, history_amp, unsigned,
                    forester::local_action_memory);
    macro_put_array(fr_agent, history_loss, float,
                    forester::local_action_memory);
    fr_agent.newVariable<unsigned>("history_idx");

    /// ## DNN Memory Exchange
    macro_put_array(fr_agent, state, float, forester::width_mu_in);
    macro_put_array(fr_agent, action, float, forester::width_mu_out);
    macro_put_array(fr_agent, raction, float, forester::width_mu_out);
    macro_put_array(fr_agent, next_state, float, forester::width_mu_in);
    fr_agent.newVariable<float>("reward");

    /// # Residential Agents
    auto ur_agent = model.newAgent("urban_residence");
    ur_agent.newState("default");
    ur_agent.newVariable<unsigned>("is_vacant", false);
    ur_agent.newVariable<unsigned>("parcel_id");
    ur_agent.newVariable<unsigned>("vacant_parcel_id");
    ur_agent.newVariable<float>("vacant_rent");
    ur_agent.newVariable<mood_t>("mood");
    ur_agent.newVariable<unsigned>("time_in_state");
    ur_agent.newVariable<float>("budget");
    ur_agent.newVariable<float>("debug_expenses");
    ur_agent.newVariable<float>("debug_balance");

    /// ## Housing Info
    ur_agent.newVariable<unsigned>("owner_id");
    ur_agent.newVariable<float>("rent");
    ur_agent.newVariable<unsigned>("wants_house");

    /// ## Job Info
    ur_agent.newVariable<unsigned>("worker_id");
    ur_agent.newVariable<unsigned>("employer_id");
    ur_agent.newVariable<float>("salary");
    ur_agent.newVariable<unsigned>("wants_job");

    /// ## On-Agent Memory / Learning
    ur_agent.newVariable<float>("exploration_rate");

    /// ## DNN Memory Exchange
    macro_put_array(ur_agent, state, float, residence::width_mu_in);
    macro_put_array(ur_agent, action, float, residence::width_mu_out);
    macro_put_array(ur_agent, raction, float, residence::width_mu_out);
    macro_put_array(ur_agent, next_state, float, residence::width_mu_in);
    ur_agent.newVariable<float>("reward");

    /// # Commercial Agents
    auto ub_agent = model.newAgent("urban_business");
    ub_agent.newState("default");

    /// ## Land Cover
    macro_put_array(ub_agent, cover, unsigned, business::nlcd_cats);
    ub_agent.newVariable<unsigned>("cells_total", 0u);
    ub_agent.newVariable<unsigned>("cells_forest", 0u);
    ub_agent.newVariable<unsigned>("cells_urban", 0u);

    /// ## Learning Framework
    macro_put_array(ub_agent, state, float, 4);
    macro_put_array(ub_agent, action, float, 6);
    macro_put_array(ub_agent, raction, float, 6);
    macro_put_array(ub_agent, next_state, float, 4);
    ub_agent.newVariable<mood_t>("fin_mood"); /* fin_bad; fin_good */
    ub_agent.newVariable<mood_t>("mood", business::init); /* init; working; grow; shrink */
    ub_agent.newVariable<unsigned>("fcount", 0u);
    ub_agent.newVariable<unsigned>("debug_list", 0u);
    ub_agent.newVariable<unsigned>("debug_make", 0u);
    ub_agent.newVariable<unsigned>("is_hiring");
    ub_agent.newVariable<unsigned>("is_firing");
    ub_agent.newVariable<int>("strategy");
    ub_agent.newVariable<unsigned>("salary_step", 0u);
    ub_agent.newVariable<unsigned>("hiring_step", 0u);

    /// ## Business Info
    ub_agent.newVariable<float>("budget");
    ub_agent.newVariable<unsigned>("days_operating");
    ub_agent.newVariable<unsigned>("employee_count");
    ub_agent.newVariable<unsigned>("employee_capacity");
    ub_agent.newVariable<unsigned, MAX_EMPLOYEES>("employee_ids");
    ub_agent.newVariable<float, MAX_EMPLOYEES>("employee_salaries");
    ub_agent.newVariable<float>("total_salary_paid");

    /// ## Land Info
    ub_agent.newVariable<unsigned>("parcel_id");

    /// ## On-Agent Memory / Learning
    ub_agent.newVariable<float>("exploration_rate");

    /// ## DNN Memory Exchange
    ub_agent.newVariable<float>("reward");

    /// # Land Cell Data 'Agent'
    auto lc_agent = model.newAgent("landcell");
    lc_agent.newState("default");
    lc_agent.newVariable<unsigned>("parcel_id");
    lc_agent.newVariable<unsigned>("land_cover");
}

#endif
