#pragma once
#ifndef FLAMEGPU_CONFIG_MESSAGES_H_
#define FLAMEGPU_CONFIG_MESSAGES_H_
#include "config.h"

/// TODO CLEANUP for datatypes
///      OTHERWISE OK (?)

extern flamegpu::ModelDescription model;

void
define_all_messages()
{
    using flamegpu::MessageBucket;
    auto farm_update_msg = model.newMessage<MessageBucket>("farm_update");
    farm_update_msg.setBounds(MIN_PID, MAX_PID);
    farm_update_msg.newVariable<unsigned>("used_bmp");
    farm_update_msg.newVariable<float>("had_loss");

    auto forest_update_msg = model.newMessage<MessageBucket>("forest_update");
    forest_update_msg.setBounds(MIN_PID, MAX_PID);
    forest_update_msg.newVariable<unsigned>("used_amp");
    forest_update_msg.newVariable<float>("had_loss");

    auto biz_raise_msg = model.newMessage<MessageBucket>("biz_raise");
    biz_raise_msg.setBounds(MIN_PID, MAX_PID);
    biz_raise_msg.newVariable<float>("mult");

    auto biz_fire_msg = model.newMessage<MessageBucket>("biz_fire");
    biz_fire_msg.setBounds(MIN_PID, MAX_PID);
    biz_fire_msg.newVariable<unsigned>("fire");

    auto biz_is_hiring_msg = model.newMessage("biz_is_hiring");
    biz_is_hiring_msg.newVariable<unsigned>("business_id");

    auto apply_to_job_msg = model.newMessage<MessageBucket>("apply_to_job");
    apply_to_job_msg.setBounds(MIN_PID, MAX_PID);
    apply_to_job_msg.newVariable<unsigned>("worker_id");
    apply_to_job_msg.newVariable<float>("target_salary");

    auto job_fill_msg = model.newMessage<MessageBucket>("job_fill");
    job_fill_msg.setBounds(MIN_PID, MAX_PID);
    job_fill_msg.newVariable<unsigned>("business_id");
    job_fill_msg.newVariable<float>("salary");

    auto job_quit_msg = model.newMessage<MessageBucket>("job_leave");
    job_quit_msg.setBounds(MIN_PID, MAX_PID);
    job_quit_msg.newVariable<unsigned>("worker_id");
    job_quit_msg.newVariable<float>("salary");

    auto job_update_msg = model.newMessage<MessageBucket>("job_update");
    job_update_msg.setBounds(MIN_PID, MAX_PID);
    job_update_msg.newVariable<unsigned, MAX_EMPLOYEES>("worker_id");
    job_update_msg.newVariable<float, MAX_EMPLOYEES>("salary");

    auto apt_listing_msg = model.newMessage("apt_listing");
    apt_listing_msg.newVariable<unsigned>("house");
    apt_listing_msg.newVariable<float>("rent");

    auto apt_offer_msg = model.newMessage<MessageBucket>("apt_offer");
    apt_offer_msg.setBounds(MIN_PID, MAX_PID);
    apt_offer_msg.newVariable<unsigned>("owner_id");

    auto apt_sale_msg = model.newMessage<MessageBucket>("apt_sale");
    apt_sale_msg.setBounds(MIN_PID, MAX_PID);
    apt_sale_msg.newVariable<unsigned>("house");
    apt_sale_msg.newVariable<float>("rent");

    auto moving_msg = model.newMessage<MessageBucket>("moving");
    moving_msg.setBounds(MIN_PID, MAX_PID);
    moving_msg.newVariable<unsigned>("owner_id");
    moving_msg.newVariable<unsigned>("worker_id");
    moving_msg.newVariable<unsigned>("employer_id");
    moving_msg.newVariable<float>("salary");

    auto farm_landuse_msg = model.newMessage<MessageBucket>("farm_landuse");
    farm_landuse_msg.setBounds(MIN_PID, MAX_PID);
    farm_landuse_msg.newVariable<unsigned>("update_corn");
    farm_landuse_msg.newVariable<unsigned>("update_dairy");
    farm_landuse_msg.newVariable<unsigned>("update_beef");
    farm_landuse_msg.newVariable<unsigned>("update_hay");
    farm_landuse_msg.newVariable<unsigned>("update_other");

    auto forest_landuse_msg
        = model.newMessage<MessageBucket>("forest_landuse");
    forest_landuse_msg.setBounds(MIN_PID, MAX_PID);
    forest_landuse_msg.newVariable<unsigned>("update_prod");

    auto business_landuse_
        = model.newMessage<MessageBucket>("business_landuse");
    business_landuse_.setBounds(MIN_PID, MAX_PID);
    business_landuse_.newVariable<unsigned>("update_action");

    auto corp_landuse_msg = model.newMessage<MessageBucket>("corp_landuse");
    corp_landuse_msg.setBounds(MIN_PID, MAX_PID);
    corp_landuse_msg.newVariable<unsigned>("update_action");

    auto cell_update_msg = model.newMessage<MessageBucket>("cell_update");
    cell_update_msg.setBounds(MIN_PID, MAX_PID);
    cell_update_msg.newVariable<unsigned>("cover");
}

#endif
