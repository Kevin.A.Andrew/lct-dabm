#pragma once
#include "config.h"
#include "learning.h"
#include "lrn_policy_gradient.h"

#define STEP_BUFFER 1
#define SKIP_STEP_BUFFER \
    if (FLAMEGPU->getStepCounter() < STEP_BUFFER) return;

__host__ size_t *
sample_memory_buffer(size_t buffer_size, size_t memory_size)
{
    using namespace lct_abm;
    size_t *sample = (size_t*)xnmalloc(buffer_size, sizeof(size_t));
    vector<size_t> full_sample(memory_size);
    iota(start_of(full_sample), end_of(full_sample), 0);
    shuffle(start_of(full_sample), end_of(full_sample), mt19937{random_device{}()});
    copy_n(start_of(full_sample), buffer_size, start_of(sample));
    return sample;
}

__host__ void
update_memory_buffer(size_t *samples, size_t *sample, size_t *memory)
{
    using namespace lct_abm;
}

FLAMEGPU_HOST_FUNCTION(\
business_evaluate_action_)
{
    using namespace lct_abm;
    using flamednn::memcpy_device;
    SKIP_STEP_BUFFER;
    int step = FLAMEGPU->getStepCounter();
    int agent_num, agent_index;
    int width_mu_in, width_mu_out;
    int net_width_q_in, net_width_q_out;
    int memory_index, memory_index_max, memory_depth, memory_width;
    int sample_index, sample_depth;
    size_t *memory_sample, memory_size;
    size_t sample_idx, sample_jdx, buffer_idx;
    float *sample_start, *memory_start, *cur_sample, *cur_memory;
    size_t agent_offset_buff, agent_offset_bank;
    size_t sample_offset_buff, sample_offset_bank;
    float *reward_vec;

    agent_num = business::num_agents;
    width_mu_in = business::width_mu_in;
    width_mu_out = business::width_mu_out;
    net_width_q_in = business::width_q_in;
    net_width_q_out = business::width_q_out;
    memory_depth = business::memory_depth;
    memory_width = business::width_mem;
    sample_depth = business::replay_depth;
    cudaMallocManaged(&reward_vec, agent_num * sizeof(float));
    fill(start_of(reward_vec), end_of(reward_vec, agent_num), 0.1f);
    cudaDeviceSynchronize();

    /// # Backward pass for mu
    flamednn::toggle_device();

    /* Load input for Q */
    float *mu_in_ptr, *mu_out_ptr, *q_in_ptr;
    for (agent_index = 0; agent_index < agent_num; ++agent_index)
    {
        mu_in_ptr = tensor_data::business::mu::in_dev_addr(agent_index);
        mu_out_ptr = tensor_data::business::mu::out_dev_addr(agent_index);
        q_in_ptr = tensor_data::business::q::in_dev_addr(agent_index);

        memcpy_device<float>(q_in_ptr, mu_in_ptr, width_mu_in);
        memcpy_device<float>(q_in_ptr + width_mu_in, mu_out_ptr, width_mu_out);
    }
    flamednn_sync_data(business, q, in, agent_num, net_width_q_in);

    /* Do Forward Pass */
    flamednn_convolve(business, q, in, l1, l1);
    flamednn_activate(business, q, l1);
    flamednn_convolve(business, q, l1, l2, l2);
    flamednn_activate(business, q, l2);
    flamednn_convolve(business, q, l2, out, out);
    flamednn_sync_data(business, q, out, agent_num, net_width_q_out);

    /* Store output from Q */

    /* Push Memory Bank */
    memory_index = step % memory_depth;
    for (agent_index = 0; agent_index < agent_num; ++agent_index)
    {
        cur_memory = tensor_data::business::memory::bank_dev_addr(agent_index,
                        memory_index);
        q_in_ptr = tensor_data::business::q::in_dev_addr(agent_index);

        memcpy_device<float>(cur_memory, q_in_ptr, net_width_q_in);
        memcpy_device<float>(cur_memory+net_width_q_in, reward_vec+agent_index, 1);
/*
        flamednn::memcpy_device<float>(
            tensor_data::business::memory::bank_dev_addr(biz_id, memory_index),
            tensor_data::business::q::in_dev_addr(biz_id),
            business::width_q_in);
*/
    }
    flamednn::toggle_device();

    /// ## Create Memory Buffer
    if (step <= sample_depth)
        return;
    memory_size = min(step, memory_depth);
    memory_sample = sample_memory_buffer(sample_depth, memory_size);

    flamednn::toggle_device();
    sample_start = tensor_data::business::memory::buffer_dev;
    memory_start = tensor_data::business::memory::bank_dev;
    for (agent_index = 0; agent_index < agent_num; ++agent_index)
    {
        agent_offset_buff = agent_index * (sample_depth * memory_width);
        agent_offset_bank = agent_index * (memory_depth * memory_width);
        for (sample_index = 0; sample_index < sample_depth; ++sample_index)
        {
            sample_offset_buff = sample_index * memory_width;
            sample_offset_bank = memory_sample[sample_index] * memory_width;
            cur_sample = sample_start + agent_offset_buff + sample_offset_buff;
            cur_memory = memory_start + agent_offset_bank + sample_offset_bank;
            flamednn::memcpy_device<float>(cur_sample, cur_memory,
                memory_width);
        }
    }

    // -- Policy Gradient --
    size_t action_size;

    int action_offset, action_width, action_index;
    int memory_sample_depth, memory_sample_width;
    int threads_per_block, num_blocks;

    float *gradient, *gradient_normalized;
    float eta;

    action_offset = CORP_STATE_WIDTH;
    action_width = CORP_ACTION_WIDTH;
    action_size = action_width * sizeof(float);

    memory_sample_depth = business::replay_depth;
    memory_sample_width = business::width_mem;

    threads_per_block = 32;
    num_blocks = (action_width + threads_per_block - 1) / threads_per_block;

    eta = 0.05f;
    
    cudaMallocManaged(&gradient, agent_num * action_size);
    cudaMallocManaged(&gradient_normalized, action_size);
    fill(start_of(gradient), end_of(gradient, action_width * agent_num), 0.0f);
    fill(start_of(gradient_normalized),
         end_of(gradient_normalized, action_width), 0.0f);
    cudaDeviceSynchronize();

    policy_gradient_kernel<<<num_blocks, threads_per_block>>>(
        tensor_data::business::q::in_dev,
        tensor_data::business::memory::buffer_dev, gradient,
        gradient_normalized, agent_num, memory_sample_depth,
        memory_sample_width, action_offset, action_width, eta);
    cudaDeviceSynchronize();

    cudaFree(gradient);
    cudaFree(gradient_normalized);

    flamednn::toggle_device();
}

FLAMEGPU_HOST_FUNCTION(\
residence_evaluate_action_)
{
    SKIP_STEP_BUFFER;
    using namespace lct_abm;
    int step = FLAMEGPU->getStepCounter();
    int agent_num, agent_index;
    int width_mu_in, width_mu_out;
    int width_q_in, width_q_out;
    int memory_index, memory_index_max, memory_depth, memory_width;
    int sample_index, sample_depth;
    size_t *memory_sample;
    int action_index, action_offset, action_width, action_size;
    int threads_per_block, num_blocks;
    float *gradient, *gradient_normalized;
    float *reward_vec;
    HostAgentAPI agents_api = FLAMEGPU->agent("urban_residence");
    DeviceAgentVector agents_vec = agents_api.getPopulationData();

    agent_num = residence::num_agents;
    width_mu_in = residence::width_mu_in;
    width_mu_out = residence::width_mu_out;
    width_q_in = residence::width_q_in;
    width_q_out = residence::width_q_out;
    memory_depth = residence::memory_depth;
    memory_width = residence::width_mem;
    cudaMallocManaged(&reward_vec, agent_num * sizeof(float));

    agent_index = 0;
    for (auto agent : agents_vec)
    {
        reward_vec[agent_index++] = agent.getVariable<float>("reward");
    }
    cudaDeviceSynchronize();

    flamednn::toggle_device();
    for (agent_index = 0; agent_index < agent_num; ++agent_index)
    {
        flamednn::memcpy_device<float>(
            tensor_data::residence::q::in_dev_addr(agent_index),
            tensor_data::residence::mu::in_dev_addr(agent_index),
            width_mu_in);
        flamednn::memcpy_device<float>(
            tensor_data::residence::q::in_dev_addr(agent_index, width_mu_in),
            tensor_data::residence::mu::out_dev_addr(agent_index),
            width_mu_out);
    }
    flamednn_sync_data(residence, q, in, agent_num, width_q_in);

    flamednn_convolve(residence, q, in, l1, l1);
    flamednn_activate(residence, q, l1);
    flamednn_convolve(residence, q, l1, l2, l2);
    flamednn_activate(residence, q, l2);
    flamednn_convolve(residence, q, l2, out, out);
    flamednn_sync_data(residence, q, out, agent_num, width_q_out);

    memory_index = step % memory_depth;
    memory_index_max = (step > memory_depth) ? memory_depth : step;
    for (agent_index = 0; agent_index < agent_num; ++agent_index)
    {
        flamednn::memcpy_device<float>(
            tensor_data::residence::memory::bank_dev_addr(agent_index,
                                                          memory_index),
            tensor_data::residence::q::in_dev_addr(agent_index),
            width_q_in);
        flamednn::memcpy_device<float>(
            tensor_data::residence::memory::bank_dev_addr(agent_index,
                memory_index) + width_q_in,
             reward_vec+agent_index, 1);
    }

    sample_depth = residence::replay_depth;

    flamednn::toggle_device();
    if (step <= sample_depth)
        return;

    memory_sample = sample_memory_buffer(sample_depth, memory_index_max);

    flamednn::toggle_device();

    float *sample_start, *memory_start, *cur_sample, *cur_memory;
    size_t agent_offset_buff, agent_offset_bank;
    size_t sample_offset_buff, sample_offset_bank;
    sample_start = tensor_data::residence::memory::buffer_dev;
    memory_start = tensor_data::residence::memory::bank_dev;
    for (agent_index = 0; agent_index < agent_num; ++agent_index)
    {
        agent_offset_buff = agent_index * (sample_depth * memory_width);
        agent_offset_bank = agent_index * (memory_depth * memory_width);
        for (sample_index = 0; sample_index < sample_depth; ++sample_index)
        {
            sample_offset_buff = sample_index * memory_width;
            sample_offset_bank = memory_sample[sample_index] * memory_width;
            cur_sample = sample_start + agent_offset_buff + sample_offset_buff;
            cur_memory = memory_start + agent_offset_bank + sample_offset_bank;

            flamednn::memcpy_device<float>(cur_sample, cur_memory,
                                           memory_width);
        }
    }
    flamednn_sync_data(residence, memory, buffer, agent_num, memory_width);

    action_offset = residence::width_mu_in;
    action_width = residence::width_mu_out;
    action_size = action_width * sizeof(float);

    cudaMallocManaged(&gradient, agent_num * action_size);
    cudaMallocManaged(&gradient_normalized, action_size);
    fill(start_of(gradient), end_of(gradient, agent_num * action_width), 0.0f);
    fill(start_of(gradient_normalized), end_of(gradient_normalized, action_width), 0.0f);
    cudaDeviceSynchronize();

    threads_per_block = 256;
    num_blocks = (action_width + threads_per_block - 1) / threads_per_block;

    policy_gradient_kernel<<<num_blocks, threads_per_block>>>(
        tensor_data::residence::q::in_dev,
        tensor_data::residence::memory::buffer_dev, gradient,
        gradient_normalized, agent_num, sample_depth, memory_width,
        action_offset, action_width, 0.05f);
    cudaDeviceSynchronize();

    cudaFree(gradient);
    cudaFree(gradient_normalized);

    flamednn::toggle_device();
}

FLAMEGPU_HOST_FUNCTION(\
farmer_evaluate_action_)
{
    using namespace lct_abm;
    SKIP_STEP_BUFFER;
    int agent_num, agent_index;
    float* reward_vec;
    HostAgentAPI agents_api = FLAMEGPU->agent("farmer");
    DeviceAgentVector agents_vec = agents_api.getPopulationData();

    agent_index = 0;
    for (auto agent : agents_vec)
        reward_vec[agent_index++] = agent.getVariable<float>("reward");
    cudaDeviceSynchronize();
}
