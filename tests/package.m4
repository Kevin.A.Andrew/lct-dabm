# Signature of the current package.
m4_define([AT_PACKAGE_NAME],
  [lct-dabm])
m4_define([AT_PACKAGE_TARNAME],
  [landcll])
m4_define([AT_PACKAGE_VERSION],
  [2.0])
m4_define([AT_PACKAGE_STRING],
  [lct-dabm 2.0])
m4_define([AT_PACKAGE_BUGREPORT],
  [kandrew6@uvm.edu])
m4_define([AT_PACKAGE_URL],
  [])
