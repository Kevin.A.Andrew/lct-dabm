#include "config.h"
#include "config_flamegpu.h"

#include <curand.h>
#include <curand_kernel.h>

#define CUDNN_FRONTEND_LOG_FILE stderr

/** NEED TO ADD OPTIONS for runtime
 *  TODO: CLEANUP FIX FILE I/O      */

int
main(int argc, const char** argv)
{
    /// # CuDNN Functionality (config_learning.h)
    cudnn_frontend::isLoggingEnabled() = true;
    initialize_learning_cudnn();

    /// # FLAMEGPU Functionality (config_flamegpu.h)
    flamegpu_define_everything();

    /// # Run the Combined Model
    flamegpu::CUDASimulation simulation(model);
    simulation.initialise(argc, argv);
    simulation.simulate();

    /// # Cleanup
    free_memory_cudnn();
    system("test -f 'end.json' && mv 'end.json' 'end-1.json'");
    simulation.exportData("end.json");

    exit(EXIT_SUCCESS);
}
