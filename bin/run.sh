
#TODO All of this config
help()
{
    printf "`tput setaf 1 bold`IN DEVELOPMENT/NEED TO CLEANUP`tput sgr0`\n"
    printf "Usage: run [OPTION] ... [FILE]\n"
    printf "Operating modes:\n"
    printf "  -t, --training\n"
    printf "  -T, --testing\n"
    printf "Specifying output:\n"
    printf "  -c, --checkpoint=N\n"
    printf "      --verbose\n"
    printf "  -o, --output=FILE\n"
    printf "Specifying input:\n"
    printf "  -i, --input=FILE\n"
    printf "Training Options:\n"
    printf "  --episode-length=NUMBER\n"
    printf "  --episode-count=NUMBER\n"
    exit 0
}

arg_count=$#
in_file=input.json
out_file=output.json
operating_mode="training"
e_len=60
e_num=2
for ((i=0; i<arg_count; i++))
do
    case "$1" in
        -h|--help)
            help
            ;;
        -i|--input)
            in_file=$2
            shift
            ;;
        -o|--output)
            out_file=$2
            shift
            ;;
        -t|--training)
            operating_mode="training"
            shift
            ;;
        -T|--testing)
            operating_mode="testing"
            shift
            ;;
        -s|--episode-length)
            e_len=$2
            shift
            ;;
        -e|--episode-count)
            e_num=$2
            shift
            ;;
    esac
    shift
done

ifile=`mktemp`
sed 's/environment": {/environment": {@ENV@/g' < $in_file > $ifile
if [ $operating_mode == "training" ]
then
    sed -i 's/@ENV@/"global_training_mode": 1@ENV@/g' $ifile
else
    sed -i 's/@ENV@/"global_testing_mode": 1@ENV@/g' $ifile
fi
sed -i "s/@ENV@/,\"global_episode_length\": $e_len@ENV@/g" $ifile
sed -i 's/@ENV@//g' $ifile
mv $ifile input.json

step_count=120
./bin/lct-dabm --in input.json --steps $step_count --verbose
mv input.json last_input.json
if [ $? -eq 0 ]
then
    if [ -f end.json ]
    then
        mv end.json $out_file
    fi
fi
if [ ! -d checkpoints ]
then
    mkdir -p checkpoints
fi
mv *checkpoint*.bin checkpoints/
if [ ! -d weights ]
then
mkdir -p weights
fi
mv *weights*.bin weights/

